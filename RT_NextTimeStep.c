/* Determine next timestep, dt_RT */

/* PREPROCESSOR MACROS
 *  ADAPTIVE_TIMESCALE (default defined)
 *  DEBUG
 *  VERBOSE (default 0)
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define VERBOSE 0
#  define ADAPTIVE_TIMESCALE
#endif

#include <stdlib.h>
#if (defined DEBUG) || (VERBOSE > 0)
#  include <stdio.h>
#endif
#include <math.h>

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_TimeScale.h"

int RT_NextTimeStep(rt_float const dt,
                    rt_float const t_step,
                    RT_ConstantsT const Constants,
                    RT_TimeScale const* const dt_RT_sugg,
                    rt_float* const dt_RT,
                    unsigned char* const LastIterationFlag) {
#ifdef ADAPTIVE_TIMESCALE
  (*dt_RT) = dt_RT_sugg->Entropy; /* Initialise */
  if ((*dt_RT) > dt_RT_sugg->I_H1) {
    (*dt_RT) = dt_RT_sugg->I_H1;
  }
  if ((*dt_RT) > dt_RT_sugg->I_He1) {
    (*dt_RT) = dt_RT_sugg->I_He1;
  }
  if ((*dt_RT) > dt_RT_sugg->I_He2) {
    (*dt_RT) = dt_RT_sugg->I_He2;
  }
  if ((*dt_RT) > dt_RT_sugg->R_H1) {
    (*dt_RT) = dt_RT_sugg->R_H1;
  }
  if ((*dt_RT) > dt_RT_sugg->R_He1) {
    (*dt_RT) = dt_RT_sugg->R_He1;
  }
  if ((*dt_RT) > dt_RT_sugg->R_He2) {
    (*dt_RT) = dt_RT_sugg->R_He2;
  }
#  ifdef DEBUG
  if (((*dt_RT) <= 0.) || !(bool)isfinite((*dt_RT))) {
    printf("ERROR: %s: %i: dt_RT=%f\n", __FILE__, __LINE__, (*dt_RT));
    printf(" dt_RT_sugg.Entropy=%4.2e;\n", dt_RT_sugg->Entropy);
    printf(
        " dt_RT_sugg.I_H1=%4.2e; dt_RT_sugg.I_He1=%4.2e; "
        "dt_RT_sugg.I_He2=%4.2e\n",
        dt_RT_sugg->I_H1,
        dt_RT_sugg->I_He1,
        dt_RT_sugg->I_He2);
    printf(
        " dt_RT_sugg.R_H1=%4.2e; dt_RT_sugg.R_He1=%4.2e; "
        "dt_RT_sugg.R_He2=%4.2e\n",
        dt_RT_sugg->R_H1,
        dt_RT_sugg->R_He1,
        dt_RT_sugg->R_He2);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#  endif
  (*dt_RT) = Constants.Timestep_Factor * (*dt_RT);
#else
  (*dt_RT) = Constants.MIN_DT;
#endif /* ADAPTIVE_TIMESCALE */
#if VERBOSE >= 2
  printf("%s: suggested (*dt_RT)=%e\n", __FILE__, (*dt_RT));
  (void)fflush(stdout);
#endif

#ifdef ENFORCE_MAX_DT
  if ((*dt_RT) > Constants.MAX_DT) (*dt_RT) = Constants.MAX_DT;
#endif

  /* Warning, the following can lead to rounding errors which create infinite
   * loops. That's why I set a flag. */
  if ((t_step + (*dt_RT)) >= dt) {
    (*dt_RT) = (dt - t_step);
    (*LastIterationFlag) = TRUE;
  }
#if defined(ADAPTIVE_TIMESCALE) && (VERBOSE >= 2)
  printf("%s: (*dt_RT)= %4.2e ( %4.2e %4.2e %4.2e %4.2e %4.2e %4.2e %4.2e )\n",
         __FILE__,
         (*dt_RT),
         dt_RT_sugg->Entropy,
         dt_RT_sugg->I_H1,
         dt_RT_sugg->I_He1,
         dt_RT_sugg->I_He2,
         dt_RT_sugg->R_H1,
         dt_RT_sugg->R_He1,
         dt_RT_sugg->R_He2);
  (void)fflush(stdout);
#endif

  return EXIT_SUCCESS;
}
