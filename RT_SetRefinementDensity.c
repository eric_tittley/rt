/* Set the the Density field in the refinement Data_los.
 *
 * RT_SetRefinementDensity(*Data_cell, *Data_los, cell)
 *
 * ARGUMENTS
 *  Input, not modified
 *   Data_los   Pointer to the RT_Data structure of the parent LOS.
 *   cell       Data_los[cell] is the cell being refined.
 *  Input, fields modified
 *   Data_cell  Pointer to the RT_Data structure for the refinement.
 *
 * NOTES
 *  Data_los.Density must be set.
 *
 * AUTHOR: Eric Tittley
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "RT.h"
#include "RT_Data.h"
#include "config.h"

int RT_SetRefinementDensity(RT_Data* const Data_cell,
                            RT_Data const* const Data_los,
                            size_t const cell) {
  size_t i;
  rt_float dD_dcell;

  /* The local density slope */
  if (cell == 0) {
    dD_dcell = Data_los->Density[1] - Data_los->Density[0];
  } else if (cell == Data_los->NumCells - 1) {
    dD_dcell = Data_los->Density[Data_los->NumCells - 1] -
               Data_los->Density[Data_los->NumCells - 2];
  } else {
    dD_dcell = (Data_los->Density[cell + 1] - Data_los->Density[cell - 1]) / 2.;
  }

  /* Conserve mass */
  rt_float const densityAtLowerEdge = Data_los->Density[cell] - 0.5 * dD_dcell;

  /* Data_cell */
  for (i = 0; i < Data_cell->NumCells; ++i) {
    rt_float const RefinementCellSizeAsFractionOfCellToBeRefined =
        1. / (rt_float)Data_cell->NumCells;
    rt_float const RefinementCellCentreAsFractionOfCellToBeRefined =
        RefinementCellSizeAsFractionOfCellToBeRefined * ((rt_float)i + 0.5);
    Data_cell->Density[i] =
        densityAtLowerEdge +
        RefinementCellCentreAsFractionOfCellToBeRefined * dD_dcell;
#ifdef DEBUG
    if ((Data_cell->Density[i] < 0.) ||
        (!(bool)isfinite(Data_cell->Density[i]))) {
      printf("ERROR: %s: %i: Data_cell->Density[%lu] = %e\n",
             __FILE__,
             __LINE__,
             i,
             Data_cell->Density[i]);
      (void)fflush(stdout);
      return EXIT_FAILURE;
    }
#endif
  }

  return EXIT_SUCCESS;
}
