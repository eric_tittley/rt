#include "RT.h"

void RT_CalculateRecombinationRatesInclCooling(
    rt_float const ExpansionFactor,
    RT_ConstantsT const Constants,
    RT_Data const Data,
    /* updated */ RT_RatesBlockT Rates) {
  size_t cellindex;
#pragma omp parallel for
  for (cellindex = 0; cellindex < Data.NumCells; cellindex++) {
    RT_RecombinationRatesInclCooling(cellindex,
                                     ExpansionFactor,
                                     Constants,
                                     Data,
                                     /*@out@*/ Rates);
  }
}
