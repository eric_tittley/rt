#ifndef _ISEQUAL_H_
#define _ISEQUAL_H_

#include "Logic.h"
#include "RT_Precision.h"

bool isequal(rt_float const n, rt_float const d);

#endif
