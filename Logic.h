#ifndef _LOGIC_H_
#define _LOGIC_H_

#if defined(bool) || defined(__cplusplus) || defined(S_SPLINT_S)
#  define HAVE_BOOL
#endif

#ifdef __IBMC__
#  include <standards.h>
typedef __bool__ bool;
#  define HAVE_BOOL
#endif

#ifndef HAVE_BOOL
typedef unsigned char bool;
#endif

#ifndef TRUE
#  define TRUE (0 == 0)
#  define FALSE (!TRUE)
#endif

#endif /* _LOGIC_H_ */
