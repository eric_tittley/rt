/* RT_UpdateCell: Update Entropy and Ionization states for the next iteration
 *
 * ierr = RT_UpdateCell(rt_float const dt_RT,
 *                      rt_float const ExpansionFactor, RT_RatesT *Rates,
 * RT_Cell *Data_cell );
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  dt_RT
 *  ExpansionFactor
 *  Rates.I_H1
 *  Rates.I_He1
 *  Rates.I_He2
 *  Rates.alpha_H1
 *  Rates.alpha_He1
 *  Rates.alpha_He2
 *  Data_cell.n_H
 *  Data_cell.n_He
 *  Data_cell.f_H1
 *  Data_cell.f_H2
 *  Data_cell.f_He1
 *  Data_cell.f_He2
 *  Data_cell.f_He3
 *  Data_cell.Entropy
 *
 * ARGUMENTS INPUT AND MODIFIED
 *  n_H1
 *  n_H2
 *  n_He1
 *  n_He2
 *  n_He3
 *  Entropy
 *
 * ARGUMENTS INITIALISED
 *  f_*         Ionization fractions of various species.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * PREPROCESSOR MACROS
 *  DEBUG
 *  VERBOSE
 *  ENFORCE_MINIMUM_CMB_TEMPERATURE
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define VERBOSE 0
#  define ENFORCE_MINIMUM_CMB_TEMPERATURE 2.0
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif
#include <math.h>

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_DebugFunctions.h"
#include "RT_RatesT.h"

int RT_UpdateCell(rt_float const dt_RT,
                  rt_float const ExpansionFactor,
                  RT_ConstantsT const Constants,
                  RT_RatesT const* const Rates,
                  RT_Cell* const Data_cell) {
#if VERBOSE > 2
  printf("%s: Updating...\n", __FILE__);
  (void)fflush(stdout);
#endif

  /* Find the neutral & ionised number densities. */
  rt_float n_H1 = Data_cell->n_H * Data_cell->f_H1;
  rt_float n_H2 = Data_cell->n_H * Data_cell->f_H2;
  rt_float n_He1 = Data_cell->n_He * Data_cell->f_He1;
  rt_float n_He2 = Data_cell->n_He * Data_cell->f_He2;
  rt_float n_He3 = Data_cell->n_He * Data_cell->f_He3;

  /* Electron density */
  rt_float n_e = n_H2 + n_He2 + (2.0 * n_He3);
#ifdef DEBUG
  if (!(bool)isfinite(n_e) || (n_e < 0.0)) {
    printf("ERROR: %s: %i: n_e=%5.3e; n_H2=%5.3e; n_He2=%5.3e; n_He3=%5.3e\n",
           __FILE__,
           __LINE__,
           n_e,
           n_H2,
           n_He2,
           n_He3);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  /* Maximum possible free electron density */
  rt_float const n_emax = Data_cell->n_H + (2.0 * Data_cell->n_He);
  if (n_e > n_emax * Constants.OnePlusEps) {
    printf("ERROR: %s: %i: n_e=%5.3e > n_emax=%5.3e\n",
           __FILE__,
           __LINE__,
           n_e,
           n_emax);
    printf("      n_e - n_emax = %5.3e\n", n_e - n_emax);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  /*Solves for Hydrogen species density */
  rt_float const n_H1new =
      n_H1 + dt_RT * (n_H2 * n_e * Rates->alpha_H1 - Rates->I_H1);
  rt_float const n_H2new =
      n_H2 + dt_RT * (Rates->I_H1 - n_H2 * n_e * Rates->alpha_H1);
#ifdef DEBUG
  if (!(bool)isfinite(n_H2new)) {
    printf(
        "ERROR: %s: %i: n_H2new=%5.3e: dt_RT=%5.3e n_H2=%5.3e "
        "Rates->I_H1=%5.3e n_e=%5.3e Rates->alpha_H1=%5.3e\n",
        __FILE__,
        __LINE__,
        n_H2new,
        dt_RT,
        n_H2,
        Rates->I_H1,
        n_e,
        Rates->alpha_H1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif
  n_H1 = n_H1new;
  n_H2 = n_H2new;

  /* The following conditions should never really be triggered. */
  if ((n_H1 < 0) || (n_H2 > Data_cell->n_H)) {
#if VERBOSE >= 2
    printf("WARNING: %s: H ionizations/recombinations too fast.\n", __FILE__);
#endif
    n_H1 = 0.;
    n_H2 = Data_cell->n_H;
  }
  if ((n_H1 > Data_cell->n_H) || (n_H2 < 0)) {
#if VERBOSE >= 2
    printf("WARNING: %s: H ionizations/recombinations too fast.\n", __FILE__);
#endif
    n_H1 = Data_cell->n_H;
    n_H2 = 0.;
  }

  /*Solves for helium species densities */
  rt_float const n_He1new =
      n_He1 + dt_RT * (n_e * n_He2 * Rates->alpha_He1 - Rates->I_He1);
  rt_float const n_He2new =
      n_He2 +
      dt_RT * (Rates->I_He1 - Rates->I_He2 +
               n_e * (n_He3 * Rates->alpha_He2 - n_He2 * Rates->alpha_He1));
  rt_float const n_He3new =
      n_He3 + dt_RT * (Rates->I_He2 - n_e * n_He3 * Rates->alpha_He2);
#ifdef DEBUG
  if (!(bool)isfinite(n_He1new)) {
    printf(
        "ERROR: %s: %i: n_He1new=%5.3e: n_He1=%5.3e; dt_RT=%5.3e; n_e=%5.3e; "
        "n_He2=%5.3e; Rates->alpha_He2=%5.3e; Rates->I_He1=%5.3e\n",
        __FILE__,
        __LINE__,
        n_He1new,
        n_He1,
        dt_RT,
        n_e,
        n_He2,
        Rates->alpha_He1,
        Rates->I_He1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if (!(bool)isfinite(n_He2new)) {
    printf(
        "ERROR: %s: %i: n_He2new=%5.3e: n_He2=%5.3e; n_He3=%5.3e; n_e=%5.3e; "
        "dt_RT=%5.3e Rates->I_He1=%5.3e Rates->I_He2=%5.3e\n",
        __FILE__,
        __LINE__,
        n_He2new,
        n_He2,
        n_He3,
        n_e,
        dt_RT,
        Rates->I_He1,
        Rates->I_He2);
    printf(" Rates->alpha_He1=%5.3e; Rates->alpha_He2=%5.3e;\n",
           Rates->alpha_He1,
           Rates->alpha_He2);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if (!(bool)isfinite(n_He3new)) {
    printf(
        "ERROR: %s: %i: n_He3new=%5.3e: n_He3=%5.3e; dt_RT=%5.3e "
        "Rates->I_He2=%5.3e n_e=%5.3e Rates->alpha_He2=%5.3e\n",
        __FILE__,
        __LINE__,
        n_He3new,
        n_He3,
        dt_RT,
        Rates->I_He2,
        n_e,
        Rates->alpha_He2);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif
  n_He1 = n_He1new;
  n_He2 = n_He2new;
  n_He3 = n_He3new;

  if (n_He1 > Data_cell->n_He) {
    n_He1 = Data_cell->n_He;
    n_He2 = 0.;
    n_He3 = 0.;
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  if (n_He2 > Data_cell->n_He) {
    n_He1 = 0.;
    n_He2 = Data_cell->n_He;
    n_He3 = 0.;
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  if (n_He3 > Data_cell->n_He) {
    n_He1 = 0.;
    n_He2 = 0.;
    n_He3 = Data_cell->n_He;
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  rt_float f;
  if (n_He1 < 0.) {
    n_He1 = 0.;
    if (n_He2 < n_He3) {
      f = n_He2 / (n_He2 + n_He3);
      n_He2 = f * Data_cell->n_He;
      n_He3 = (1. - f) * Data_cell->n_He;
    } else {
      f = n_He3 / (n_He2 + n_He3);
      n_He3 = f * Data_cell->n_He;
      n_He2 = (1. - f) * Data_cell->n_He;
    }
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  if (n_He2 < 0.) {
    n_He2 = 0.;
    if (n_He1 < n_He3) {
      f = n_He1 / (n_He1 + n_He3);
      n_He1 = f * Data_cell->n_He;
      n_He3 = (1. - f) * Data_cell->n_He;
    } else {
      f = n_He3 / (n_He1 + n_He3);
      n_He3 = f * Data_cell->n_He;
      n_He1 = (1. - f) * Data_cell->n_He;
    }
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  if (n_He3 < 0.) {
    n_He3 = 0.;
    if (n_He2 < n_He1) {
      f = n_He2 / (n_He2 + n_He1);
      n_He2 = f * Data_cell->n_He;
      n_He1 = (1. - f) * Data_cell->n_He;
    } else {
      f = n_He1 / (n_He2 + n_He1);
      n_He1 = f * Data_cell->n_He;
      n_He2 = (1. - f) * Data_cell->n_He;
    }
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }

  /* Update ionization fractions */
  if (Data_cell->n_H > 0.) {
    Data_cell->f_H1 = n_H1 / Data_cell->n_H;
    Data_cell->f_H2 = n_H2 / Data_cell->n_H;
  } else {
    Data_cell->f_H1 = 1.;
    Data_cell->f_H2 = 0.;
  }
  /* remember: small = big - big leads to truncation errors.
   * Always perform the substraction: big = big - small */
  if (Data_cell->f_H1 > Data_cell->f_H2) {
    Data_cell->f_H1 = 1. - Data_cell->f_H2;
  } else {
    Data_cell->f_H2 = 1. - Data_cell->f_H1;
  }
  if (Data_cell->n_He > 0.) {
    Data_cell->f_He1 = n_He1 / Data_cell->n_He;
    Data_cell->f_He2 = n_He2 / Data_cell->n_He;
    Data_cell->f_He3 = n_He3 / Data_cell->n_He;
    if ((Data_cell->f_He1 >= Data_cell->f_He2) &&
        (Data_cell->f_He1 >= Data_cell->f_He3)) {
      Data_cell->f_He1 = 1. - Data_cell->f_He2 - Data_cell->f_He3;
    } else if ((Data_cell->f_He2 >= Data_cell->f_He1) &&
               (Data_cell->f_He2 >= Data_cell->f_He3)) {
      Data_cell->f_He2 = 1. - Data_cell->f_He1 - Data_cell->f_He3;
    } else {
      Data_cell->f_He3 = 1. - Data_cell->f_He1 - Data_cell->f_He2;
    }
  } else { /* No Helium */
    Data_cell->f_He1 = 1.;
    Data_cell->f_He2 = 0.;
    Data_cell->f_He3 = 0.;
  }

  /* Update the entropy */
  /* Need to worry about under & over flows.
   * Scale is 10^(9 - ( -24 * 5/3 ) + (-10 -17 ) )
   * Which has an overflow ( 9 + 40 ) and an underflow (-10 - 17).
   * So stick the +17 with the density as (-24 +17*3/5)*5/3
   * to make it 10^(9 - (-23) -10 )  which is float safe in any order. */
  rt_float ResidualHeatingScaleFactorGammaInv =
      POW(Constants.Ma, 1. / Constants.gamma_ad);
  rt_float Entropy_new =
      Data_cell->Entropy +
      dt_RT * (Constants.gamma_ad - 1.) /
          POW(Data_cell->Density * ResidualHeatingScaleFactorGammaInv,
              Constants.gamma_ad) *
          (Rates->G - Rates->L);

#ifdef DEBUG
  if (!(bool)isfinite(Entropy_new)) {
    printf(
        "ERROR: %s: %i: Entropy_new=%5.3e; Entropy=%5.3e; dt_RT=%5.3e; "
        "Density=%5.3e; G=%5.3e; L=%5.3e;\n",
        __FILE__,
        __LINE__,
        Entropy_new,
        Data_cell->Entropy,
        dt_RT,
        Data_cell->Density,
        Rates->G,
        Rates->L);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  /* Update the neutral & ionised number densities. */
  n_H1 = Data_cell->n_H * Data_cell->f_H1;
  n_H2 = Data_cell->n_H * Data_cell->f_H2;
  n_He1 = Data_cell->n_He * Data_cell->f_He1;
  n_He2 = Data_cell->n_He * Data_cell->f_He2;
  n_He3 = Data_cell->n_He * Data_cell->f_He3;

  /* Electron density */
  n_e = n_H2 + n_He2 + (2. * n_He3);

  /* Total number density */
  rt_float const n = Data_cell->n_H + Data_cell->n_He + n_e;

  /* Temperature is a function of Entropy */
  /* Assumes m_He = 4 m_H and mass_e = 0 */
  rt_float const mu = (Data_cell->n_H + 4. * Data_cell->n_He) / n;

#if (defined ENFORCE_MINIMUM_CMB_TEMPERATURE) || (defined CONSTANT_TEMPERATURE)
#  ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
  /* The entropy cannot go below that set by the CMB temperature */
  rt_float const Entropy_Min =
      (Constants.k_B * ENFORCE_MINIMUM_CMB_TEMPERATURE) /
      (mu * Constants.mass_p *
       POW(Data_cell->Density, Constants.gamma_ad - 1.));
  if (Entropy_new < Entropy_Min) {
    Entropy_new = Entropy_Min;
  }
#  else
  /* CONSTANT_TEMPERATURE */
  Entropy_new = (Constants.k_B * CONSTANT_TEMPERATURE) /
                (mu * Constants.mass_p *
                 POW(Data_cell->Density, Constants.gamma_ad - 1.));
#  endif
#  ifdef DEBUG
  if (!(bool)isfinite(Entropy_new) || (Entropy_new <= 0)) {
    printf("ERROR: %s: %i: Entropy_new=%5.3e; mu=%7.5f; Density=%5.3e\n",
           __FILE__,
           __LINE__,
           Entropy_new,
           mu,
           Data_cell->Density);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#  endif
#endif
  if (Entropy_new < 0.) {
    /* NOTE: This is bad.  Entropy can never be zero. The code will crash
     * elsewhere if Entropy is zero.  Need something to put here. */
    Entropy_new = 0;
  }

  Data_cell->Entropy = Entropy_new;

#if VERBOSE > 2
  printf("%s: ...done\n", __FILE__);
  (void)fflush(stdout);
#endif

  /* Note the scaling of the density */
  rt_float const Temperature =
      mu * (Constants.mass_p / Constants.k_B) * Entropy_new *
      POW(Constants.Mpc, 1.0 - Constants.gamma_ad) *
      POW(Data_cell->Density * Constants.Mpc, Constants.gamma_ad - 1.0);
  Data_cell->T = Temperature;

  return EXIT_SUCCESS;
}
