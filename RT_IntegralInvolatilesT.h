#ifndef _RT_INTEGRALINVOLATILES_H_
#define _RT_INTEGRALINVOLATILES_H_

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_ConstantsT.h"
#include "RT_Precision.h"
#include "RT_SourceT.h"

struct RT_IntegralInvolatiles_struct {
  size_t nFrequencies;
  size_t nSpecies; /* Alphas will have nSpecies * nFrequencies elements */
  rt_float* Frequencies;
  rt_float* Weights;
  rt_float* Alphas;
  rt_float* Luminosities;
};

typedef struct RT_IntegralInvolatiles_struct RT_IntegralInvolatilesT;

/* The only method for setting nFrequencies and nSpecies */
int RT_IntegralInvolatiles_allocate(RT_ConstantsT const Constants,
                                    RT_IntegralInvolatilesT* Involatiles);

void RT_IntegralInvolatiles_free(RT_IntegralInvolatilesT Involatiles);

int RT_IntegralInvolatiles_setFrequenciesWeightsAlphas(
    RT_ConstantsT const Constants,
    RT_IntegralInvolatilesT Involatiles);

int RT_IntegralInvolatiles_setLuminosity(RT_SourceT Source,
                                         RT_ConstantsT const Constants,
                                         rt_float SourceRedshift,
                                         RT_IntegralInvolatilesT Involatiles);

#endif
