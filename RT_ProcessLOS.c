/* Propagation of ionisation fronts through a realistic density field using the
 * photon conserving algorithm, taking cosmological expansion into account.
 *
 * This is the core Radiative Transfer function.  It integrates the equations
 * for the given density distribution from time t to t+dt, smoothly modifying
 * the gas density from rho(t) to rho(t+dt).
 *
 * RT_ProcessLOS takes as input a density field along a line of sight (density,
 * vector) and the state of the gas (Temperature, Ion density, etc (see RT.h))
 * as a structure of scalars and vectors.  Returns a modified structure
 * corresponding to the new state of the system.
 *
 * ARGUMENTS
 *  Input, not modified
 *   t                The time at the start of the iteration (s)
 *   dt               Timestep (s)
 *   ExpansionFactor  Expansion factor at the *end* of the iteration.
 *  Input, modified
 *   Data_los         Structure containing the status of the gas.
 *                    It is essential that the following fields be set before
 * calling: R[*], Density[*], Entropy[*], NCols[0:5], NumCells RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE      If DEBUG is defined, exits on various numerical errors.
 *
 * AUTHORS
 *  Jamie Bolton (original code)
 *  Eric Tittley (incorporation into PMRT, parallelisation, refinements)
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define VERBOSE 0
#  define INTEGRATE_ODE_RUNGE_KUTTA
#  define ADAPTIVE_TIMESCALE
#endif

#include <stdlib.h>
#if (VERBOSE > 0) || (defined DEBUG)
#  include <stdio.h>
#endif
#include <math.h>
#include <string.h>

#if VERBOSE > 0
#  include <sys/types.h>
#  include <unistd.h>
#endif

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_SourceT.h"
#include "RT_TimeScale.h"

#define FREE_MEMORY_BASE free(CellsNotRefined);

#ifdef REFINEMENTS
#  define FREE_MEMORY_REFS free(CellsToRefine);
#else
#  define FREE_MEMORY_REFS
#endif

#define FREE_MEMORY FREE_MEMORY_BASE FREE_MEMORY_REFS

/* RT_ProcessLOS modifies Data_los */
int RT_ProcessLOS(rt_float const t,
                  rt_float const dt,
                  rt_float const ExpansionFactor,
                  RT_SourceT const Source,
                  RT_ConstantsT const Constants,
                  RT_Data *const Data_los) {
  size_t iz;
  bool LastIterationFlag;

  /* Timescales */
  rt_float t_step = 0.0;
  rt_float dt_RT;
  RT_TimeScale dt_RT_sugg;
  int nIter = 0;

  int ierr;
#ifdef REFINEMENTS
  rt_float n_H1;
  rt_float tau_H1_cell;
#  ifdef USE_MCTfR
  rt_float tau_H1;
#  endif
#endif

  size_t NCellsNotRefined;
  size_t *CellsNotRefined = NULL;
#ifdef REFINEMENTS
  size_t NCellsToRefine, i;
  size_t *CellsToRefine = NULL;
#endif

#if VERBOSE > 0
  printf("Got to %s (PID %5i)\n", __FILE__, getpid());
  (void)fflush(stdout);
#endif

#ifdef DEBUG
  if (dt <= 0.0) {
    printf("ERROR: %s: %i: dt=%e;\n", __FILE__, __LINE__, dt);
    (void)fflush(stdout);
    FREE_MEMORY;
    return EXIT_FAILURE;
  }
#endif

  /* Number of cells in this LOS */
  size_t N_cells = Data_los->NumCells;
#ifdef DEBUG
  if (N_cells <= 0) {
    printf("ERROR: %s: %i: N_cells=%lu <= 0;\n", __FILE__, __LINE__, N_cells);
    (void)fflush(stdout);
    FREE_MEMORY;
    return EXIT_FAILURE;
  }
#endif

#ifdef REFINEMENTS
  /* Allocate memory for the lists of cells to process and cells to refine */
  CellsToRefine = (size_t *)calloc(N_cells, sizeof(size_t));
#  ifdef DEBUG
  if (CellsToRefine == NULL) {
    printf("ERROR: %s: %i: calloc returned NULL\n", __FILE__, __LINE__);
    printf("      N_cells=%lu\n", N_cells);
    (void)fflush(stdout);
    FREE_MEMORY;
    return EXIT_FAILURE;
  }
#  endif
#endif
  CellsNotRefined = (size_t *)calloc(N_cells, sizeof(size_t));
#ifdef DEBUG
  if (CellsNotRefined == NULL) {
    printf("ERROR: %s: %i: calloc returned NULL\n", __FILE__, __LINE__);
    printf("      N_cells=%lu\n", N_cells);
    (void)fflush(stdout);
    FREE_MEMORY;
    return EXIT_FAILURE;
  }
#endif

  if (dt < Constants.MIN_DT) {
    dt_RT = dt;
    LastIterationFlag = TRUE;
  } else {
    dt_RT = Constants.MIN_DT;
    LastIterationFlag = FALSE;
  }

#ifdef DEBUG
  if (dt_RT < 0.) {
    printf("ERROR: %s: (initial): dt_RT < 0: dt_RT=%f\n", __FILE__, dt_RT);
    (void)fflush(stdout);
    FREE_MEMORY;
    return EXIT_FAILURE;
  }
#endif

#if (defined SEGMENTED_LOS) && !(defined THIN_APPROX)
  /* NCol[0:5] must already be set. Not required if a refinement. */
  RT_UpdateColumnDensitiesAlongLOS(0., dt, Constants, Data_los);
  /* Set the cumulative column densities through this LOS segment
   * at the *beginning* of dt. */
  Data_los->NCol[6] = Data_los->column_H1[N_cells - 1] +
                      (Data_los->n_H[N_cells - 1] *
                       Data_los->f_H1[N_cells - 1] * Data_los->dR[N_cells - 1]);
  Data_los->NCol[7] =
      Data_los->column_He1[N_cells - 1] +
      (Data_los->n_He[N_cells - 1] * Data_los->f_He1[N_cells - 1] *
       Data_los->dR[N_cells - 1]);
  Data_los->NCol[8] =
      Data_los->column_He2[N_cells - 1] +
      (Data_los->n_He[N_cells - 1] * Data_los->f_He2[N_cells - 1] *
       Data_los->dR[N_cells - 1]);
#endif

  /* ****** MAIN LOOP OVER TIME *******
   * do
   *  t_step += dt_RT (calculated)
   * While t_step <= dt
   */

  /* Start of main loop over time (t_step = 0 to dt, incrementing by dt_RT
   * each step */
  do {
#if VERBOSE > 1
    printf("%s: %i: Start of loop: t_step=%5.3e; dt=%5.3e; dt_RT=%5.3e\n",
           __FILE__,
           __LINE__,
           t_step,
           dt,
           dt_RT);
    (void)fflush(stdout);
#endif

#ifdef DEBUG
    if ((t_step + dt_RT) > dt * Constants.OnePlusEps) {
      printf("ERROR: %s: %i: t_step + dt_RT > dt\n", __FILE__, __LINE__);
      printf("      t_step=%5.3e; dt_RT=%5.3e; dt=%5.3e\n", t_step, dt_RT, dt);
      printf("      LastIterationFlag=%i\n", (int)LastIterationFlag);
      (void)fflush(stdout);
      FREE_MEMORY;
      return EXIT_FAILURE;
    }
#endif

    RT_TimeScale_SetInfiniteTime(&dt_RT_sugg);

    /* Time the source turns on.  Note that this is becoming increasingly
     * redundant since it is better to call RT_Luminosity with z = redshift of
     * the source as seen from the cell/particle */
    rt_float t_on = cosmological_time(1. / (Source.z_on + 1.0), Constants);

    /* The position of the light front. */
    rt_float const DistanceTolerance = Constants.c * dt / 1e5;
    rt_float R_LightFrontPosition =
        RT_LightFrontPosition(t + t_step, t_on, DistanceTolerance, Constants);

    /* If R[0] > Light Front Position, then the light front will not enter the
     * first cell.
     * The light front is between the source and the lower edge of the first
     * cell. Reset t_step such that the time step starts with the light front at
     * the lower edge of the first cell.
     */
    if (R_LightFrontPosition < Data_los->R[0]) {
      printf(
          "%s: %i: Resetting t_step from %5.3es", __FILE__, __LINE__, t_step);
      /* The following calculation of t_step is only an approximation if there
       * is cosmological expansion. Exact, otherwise. */
      /* t_step = fmin(dt, Data_los->R[0]/c - (t-t_on) ); */
      t_step = Data_los->R[0] / Constants.c - (t - t_on);
      if (t_step > dt) {
        t_step = dt;
        printf(" to %5.3es\n", t_step);
        LastIterationFlag = TRUE;
        break; /* out of while loop */
      }
      printf(" to %5.3es\n", t_step);
    }

    /* Update densities due to compression/expansion */
    rt_float const density_to_n_H =
        (1. - Constants.Y) / (Constants.mass_p + Constants.mass_e);
    rt_float const n_H_to_n_He =
        0.25 * Constants.Y / (1.0 - Constants.Y); /* Nan if Constants.Y==1 */
    for (iz = 0; iz < N_cells; iz++) {
      /* Density */
      Data_los->n_H[iz] = density_to_n_H * Data_los->Density[iz];
      if (Constants.Y > 0.) {
        if (Constants.Y < 1.) {
          Data_los->n_He[iz] = n_H_to_n_He * Data_los->n_H[iz];
        } else {
          /* Constants.Y=1 */
          Data_los->n_He[iz] = Data_los->Density[iz] /
                               (4. * (Constants.mass_p + Constants.mass_e));
        }
      } else {
        Data_los->n_He[iz] = 0.;
      }
#ifdef DEBUG
      if ((Data_los->n_H[iz] < 0.) || !(bool)isfinite(Data_los->n_H[iz])) {
        printf(
            "%s: %i: ERROR: n_H[%lu]=%e; Data_los->Density[iz]=%e; "
            "density_to_n_H=%e\n",
            __FILE__,
            __LINE__,
            iz,
            Data_los->n_H[iz],
            Data_los->Density[iz],
            density_to_n_H);
        (void)fflush(stdout);
        FREE_MEMORY;
        return EXIT_FAILURE;
      }
      if ((Data_los->n_H[iz] <= 0.) && (Data_los->n_He[iz] <= 0.)) {
        printf("%s: %i: ERROR: n_H[%lu]=%e; n_He[%lu]=%e\n",
               __FILE__,
               __LINE__,
               iz,
               Data_los->n_H[iz],
               iz,
               Data_los->n_He[iz]);
        (void)fflush(stdout);
        FREE_MEMORY;
        return EXIT_FAILURE;
      }
#endif
    }

    RT_UpdateColumnDensitiesAlongLOS(t_step, dt, Constants, Data_los);

    /* Restructure to make easier to parallelise for example on hyperthreaded
     * architecture or GPUs.
     *
     * 1) determine which cells need refining and which don't.
     * 2) Loop through all the cells to refine.
     * 3) Loop through all the cells that don't need refining.
     *
     * If no refinements, then skip the test and process all
     * the cells.
     */

#ifdef REFINEMENTS
    /* Determine which cells need refining and which don't. */
    NCellsToRefine = 0;
    NCellsNotRefined = 0;
    for (iz = 0; iz < N_cells; iz++) {
      /* The optical depth in the cell */
      n_H1 = Data_los->n_H[iz] * Data_los->f_H1[iz];
      tau_H1_cell = Constants.sigma_0 * n_H1 * Data_los->dR[iz];
      /* Check to see if refinement is required. */
#  ifdef USE_MCTfR
      /* Cumulative optical depth at Lyman edge up to the lower edge of the cell
       */
      tau_H1 = Constants.sigma_0 * Data_los->column_H1[iz];
      /* If  (the cell has already been split)
       *  OR (the cell is optically thick AND there are photons hitting the
       * cell) */
      if ((Data_los->Ref_pointer[iz] != NULL) ||
          ((tau_H1_cell > Constants.Max_tau_in_cell) &&
           (tau_H1 < Constants.Max_cumulative_tau_for_refinement))) {
#  else
      /* If  (the cell has already been split) OR (the cell is optically thick)
       */
      if ((Data_los->Ref_pointer[iz] != NULL) ||
          (tau_H1_cell > Constants.Max_tau_in_cell)) {
#  endif
        /* Remember which cells need refining */
        CellsToRefine[NCellsToRefine] = iz;
        NCellsToRefine++;
      } else { /* No refinement required */
        CellsNotRefined[NCellsNotRefined] = iz;
        NCellsNotRefined++;
      }
    }

    /* Loop over all the cells that need refining */
    for (i = 0; i < NCellsToRefine; i++) {
      iz = CellsToRefine[i];
      ierr = RT_RefineCell(iz,
                           dt,
                           dt_RT,
                           t + t_step,
                           ExpansionFactor,
                           Source,
                           Constants,
                           Data_los);
#  ifdef DEBUG
      if (ierr == EXIT_FAILURE) {
        printf("ERROR: %s %i: RT_RefineCell failed for cell %lu\n",
               __FILE__,
               __LINE__,
               iz);
        (void)fflush(stdout);
        FREE_MEMORY;
        return EXIT_FAILURE;
      }
#  endif
    }  /* End of loop over the cells that need refining */
#else  /* No REFINEMENTS */
    NCellsNotRefined = N_cells;
    for (iz = 0; iz < N_cells; iz++) {
      CellsNotRefined[iz] = iz;
    }
#endif /* if REFINEMENTS ... else ... endif */

    /* Process all cells that didn't need refining */
    ierr = RT_ProcessUnrefinedCells(CellsNotRefined,
                                    NCellsNotRefined,
                                    LastIterationFlag,
                                    ExpansionFactor,
                                    t + t_step,
                                    dt,
                                    dt_RT,
                                    Source,
                                    Constants,
                                    Data_los,
                                    &dt_RT_sugg);
#ifdef DEBUG
    if (ierr == EXIT_FAILURE) {
      printf("ERROR: %s %i: RT_ProcessUnrefinedCells failed.\n",
             __FILE__,
             __LINE__);
      (void)fflush(stdout);
      FREE_MEMORY;
      return EXIT_FAILURE;
    }
#endif

    /* Increment the time step */
    t_step += dt_RT;

    /* Determine next timestep, dt_RT */
    ierr = RT_NextTimeStep(
        dt, t_step, Constants, &dt_RT_sugg, &dt_RT, &LastIterationFlag);
#ifdef DEBUG
    if (ierr == EXIT_FAILURE) {
      printf("ERROR: %s: %i: RT_NextTimeStep failed. iz=%lu\n",
             __FILE__,
             __LINE__,
             iz);
      (void)fflush(stdout);
      FREE_MEMORY;
      return EXIT_FAILURE;
    }
#endif

#if VERBOSE > 1
    printf("%s: %i: End of loop: t_step=%5.3e; dt=%5.3e;\n",
           __FILE__,
           __LINE__,
           t_step,
           dt);
    (void)fflush(stdout);
#endif
    nIter++;
  } while (t_step < dt
#ifdef MAX_RT_ITERS
           && nIter < MAX_RT_ITERS
#endif
  ); /* end loop over dt when t_step == t_final */
  (void)fflush(stdout);

#ifdef DEBUG
  if (!LastIterationFlag) {
    printf(
        "ERROR: %s: %i: Loop through dt finished without setting "
        "LastIterationFlag\n",
        __FILE__,
        __LINE__);
    printf("      dt=%5.3e; t_step=%5.3e; dt_RT=%5.3e\n", dt, t_step, dt_RT);
    (void)fflush(stdout);
    FREE_MEMORY;
    return EXIT_FAILURE;
  }
#endif

#if VERBOSE > 1
  printf("%s: PID: %i: Updating column densities...", __FILE__, getpid());
  (void)fflush(stdout);
#endif
  /* Update the column densities through to the end of this LOS on this grid,
   * after dt */
  RT_UpdateColumnDensitiesAlongLOS(dt, dt, Constants, Data_los);
#if (defined SEGMENTED_LOS) && !(defined THIN_APPROX)
  Data_los->NCol[9] = Data_los->column_H1[N_cells - 1] +
                      (Data_los->n_H[N_cells - 1] *
                       Data_los->f_H1[N_cells - 1] * Data_los->dR[N_cells - 1]);
  Data_los->NCol[10] =
      Data_los->column_He1[N_cells - 1] +
      (Data_los->n_He[N_cells - 1] * Data_los->f_He1[N_cells - 1] *
       Data_los->dR[N_cells - 1]);
  Data_los->NCol[11] =
      Data_los->column_He2[N_cells - 1] +
      (Data_los->n_He[N_cells - 1] * Data_los->f_He2[N_cells - 1] *
       Data_los->dR[N_cells - 1]);
#endif

#if VERBOSE > 1
  printf("done.\n");
  (void)fflush(stdout);
#endif

  printf("<dt_RT> = %e\n", dt / ((rt_float)nIter));

#if VERBOSE > 0
  printf("Leaving %s (PID %5i)\n", __FILE__, getpid());
  (void)fflush(stdout);
#endif
  FREE_MEMORY;

  return EXIT_SUCCESS;
}
