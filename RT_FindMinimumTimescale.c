#include <math.h>
#include <stdlib.h>

#include "RT.h"

void fMT_findTauInEachCell_kernel(RT_ConstantsT const Constants,
                                  RT_RatesBlockT const Rates,
                                  RT_Data const Data,
                                  /* out */ rt_float *tau) {
  size_t cellindex;
  for (cellindex = 0; cellindex < Data.NumCells; cellindex++) {
    rt_float tau_local = tau[cellindex];

    /* Find the neutral & ionised number densities. */
    rt_float const n_H1 = Data.n_H[cellindex] * Data.f_H1[cellindex];
    rt_float const n_H2 = Data.n_H[cellindex] * Data.f_H2[cellindex];
    rt_float const n_He2 = Data.n_He[cellindex] * Data.f_He2[cellindex];
    rt_float const n_He3 = Data.n_He[cellindex] * Data.f_He3[cellindex];

    /* Electron density */
    rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

    /* Entropy timescale */
    /* Comment about those Ma flying around:
     * G & L come in with a value scaled UP by Ma, so that it can fit in the
     * float range. The result (G-L) is still too high by a factor of Ma. That
     * extra factor is removed by division with
     * ResidualHeatingScaleFactorGammaInv^gamma_ad
     *
     * All intermediate terms must also fit in the float range, hence the form
     * below, which can easily be reduced to a more sensible form, a form that
     * will cause numerical errors.
     */
    rt_float ResidualHeatingScaleFactorGammaInv =
        POW(Constants.Ma, 1. / Constants.gamma_ad);
    rt_float const EntropyRate =
        ((Constants.gamma_ad - 1.) /
         POW(Data.Density[cellindex] * ResidualHeatingScaleFactorGammaInv,
             Constants.gamma_ad) *
         (Rates.G[cellindex] - Rates.L[cellindex]));
    /* Only worry about cooling too fast */
    if (EntropyRate < 0.) {
      rt_float const tau_test = -1.0 * Data.Entropy[cellindex] / EntropyRate;
      if (tau_test < tau_local) tau_local = tau_test;
    }

    /* Ionization timescale */
    /* Trigger on dNH1dt increasing or decreasing */
    rt_float const dNH1dt =
        (n_H2 * n_e * Rates.alpha_H1[cellindex] - Rates.I_H1[cellindex]);

    /* If H is mostly ionized, nHI and nHII will be set to the
     * equilibrium value (See RT_UpdateCell_RateBlock) */
    if (RT_useEquilibriumValues(cellindex, &Data, &Rates, &Constants)) {
      tau_local = 1e25;
    } else {
      rt_float Delta;
      if (n_H1 >= n_H2) {
        Delta = n_H1 - Rates.n_HI_Equilibrium[cellindex];
      } else {
        Delta = n_H2 - Rates.n_HII_Equilibrium[cellindex];
      }
      rt_float tau_test = FABS(Delta / dNH1dt);
      if (tau_test < tau_local) tau_local = tau_test;
    }

    /*** TODO: Add rates for changes to H2, He2, & He3 ***/

    tau[cellindex] = tau_local;

  }  // end if cellindex < NumCells
}

rt_float RT_FindMinimumTimescale(RT_ConstantsT const Constants,
                                 RT_RatesBlockT const Rates,
                                 RT_Data const Data) {
  rt_float const InfiniteTime = 1e20;

  // Allocate and Initialize the timescale for each cell
  rt_float *tau = (rt_float *)malloc(Data.NumCells * sizeof(rt_float));
#pragma omp parallel for shared(tau)
  for (size_t cellindex = 0; cellindex < Data.NumCells; cellindex++) {
    tau[cellindex] = InfiniteTime;
  }

  fMT_findTauInEachCell_kernel(Constants,
                               Rates,
                               Data,
                               /* out */ tau);

  // Now find the minimum timescale of all the timescales, tau
  //  NOTE: Cannot parallelize this. Or, if we could, the effort isn't worth it.
  rt_float mintau = InfiniteTime;
  for (size_t cellindex = 0; cellindex < Data.NumCells; cellindex++) {
    if (tau[cellindex] < mintau) mintau = tau[cellindex];
  }

  free(tau);

  return mintau;
}
