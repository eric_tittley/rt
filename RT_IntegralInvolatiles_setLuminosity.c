#include <stdio.h>
#include <stdlib.h>

#include "RT.h"
#include "RT_IntegralInvolatilesT.h"
#include "RT_Luminosity.h"

/* SourceRedshift is the redshift of the target plus the redshift correction for
 * the source being seen at some time even earlier by the target.
 * For infinite light speed, SourceRedshift = Redshift.
 * Otherwise, use
 *  t_now = cosmological_time(1./(Redshift+1.);
 *  t_source = t_now - Data.R/Constants.c;
 *  sourceRedshift = expansion_factor_from_time(t_source);
 * It should be a Task to do to convert all that into a single function,
 * merging the content of those two function into a single self-consistent
 * function:
 *  sourceRedshift = redshiftAtDistance(CurrentRedshift, Distance);
 */

int RT_IntegralInvolatiles_setLuminosity(RT_SourceT Source,
                                         RT_ConstantsT const Constants,
                                         rt_float SourceRedshift,
                                         RT_IntegralInvolatilesT Involatiles) {
  /* Calculate the Luminosity at this redshift at this frequency */
  RT_SetSourceFunction(&Source);

  /* Re-cast the LuminosityFunction from void (*)(void *) */
  rt_float (*LuminosityFunction)(RT_LUMINOSITY_ARGS) =
      (rt_float(*)(RT_LUMINOSITY_ARGS))Source.LuminosityFunction;

  size_t i;
  for (i = 0; i < Involatiles.nFrequencies; ++i) {
    Involatiles.Luminosities[i] = (*(LuminosityFunction))(
        Involatiles.Frequencies[i], SourceRedshift, &Source, &Constants);
    Involatiles.Luminosities[i] /= Involatiles.Frequencies[i];
  }

  return EXIT_SUCCESS;
}
