/* Allocate memory for a RT_Data structure and associated memory. The memory is
 * cleared and the pointers are set to appropriate locations in the memory
 * space.
 *
 * ARGUMENTS
 *  NumCells   Number of cells in the line of sight.
 *
 * RETURNS
 *  Pointer to the RT_Data structure.
 *  NULL if failure.
 *
 * SEE ALSO
 *  RT_Data_Free, RT_Data_Assign
 *
 * AUTHOR: Eric Tittley
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

#if ((defined DEBUG) || (VERBOSE > 0)) && (defined RT_MPI)
#  include <mpi.h>
#endif

#include "RT_Data.h"

/*@null@*/ RT_Data *RT_Data_Allocate(size_t const NumCells) {
  int ierr;
  RT_Data *Data;
#if VERBOSE >= 1
  size_t AllocatedMemory = 0;
#endif
#if (defined DEBUG) || (VERBOSE > 0)
#  ifdef RT_MPI
  int me;
  (void)MPI_Comm_rank(MPI_COMM_WORLD, &me);
#  else
  const int me = 0;
#  endif
#endif

  /* Allocate memory for the RT_Data index */
#if VERBOSE >= 1
  AllocatedMemory += sizeof(RT_Data);
#endif
  Data = (RT_Data *)malloc(sizeof(RT_Data));
#ifdef DEBUG
  if (Data == NULL) {
    printf(
        "(%2i) ERROR: RT_Data_Allocate: %i: Failed to allocate memory for "
        "RT_Data.\n",
        me,
        __LINE__ - 4);
    (void)fflush(stdout);
    return NULL;
  }
#endif
#if VERBOSE > 1
  printf("(%2i) RT_Data_Allocate: %i: Allocated RT_Data: %i bytes @ %p\n",
         me,
         __LINE__,
         (int)sizeof(RT_Data),
         Data);
  (void)fflush(stdout);
#endif

  Data->NumCells = NumCells;
  Data->Cell = (size_t)0; /* Arbitrarily set */

  /* Calculate the amount of memory to allocate.
   * Assigning 8-bytes per void pointer, even if only 4 are needed,
   * makes the data more portable. */
  Data->NBytes =
      NumCells * ((size_t)RT_DATA_N_FIELDS_DOUBLE * sizeof(rt_float) +
                  (size_t)RT_DATA_N_FIELDS_POINTER * sizeof(void *)) +
      (size_t)RT_DATA_N_FIELDS_DOUBLE_REFINEMENTS * sizeof(rt_float);

  /* Allocate the memory space */
#if VERBOSE >= 1
  AllocatedMemory += Data->NBytes;
#endif
  Data->MemSpace = malloc(Data->NBytes);
#ifdef DEBUG
  if (Data->MemSpace == NULL) {
    printf(
        "(%2i) ERROR: %s: %i: Failed to allocate memory for RT_Data "
        "MemSpace.\n",
        me,
        __FILE__,
        __LINE__);
    printf("(%2i)  Was trying to allocate %lu bytes\n", me, Data->NBytes);
    (void)fflush(stdout);
    free(Data);
    return NULL;
  }
#endif
#if VERBOSE > 1
  printf("(%2i) %s: %i: Allocated MemSpace: %lu bytes @ %p\n",
         me,
         __FILE__,
         __LINE__,
         Data->NBytes,
         Data->MemSpace);
  (void)fflush(stdout);
#endif

  /* Set the pointers in Data to locations in the Memory Space. */
  ierr = RT_Data_Assign(Data);
#ifdef DEBUG
  if (ierr != EXIT_SUCCESS) {
    printf(
        "(%2i) ERROR: RT_Data_Allocate: %i: Failed to assign pointers for "
        "RT_Data.\n",
        me,
        __LINE__ - 4);
    (void)fflush(stdout);
    free(Data->MemSpace);
    free(Data);
    return NULL;
  }
#endif

  /* Clear the memory space.  NULL as a pointer is simply 0. */
  memset(Data->MemSpace, 0, (size_t)Data->NBytes);

#if VERBOSE >= 1
  printf("(%2i) RT_Data_Allocate: Allocated %i bytes.\n",
         me,
         (int)AllocatedMemory);
  (void)fflush(stdout);
#endif

  return Data;
}
