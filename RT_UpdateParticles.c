#include "RT.h"

void uP_kernel(rt_float const dt_RT,
               rt_float const ExpansionFactor,
               RT_ConstantsT const Constants,
               RT_RatesBlockT const Rates,
               /* out */ RT_Data const Data) {
  size_t cellindex;
  for (cellindex = 0; cellindex < Data.NumCells; cellindex++) {
    RT_UpdateCell_RateBlock(dt_RT,
                            ExpansionFactor,
                            cellindex,
                            Constants,
                            Rates,
                            /* updated */ Data);
  }
}

void RT_UpdateParticles(rt_float const dt_RT,
                        rt_float const Redshift,
                        RT_ConstantsT const Constants,
                        RT_RatesBlockT const Rates,
                        /* Updated */ RT_Data Data) {
  const rt_float ExpansionFactor = 1. / (Redshift + 1.0);

  uP_kernel(dt_RT,
            ExpansionFactor,
            Constants,
            Rates,
            /* out */ Data);
}
