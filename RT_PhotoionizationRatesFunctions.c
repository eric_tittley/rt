/* RT_PhotoionizationRateFunctions
 *  Evaluates photoionization and photoionization heating rates at a given
 *  frequency for HI, HeI, & HeII
 *
 *  int RT_PhotoionizationRatesFunctions(rt_float nu,
 *                                       rt_float N_H1,
 *                                       rt_float N_He1,
 *                                       rt_float N_He2,
 *                                       rt_float N_H1_cum,
 *                                       rt_float N_He1_cum,
 *                                       rt_float N_He2_cum,
 *                                       rt_float z,
 *                                       RT_SourceT *Source,
 *                                       RT_ConstantsT *Constants,
 *                                       rt_float Alpha[3],
 *                                       rt_float f[7] )
 *
 * ARGUMENTS
 *  Input, not modified
 *   nu                 Frequency at which to evalute the rates. [Hz]
 *   N_H1, N_He1, N_He2         Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum     Cumulative column densities between
 *                                      the source and the near edge of the
 *                                      cell.  [m^2]
 *   z          Redshift of source (used if the source is a function of time).
 *   Source     The source
 *   Alpha[3]   Cross-sections at nu: H1-H2, He1-He2, He2-He3
 *
 *  Output [Unattenuated]
 *   f[7]       Rates at frequency, nu. [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2]
 *              I_* are the photoionization rates (* h_p). [s^-1 Hz^-1]
 *              G_* are the heating rates (* h_p). [J s^-1 Hz^-1]
 *              Gamma_HI is the photoionization rate per particle [m^3 s^-1
 * Hz^-1]
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * NOTES
 *  There is a factor of h_p^-1 missing in f since it is
 *  more efficient to apply the factor after integrating.
 *  f/h_p is what you really want.
 *
 *  Also note that this function is the most heavily called within RT.
 *
 * CALLS
 *  Photoionization cross-sections:
 *   H1_H2()
 *   He1_He2()
 *   He2_He3()
 *  RT_Luminosity()    Source luminosity.
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif
#include <math.h>

#include "Epsilon.h"
#include "RT.h"
#include "RT_ConstantsT.h"

/* f = [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2] */

int RT_PhotoionizationRatesFunctions(rt_float const nu,
                                     rt_float const Luminosity,
                                     rt_float const N_H1,
                                     rt_float const N_He1,
                                     rt_float const N_He2,
                                     rt_float const N_H1_cum,
                                     rt_float const N_He1_cum,
                                     rt_float const N_He2_cum,
                                     RT_ConstantsT const* const Constants,
                                     rt_float const* const Alpha,
                                     /*@out@*/ rt_float* const f) {
#ifdef DEBUG
  if (!isfinite(N_H1) || N_H1 < 0. || !isfinite(N_He1) || N_He1 < 0. ||
      !isfinite(N_He2) || N_He2 < 0. || !isfinite(N_H1_cum) || N_H1_cum < 0. ||
      !isfinite(N_He1_cum) || N_He1_cum < 0. || !isfinite(N_He2_cum) ||
      N_He2_cum < 0.) {
    printf("ERROR: %s: %i: N_H1=%5.3e; N_He1=%5.3e; N_He2=%5.3e\n",
           __FILE__,
           __LINE__,
           N_H1,
           N_He1,
           N_He2);
    printf(" N_H1_cum=%5.3e; N_He1_cum=%5.3e; N_He2_cum=%5.3e\n",
           N_H1_cum,
           N_He1_cum,
           N_He2_cum);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  /* tau_i = The optical depth of the gas in the cell at a given frequency. */
  rt_float tau_1, tau_2, tau_3;
  tau_1 = Alpha[0] * N_H1;
  if (nu > Constants->nu_1) {
    tau_2 = Alpha[1] * N_He1;
    if (nu > Constants->nu_2) {
      tau_3 = Alpha[2] * N_He2;
    } else {
      tau_3 = 0.;
    }
  } else {
    tau_2 = 0.;
    tau_3 = 0.;
  }
#ifdef DEBUG
  if (!isfinite(tau_1) || tau_1 < 0.) {
    printf("ERROR: %s: %i: tau_1=%5.3e; Alpha[0]=%5.3e; N_H1=%5.3e;\n",
           __FILE__,
           __LINE__,
           tau_1,
           Alpha[0],
           N_H1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if (!isfinite(tau_2) || tau_2 < 0.) {
    printf("ERROR: %s: %i: tau_2=%5.3e; Alpha[1]=%5.3e; N_He1=%5.3e;\n",
           __FILE__,
           __LINE__,
           tau_2,
           Alpha[1],
           N_He1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if (!isfinite(tau_3) || tau_3 < 0.) {
    printf("ERROR: %s: %i: tau_3=%5.3e; Alpha[2]=%5.3e; N_He2=%5.3e;\n",
           __FILE__,
           __LINE__,
           tau_3,
           Alpha[2],
           N_He2);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  /*The transmission factor.  What fraction of light has reached this cell. */
  rt_float const Trans =
      EXP(-(Alpha[0] * N_H1_cum + Alpha[1] * N_He1_cum + Alpha[2] * N_He2_cum));
#ifdef DEBUG
  if (!isfinite(Trans) || Trans < 0) {
    printf("ERROR: %s: %i: Trans=%5.3e: Alpha=[%5.3e; %5.3e; %5.3e]\n",
           __FILE__,
           __LINE__ - 4,
           Trans,
           Alpha[0],
           Alpha[1],
           Alpha[2]);
    printf("   N_H1_cum=%5.3e N_He1_cum=%5.3e N_He2_cum=%5.3e\n",
           N_H1_cum,
           N_He1_cum,
           N_He2_cum);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  /* Normalization */
  rt_float const tau = tau_1 + tau_2 + tau_3;
  /* Edit: See notes 2014Mar28
   * (1.0 - EXP(-tau)) / tau Taylor Expands to 1-tau/2 for tau -> 0
   * Indeed, it is a very good approximation for tau < 0.01 (2x10^-5 fractional
   * error)
   */
  rt_float norm;
  if (tau >
      0.01) { /* Avoid expensive EXP() call and possible numerical errors */
    rt_float TransmissionThroughCell;
    if (tau > INV_LN_EPSILON) { /* -ln(EPSILON) */
      TransmissionThroughCell = 1.0;
    } else {
      TransmissionThroughCell = (1.0 - EXP(-tau));
    }
    norm = TransmissionThroughCell / tau;
  } else {
    norm = 1.0 - tau / 2.;
  }
#ifdef DEBUG
  if (!isnormal(norm) || norm < 0.) {
    printf(
        "ERROR: %s: %i: norm=%5.3e; TransmissionThroughCell=%5.3e; tau=%5.3e\n",
        __FILE__,
        __LINE__,
        norm,
        (1. - EXP(-tau)),
        tau);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  rt_float const L = Luminosity * Trans * norm;

  if (nu >= Constants->nu_0) {
    f[0] = L * tau_1;
    f[3] = f[0] * (nu - Constants->nu_0);
  } else {
    f[0] = 0.;
    f[3] = 0.;
  }
  if (Constants->Y > 0.) { /* Only evaluate I_He* and G_He* if there is He */
    if (nu >= Constants->nu_1) {
      f[1] = L * tau_2;
      f[4] = f[1] * (nu - Constants->nu_1);
    } else {
      f[1] = 0.;
      f[4] = 0.;
    }
    if (nu >= Constants->nu_2) {
      f[2] = L * tau_3;
      f[5] = f[2] * (nu - Constants->nu_2);
    } else {
      f[2] = 0.;
      f[5] = 0.;
    }
  } else {
    f[1] = 0.;
    f[2] = 0.;
    f[4] = 0.;
    f[5] = 0.;
  }

  /* Gamma_HI */
  if (nu >= Constants->nu_0) {
    f[6] = L * Alpha[0];
  } else {
    f[6] = 0.;
  }

#ifdef DEBUG
  size_t i;
  for (i = 0; i < 7; i++) {
    if (!isfinite(f[i]) || f[i] < 0.) {
      printf("ERROR: %s: %i: f[%lu]=%5.3e\n", __FILE__, __LINE__, i, f[i]);
      printf(
          " nu=%5.3e tau_1=%5.3e tau_2=%5.3e tau_3=%5.3e Trans=%5.3e "
          "norm=%5.3e\n",
          nu,
          tau_1,
          tau_2,
          tau_3,
          Trans,
          norm);
      (void)fflush(stdout);
      return EXIT_FAILURE;
    }
  }
#endif

  return EXIT_SUCCESS;
}
