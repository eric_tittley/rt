/*
 * PREPROCESSOR MACROS
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_Data.h"

void RT_Cell_Copy(RT_Cell const* const Cell_from,
                  /*@out@*/ RT_Cell* const Cell_to) {
  Cell_to->R = Cell_from->R;
  Cell_to->dR = Cell_from->dR;
  Cell_to->Density = Cell_from->Density;
  Cell_to->Entropy = Cell_from->Entropy;
  Cell_to->T = Cell_from->T;
  Cell_to->n_H = Cell_from->n_H;
  Cell_to->f_H1 = Cell_from->f_H1;
  Cell_to->f_H2 = Cell_from->f_H2;
  Cell_to->n_He = Cell_from->n_He;
  Cell_to->f_He1 = Cell_from->f_He1;
  Cell_to->f_He2 = Cell_from->f_He2;
  Cell_to->f_He3 = Cell_from->f_He3;
  Cell_to->column_H1 = Cell_from->column_H1;
  Cell_to->column_He1 = Cell_from->column_He1;
  Cell_to->column_He2 = Cell_from->column_He2;
#ifdef RT_DATA_VELOCITIES
  Cell_to->v_z = Cell_from->v_z;
  Cell_to->v_x = Cell_from->v_x;
#endif
}
