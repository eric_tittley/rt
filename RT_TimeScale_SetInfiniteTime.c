#include "RT_TimeScale.h"

void RT_TimeScale_SetInfiniteTime(RT_TimeScale* dt_RT_sugg) {
  dt_RT_sugg->Entropy = 1e20; /* Set to an absurdly large value: 3 Tyr */
  dt_RT_sugg->I_H1 = 1e20;    /* Set to an absurdly large value: 3 Tyr */
  dt_RT_sugg->I_He1 = 1e20;   /* Set to an absurdly large value: 3 Tyr */
  dt_RT_sugg->I_He2 = 1e20;   /* Set to an absurdly large value: 3 Tyr */
  dt_RT_sugg->R_H1 = 1e20;    /* Set to an absurdly large value: 3 Tyr */
  dt_RT_sugg->R_He1 = 1e20;   /* Set to an absurdly large value: 3 Tyr */
  dt_RT_sugg->R_He2 = 1e20;   /* Set to an absurdly large value: 3 Tyr */
}
