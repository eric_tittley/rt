/* isequal: isequal tests the equivalence of two floats (double or single)
 *          depending on RT_DOUBLE.
= *
 * ARGUMENTS
 *  n, d    Two numbers to compare.
 *
 * RETURNS
 *  TRUE if n is equal to d to within machine precision.
 *  FALSE if n is not equal to d to withing machine precision.
 *
 * AUTHOR: Eric Tittley
 *
 */

#include "isequal.h"

#include "Epsilon.h"
#include "Logic.h"
#include "RT_Precision.h"

bool isequal(rt_float const n, rt_float const d) {
  if ((d < 1.0) && (n > d * FLOAT_MAX)) return FALSE;
  if (((1.0 - EPSILON) < (n / d)) && ((n / d) < (1.0 + EPSILON))) {
    return TRUE;
  } else {
    return FALSE;
  }
}
