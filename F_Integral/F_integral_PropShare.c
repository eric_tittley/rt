/* F_integral_PropShare: Evaluates photoionisation and photoionisation heating rates at
 *                       a given frequency for HI, HeI, & HeII
 *
 *  int F_integral_PropShare(double nu,
 *                 double N_H1, double N_He1, double N_He2,
 *                 double N_H1_cum, double N_He1_cum, double N_He2_cum,
 *                 double z, double Alpha[3], double f[6] )
 *
 * ARGUMENTS
 *  Input, not modified
 *   nu                             Frequency at which to evalute the rates. [Hz]
 *   N_H1, N_He1, N_He2             Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum Cumulative column densities between
 *                                  the source and the near edge of the
 *                                  cell.  [m^2]
 *   z                              Redshift (used if the source is a function of time).
 *   Alpha[3]                       Cross-sections at nu: H1-H2, He1-He2, He2-He3
 *
 *  Output
 *   f[6]    Rates at frequency, nu. [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2]
 *           I_* are the photoionisation rates (* h_p). [s^-1 Hz^-1]
 *           G_* are the heating rates (* h_p). [J s^-1 Hz^-1]
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * EXTERNAL MACROS AND VARIABLES
 *  nu_1       HI   ionisation threshold. [Hz] (double)
 *  nu_2       HeI  ionisation threshold. [Hz] (double)
 *  nu_3       HeII ionisation threshold. [Hz] (double)
 *  Y
 *
 * NOTES
 *  There is a factor of h_p^-1 missing in f since it is
 *  more efficient to apply the factor after integrating.
 *  f/h_p is what you really want.
 *
 *  Also note that this function is the most heavily called within RT.
 *
 * CALLS
 *  Photoionisation cross-sections:
 *   H1_H2()
 *   He1_He2()
 *   He2_He3()
 *  RT_Luminosity()    Source luminosity.
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  12 04 03 First version, based on F_integral.c
 */

#include "config.h"

/* DEBUG is expensive in this routine, adding 6% to the total RT runtime. */
#ifdef DEBUG
#undef DEBUG
#endif

#include <stdlib.h>
#ifdef DEBUG
#include <stdio.h>
#endif
#include <math.h>

#ifndef __DBL_MIN__
#define __DBL_MIN__ 2.2250738585072014e-308
#endif
#ifndef __DBL_MAX__
#define __DBL_MAX__ 1.7976931348623157e+308
#endif

#include "RT.h"
#include "RT_constants_cosmology.h"
#include "RT_constants_physical.h"

/* f = [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2] */

int F_integral_PropShare(const double nu, const double N_H1, const double N_He1,
               const double N_He2, const double N_H1_cum,
               const double N_He1_cum, const double N_He2_cum,
               const double z, const double *Alpha, /*@out@*/ double *f )
{
 double tau_1, tau_2, tau_3;
 double Trans, norm;
 double enumu, denom;
 double L;
 double e1,e2,e3;
#ifdef DEBUG
 int i;
#endif

#ifdef DEBUG
 if(   !isfinite(N_H1)      || N_H1<0.
    || !isfinite(N_He1)     || N_He1<0.
    || !isfinite(N_He2)     || N_He2<0.
    || !isfinite(N_H1_cum)  || N_H1_cum<0.
    || !isfinite(N_He1_cum) || N_He1_cum<0.
    || !isfinite(N_He2_cum) || N_He2_cum<0. ) {
  printf("ERROR: F_integral: %i: N_H1=%5.3e; N_He1=%5.3e; N_He2=%5.3e\n",
         __LINE__,N_H1,N_He1,N_He2);
  printf(" N_H1_cum=%5.3e; N_He1_cum=%5.3e; N_He2_cum=%5.3e\n",
         __LINE__,N_H1_cum,N_He1_cum,N_He2_cum);
  (void)fflush(stdout);
  return EXIT_FAILURE
 }
#endif

 /* k_i   = The photo-ionisation cross sections
  * tau_i = The optical depth of the gas in the cell at a given frequency.
  * e_i   = The exponentials, which are expensive to calculate. */
 tau_1 = Alpha[0] * N_H1;
 e1 = exp(-tau_1);
 if (nu > nu_1) {
  tau_2 = Alpha[1] * N_He1;
  e2 = exp(-tau_2);
  if (nu > nu_2) {
   tau_3 = Alpha[2] * N_He2;
   e3 = exp(-tau_3);
  } else {
  #ifdef DEBUG
   tau_3 = 0.;
  #endif
   e3    = 1.;
  }
 } else {
 #ifdef DEBUG
  tau_2 = 0.;
  tau_3 = 0.;
 #endif
  e2    = 1.;
  e3    = 1.;
 }
#ifdef DEBUG
 if( !isfinite(tau_1) || tau_1 < 0 ) {
  printf("ERROR: F_integral: tau_1=%5.3e; Alpha[0]=%5.3e; N_H1=%5.3e;\n",
         tau_1,Alpha[0],N_H1);
  (void)fflush(stdout);
  return EXIT_FAILURE
 }
 if( !isfinite(tau_2) || tau_2 < 0 ) {
  printf("ERROR: F_integral: tau_2=%5.3e; Alpha[1]=%5.3e; N_He1=%5.3e;\n",
         tau_2,Alpha[1],N_He1);
  (void)fflush(stdout);
  return EXIT_FAILURE
 }
 if( !isfinite(tau_3) || tau_3 < 0 ) {
  printf("ERROR: F_integral: tau_3=%5.3e; Alpha[2]=%5.3e; N_He2=%5.3e;\n",
         tau_3,Alpha[2],N_He2);
  (void)fflush(stdout);
  return EXIT_FAILURE
 }
#endif

 /*The transmission factor.  What fraction of light has reached this cell. */
 Trans = exp(-(  Alpha[0] * N_H1_cum
               + Alpha[1] * N_He1_cum
               + Alpha[2] * N_He2_cum ));
#ifdef DEBUG
 if ( !isfinite(Trans) || Trans<0 ) {
  printf("ERROR: F_integral: %i: Trans=%5.3e: Alpha=[%5.3e; %5.3e; %5.3e]\n",
         __LINE__-4,Trans, Alpha[0], Alpha[1], Alpha[2]); 
  printf("   N_H1_cum=%5.3e N_He1_cum=%5.3e N_He2_cum=%5.3e\n",
         N_H1_cum, N_He1_cum, N_He2_cum);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* Normalization */
 /* enumu and denom are always between 0 and 1
  * There are only two singularities (denom -> 0) :
  * As e1, e2, e3 -> 1, norm -> 1
  * As e1, e2, e3 -> 0, norm -> 1/(e2e3+e1e3+e1e2) which is divergent as
  *  1/(3\delta^2)
  * If we trip the 2nd case, it means the cell's optical depth is too great
  * for the approximations we are using, anyway.
  * See notes 100823*/
 enumu = (1.0 - e1*e2*e3);
 denom =  (1.0 - e1) + (1.0 - e2) + (1.0 - e3) ;
 if ( denom > __DBL_EPSILON__ ) {
  norm = enumu / denom;
 } else {
  /* All absorptions are zero */
  norm = 0.0;
 }
#ifdef DEBUG
 if ( !isnormal(norm) || norm<0. ) {
  printf("ERROR: F_integral: %i: norm=%5.3e. enumu=%e; denom=%e\n",
         __LINE__,enumu, denom);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 L = RT_Luminosity(nu, z)/nu * Trans;
 /* Because norm can be very large when the (1-e_i)e_j e_k 's are small
  * it is crucial (numerically) to multiply norm by the small values.
  * I.e. don't pull norm out and place it in the previous line.
  */
 if(nu>=nu_0) {
  f[0] = L * ((1.0 - e1 ) * norm);
  f[3] = f[0]*(nu - nu_0);
 } else {
  f[0] = 0.;
  f[3] = 0.;
 }
 if( Y > 0 ) { /* Only evaluate I_He* and G_He* if there is He */
  if(nu>=nu_1) {
   f[1] = L * ((1.0 - e2) * norm);
   f[4] = f[1]*(nu - nu_1);
  } else {
   f[1] = 0.;
   f[4] = 0.;
  }
  if(nu>=nu_2) {
   f[2] = L * ((1.0 - e3) * norm);
   f[5] = f[2]*(nu - nu_2);
  } else {
   f[2] = 0.;
   f[5] = 0.;
  }
 } else {
  f[1] = 0.;
  f[2] = 0.;
  f[4] = 0.;
  f[5] = 0.;
 }


#ifdef DEBUG
 for(i=0;i<6;i++) {
  if( !isfinite(f[i]) || f[i]<0 ) {
   printf("ERROR: F_integral: %i: f[%i]=%5.3e\n", __LINE__-4,i,f[i]);
   printf(" nu=%5.3e tau_1=%5.3e tau_2=%5.3e tau_3=%5.3e Trans=%5.3e norm=%5.3e z=%6.2f\n",
          nu, tau_1, tau_2, tau_3, Trans, norm, z);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 }
#endif

 return EXIT_SUCCESS;
}

