#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math.h>

#include "RT.h"

rt_float RT_TemperatureFromEntropy(RT_ConstantsT const Constants,
                                   rt_float const Entropy,
                                   size_t const iCell,
                                   RT_Data const Data) {
  /* Update the neutral & ionised number densities. */
  rt_float const n_H2 = Data.n_H[iCell] * Data.f_H2[iCell];
  rt_float const n_He2 = Data.n_He[iCell] * Data.f_He2[iCell];
  rt_float const n_He3 = Data.n_He[iCell] * Data.f_He3[iCell];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2. * n_He3);

  /* Total number density */
  rt_float const n = Data.n_H[iCell] + Data.n_He[iCell] + n_e;

  rt_float const mu = (Data.n_H[iCell] + 4. * Data.n_He[iCell]) / n;

  /* Note the scaling of the density
   * 10^( 0 + (-27 - -23) + 22 -2/3*22 + (2/3)*(-28+22) )
   * 10^(     -3          + 7          - 4)
   * 10^( 0 )
   * 1
   */
  rt_float const Temperature =
      mu * (Constants.mass_p / Constants.k_B) * Entropy *
      POW(Constants.Mpc, 1.0 - Constants.gamma_ad) *
      POW(Data.Density[iCell] * Constants.Mpc, Constants.gamma_ad - 1.0);
#ifdef DEBUG
  if (!(bool)isfinite((Temperature))) {
    printf(
        "ERROR: %s: %i: Temperature=%5.3e; mu=%5.3e; Entropy=%5.3e; "
        "Density=%5.3e\n",
        __FILE__,
        __LINE__,
        Temperature,
        mu,
        Entropy,
        Data.Density[iCell]);
    (void)fflush(stdout);
    return -1.0;
  }
#endif

  return Temperature;
}
