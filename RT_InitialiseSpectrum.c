/* RT_InitialiseSpectrum: Initialise global variables pertaining to the source
 * spectrum.
 *
 * RT_InitialiseSpectrum(me)
 *
 * ARGUMENTS
 *  me                   The process number (int const)
 *  RT_SourceT * Source
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * NOTES
 *  Code does nothing if no pre-processor directives are set.
 *
 * PREPROCESSOR MACROS
 *  SOURCE_NORMALISE_SPECTRUM
 *  SOURCE_SB_QSO_HYBRID
 *  SOURCE_TABLE
 *
 * AUTHOR: Eric Tittley
 */

/* WARNING!!!!! This routine is a mess. It was designed to set global
 * variables.  These globals are now in RT_SourceT Source
 * I don't have time to fix this for all cases.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_CONFIG
#  include "config.h"
#endif
#undef VERBOSE
#define VERBOSE 1

#include "Epsilon.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Luminosity.h"
#include "RT_SourceT.h"

#ifndef MASTER
#  define MASTER 0
#endif

int RT_InitialiseSpectrum(int const me,
                          RT_ConstantsT const Constants,
                          RT_SourceT* const Source) {
#ifdef SOURCE_NORMALISE_SPECTRUM
  rt_float L_0_new;
#endif
  rt_float sum = 0.0, nu, dnu;

#ifdef SOURCE_TABLE
  /* Read in the source spectrum table */
  /* The following global variables are set:
   * source_N, source_nu, source_f,
   * nu_end, nu_end_table (both if necessary) */
  if (RT_LoadSourceTable(me, Source) == EXIT_FAILURE) {
    printf("ERROR: %s: %i: Unable to read source spectrum file: %s.\n",
           __FILE__,
           __LINE__,
           SOURCE_TABLE);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#  if (defined SOURCE_NORMALISE_SPECTRUM) && (defined SOURCE_SB_QSO_HYBRID)
  /* Note that if SOURCE_HYBRID is defined (which implies SOURCE_TABLE)
   * then ne_end must be 1e20, not nu_end_table. */
  Source->nu_end = 1e20;
#  endif
#else /* else not SOURCE TABLE */
#  ifdef SOURCE_NORMALISE_SPECTRUM
  /* nu_end is used if Gaussian Quadrature is *not* used,
   * or if Normalisation is done. */
  Source->nu_end = 1e20;
#  endif
#endif /* endif SOURCE_TABLE else */

#ifdef SOURCE_NORMALISE_SPECTRUM
#  if VERBOSE >= 1
  printf("(%2i) %s: Normalising spectrum...\n", me, __FILE__);
  (void)fflush(stdout);
#  endif
  /* Normalise the source spectrum to produce N_H_Ionising_photons_per_sec
   * at a redshift in the middle of the source's lifetime. */
  if (me == MASTER) {
#  ifdef SOURCE_SB_QSO_HYBRID
    /* In the case of the hybrid spectrum, we need to rescale the spectra. */
    Source->L_0_SB = Source->L_0 / Source->SB_L_nu_0;
    Source->L_0_PL = Source->L_0 * Source->QSO_SB_L_factor *
                     (Source->alpha * Source->SB_N_photons);
    printf(
        " Setting internal Luminosities to L_0_SB = %5.3e; L_0_PL = %5.3e;\n",
        L_0_SB,
        L_0_PL);
#  endif
    dnu = FMAX(1e12, Source->nu_end * (10.0 * EPSILON));
    rt_float Redshift = (Source->z_on + Source->z_off) / 2.0;
    /* Re-cast the LuminosityFunction from void (*)(void *) */
    rt_float (*LuminosityFunction)(RT_LUMINOSITY_ARGS) =
        (rt_float(*)(RT_LUMINOSITY_ARGS))Source->LuminosityFunction;
    sum = 0.0;
    for (nu = Constants.nu_0; nu < Source->nu_end; nu += dnu) {
      sum += (*(LuminosityFunction))(nu, Redshift, Source) /
             (nu * Constants.h_p * PETA); /* petaN per Hz */
    }
    sum = sum * dnu; /* petaN */
    /* The parameter N_H_Ionising_photons_per_sec should be
     * petaN_H_Ionising_photons_per_sec to keep it within the range of floats */
    L_0_new =
        Source->L_0 * ((Source->N_H_Ionising_photons_per_sec / PETA) / sum);
    printf("%s: Renormalising L_0 from %6.4e to %6.4e W/Hz\n",
           __FILE__,
           Source->L_0,
           L_0_new);
    (void)fflush(stdout);
    Source->L_0 = L_0_new;
  }
  MPI_Bcast(&(Source->L_0), 1, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
#  if VERBOSE >= 1
  printf("(%2i) %s: Done normalising spectrum.\n", me, __FILE__);
  (void)fflush(stdout);
#  endif
#endif

#ifdef SOURCE_SB_QSO_HYBRID
  /* In the case of the hybrid spectrum, we need to rescale the spectra.
   * L_0_SB & L_0_PL are globals externally declared in constants.c */
  Source->L_0_SB = Source->L_0 / Source->SB_L_nu_0;
  Source->L_0_PL = Source->L_0 * Source->QSO_SB_L_factor *
                   (Source->alpha * Source->SB_N_photons);
  if (me == MASTER) {
    printf(" L_0_SB * SB_L_nu_0 = %e (calculated)\n",
           Source->L_0_SB * Source->SB_L_nu_0);
    printf(" L_0_PL = %e (calculated)\n", Source->L_0_PL);
    (void)fflush(stdout);
  }
#endif

#ifdef SOURCE_MONOCHROMATIC
  Source->L_0 = Source->N_H_Ionising_photons_per_sec * Constants.h_p *
                Constants.nu_0 / delta_nu;
  if (me == MASTER) {
    printf("%s: L_0=%5.3e\n", __FILE__, L_0);
    printf(
        "%s: N_dot=%5.3e which should equal "
        "N_H_Ionising_photons_per_sec=%5.3e\n",
        __FILE__,
        PhotonFlux(Constants.nu_0,
                   Constants.nu_1,
                   0.,
                   Constants.NLevels[0],
                   *Source,
                   Constants),
        Source->N_H_Ionising_photons_per_sec);
    (void)fflush(stdout);
  }
#endif

  if (me == MASTER) {
    /* Calculate the number of ionizing photons per second */
    if (Source->nu_end < Constants.nu_0) {
      Source->nu_end = 50000.0 * Constants.nu_0;
    }
    dnu = FMAX(1e12, Source->nu_end * (10.0 * EPSILON));
    printf("nu_end=%5.3e; nu_0=%5.3e; dnu=%5.3e; \n",
           Source->nu_end,
           Constants.nu_0,
           dnu);
    rt_float Redshift = (Source->z_on + Source->z_off) / 2.0;
    /* Re-cast the LuminosityFunction from void (*)(void *) */
    rt_float (*LuminosityFunction)(RT_LUMINOSITY_ARGS) =
        (rt_float(*)(RT_LUMINOSITY_ARGS))Source->LuminosityFunction;
    sum = 0.0;
    for (nu = Source->nu_end; nu >= Constants.nu_0; nu -= dnu) {
      sum += (*(LuminosityFunction))(nu, Redshift, Source, &Constants) /
             (nu * Constants.h_p * 1e30); /* PetaN per Hz */
      if (!isfinite(sum)) printf("nu=%5.3e; sum=%5.3e\n", nu, sum);
    }
    sum *= dnu; /* N */
    printf("Between nu = %5.3e and %5.3e,\n", Constants.nu_0, Source->nu_end);
    printf(
        "Source generates %5.3e x 1e30 ionizing photons per second at "
        "z=%5.3e\n",
        sum,
        Redshift);
    printf("hopefully to within 1%%.\n");
    printf(
        " Note: not using the same integration scheme as used in the code\n");
    (void)fflush(stdout);
  }

  return EXIT_SUCCESS;
}
