#include <stdio.h>
#include <stdlib.h>

#include "GL_weights.h"
#include "RT.h"
#include "RT_IntegralInvolatilesT.h"

/* Convert span [-1,1] to [a,b] */
rt_float GaussLegendreXTrans(rt_float const t,
                             rt_float const a,
                             rt_float const b) {
#if 0
 return (a+b)/2. + t*(b-a)/2;
#else
  return a + (b - a) * (1 + t) / 2.;
#endif
}

int RT_IntegralInvolatiles_setFrequenciesWeightsAlphas(
    RT_ConstantsT const Constants,
    RT_IntegralInvolatilesT Involatiles) {
  /* Catch the monochromatic case */
  if ((Constants.NLevels_NIntervals == 1) & (Constants.NLevels[0] == 1)) {
    Involatiles.Weights[0] = 1.0;
    printf(
        "WARNING: %s: %i: Monochromatic case. You need to set frequency, "
        "luminosity, and alphas elsewhere.\n",
        __FILE__,
        __LINE__);
    /* TODO: if Source is passed in, we can set the frequency, luminosity, and
     * alphas here */
    return EXIT_SUCCESS;
  }

  /* I'm going to assume that there are three intervals:
   *  nu_0 to nu_1, nu_1 to nu_2, and nu_2 to inf */

  size_t i;
  size_t skip = 0;

  rt_float const* Xs;
  rt_float const* W;

  rt_float Scale;

  /* FIRST INTERVAL */

  /* Associate the sample positions and weights with tabulated values */
  switch (Constants.NLevels[0]) {
    case 2:
      Xs = Xs_02_Legendre;
      W = W_02_Legendre;
      break;
    case 4:
      Xs = Xs_04_Legendre;
      W = W_04_Legendre;
      break;
    case 8:
      Xs = Xs_08_Legendre;
      W = W_08_Legendre;
      break;
    case 16:
      Xs = Xs_16_Legendre;
      W = W_16_Legendre;
      break;
#if 0
  case  32: Xs = Xs_32_Legendre; W = W_32_Legendre; break;
  case  64: Xs = Xs_64_Legendre; W = W_64_Legendre; break;
  case 128: Xs = Xs_128_Legendre; W = W_128_Legendre; break;
  case 256: Xs = Xs_256_Legendre; W = W_256_Legendre; break;
#endif
    default:
      printf("ERROR: %s: %i: Unknown level NLevels[0]=%lu\n",
             __FILE__,
             __LINE__,
             Constants.NLevels[0]);
      return EXIT_FAILURE;
  }
  Scale = (Constants.nu_1 - Constants.nu_0) / 2.;
  for (i = 0; i < Constants.NLevels[0]; i++) {
    Involatiles.Frequencies[skip + i] =
        GaussLegendreXTrans(Xs[i], Constants.nu_0, Constants.nu_1);
    Involatiles.Weights[skip + i] = W[i] * Scale;
  }
  skip += Constants.NLevels[0];

  /* SECOND INTERVAL */

  /* Associate the sample positions and weights with tabulated values */
  switch (Constants.NLevels[1]) {
    case 2:
      Xs = Xs_02_Legendre;
      W = W_02_Legendre;
      break;
    case 4:
      Xs = Xs_04_Legendre;
      W = W_04_Legendre;
      break;
    case 8:
      Xs = Xs_08_Legendre;
      W = W_08_Legendre;
      break;
    case 16:
      Xs = Xs_16_Legendre;
      W = W_16_Legendre;
      break;
#if 0
  case  32: Xs = Xs_32_Legendre; W = W_32_Legendre; break;
  case  64: Xs = Xs_64_Legendre; W = W_64_Legendre; break;
  case 128: Xs = Xs_128_Legendre; W = W_128_Legendre; break;
  case 256: Xs = Xs_256_Legendre; W = W_256_Legendre; break;
#endif
    default:
      printf("ERROR: %s: %i: Unknown level NLevels[1]=%lu\n",
             __FILE__,
             __LINE__,
             Constants.NLevels[1]);
      return EXIT_FAILURE;
  }
  Scale = (Constants.nu_2 - Constants.nu_1) / 2.;
  for (i = 0; i < Constants.NLevels[1]; i++) {
    Involatiles.Frequencies[skip + i] =
        GaussLegendreXTrans(Xs[i], Constants.nu_1, Constants.nu_2);
    Involatiles.Weights[skip + i] = W[i] * Scale;
  }
  skip += Constants.NLevels[1];

  /* THIRD INTERVAL */

  /* Associate the sample positions and weights with tabulated values */
  /* Associate the sample positions and weights with tabulated values */
  switch (Constants.NLevels[2]) {
    case 2:
      Xs = Xs_02_Laguerre;
      W = W_02_Laguerre;
      break;
    case 4:
      Xs = Xs_04_Laguerre;
      W = W_04_Laguerre;
      break;
    case 8:
      Xs = Xs_08_Laguerre;
      W = W_08_Laguerre;
      break;
    case 16:
      Xs = Xs_16_Laguerre;
      W = W_16_Laguerre;
      break;
#if 0
  case  32: Xs = Xs_32_Laguerre; W = W_32_Laguerre; break;
  case  64: Xs = Xs_64_Laguerre; W = W_64_Laguerre; break;
  case 128: Xs = Xs_128_Laguerre; W = W_128_Laguerre; break;
  case 256: Xs = Xs_256_Laguerre; W = W_256_Laguerre; break;
#endif
    default:
      printf("ERROR: %s: %i: Unknown level NLevels[2]=%lu\n",
             __FILE__,
             __LINE__,
             Constants.NLevels[2]);
      (void)fflush(stdout);
      return EXIT_FAILURE;
  }
  Scale = Constants.nu_2;
  for (i = 0; i < Constants.NLevels[2]; i++) {
    Involatiles.Frequencies[skip + i] = Constants.nu_2 * (Xs[i] + 1.0);
    Involatiles.Weights[skip + i] = W[i] * Scale;
  }
  skip += Constants.NLevels[2];

  if (skip != Involatiles.nFrequencies) {
    printf(
        "ERROR: %s: %i: number of frequecies incompatibility: skip=%lu; "
        "nFrequencies=%lu\n",
        __FILE__,
        __LINE__,
        skip,
        Involatiles.nFrequencies);
    return EXIT_FAILURE;
  }

  /* END OF INTERVALS */

  /* Set the absorption cross sections (sigma) at these frequencies of HI, HeI,
   * and HeII */
  for (i = 0; i < Involatiles.nFrequencies; ++i) {
    rt_float const nu = Involatiles.Frequencies[i];
    skip = i * Involatiles.nSpecies; /* nSpecies must be hard-wired as 3 */
    Involatiles.Alphas[skip + 0] = H1_H2(nu, Constants);
    if (nu > Constants.nu_1) {
      Involatiles.Alphas[skip + 1] = He1_He2(nu, Constants);
      if (nu > Constants.nu_2) {
        Involatiles.Alphas[skip + 2] = He2_He3(nu, Constants);
      } else {
        Involatiles.Alphas[skip + 2] = 0.;
      }
    } else {
      Involatiles.Alphas[skip + 1] = 0.;
      Involatiles.Alphas[skip + 2] = 0.;
    }
  }

  return EXIT_SUCCESS;
}
