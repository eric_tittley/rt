/* Linearly interpolate the density (or any other scalar field )at a position r
 * from a density grid calculated by, for example, calc_density.
 *
 * ARGUMENTS
 *  r         The centre of the cell to which to interpolate.
 *  density   The density grid.
 *  N         The number of elements in the density grid.
 *  Ro        The lower edge of the region density spans
 *  width     The width of the region density spans.
 *
 * RETURNS
 *  A density in units of input density grid, or -1.0 if out of bounds or
 *  other error.
 *
 * NOTE
 *  Linear interpolation is conservative, which is necessary.
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  05 07 06 First version.
 *  05 07 14 Return density[N-1] if i_hi==N, not density[N], which is beyond
 *           array limits.
 *  06 03 20 Added extra error checking.
 *           Modified logic when r is outside the usable range for
 * interpolation. 06 06 29 Test and deal with the case where i_real = N-1.  This
 * used to produce i_lo == N-1 and i_hi == N which is out of bounds. 07 04 16
 * Renamed RT_InterpolateFromGrid (was RT_interpolate_density) Made more
 * general.  Any scalar field can now be interpolated. Now cannot enforce
 * minimum density. Don't need to include constants.h
 */
#include "config.h"

#ifdef DEBUG
#  include <stdio.h>
#endif

#include <math.h>

#include "RT.h"

rt_float RT_InterpolateFromGrid(rt_float const r,
                                float const* const density,
                                size_t const N,
                                rt_float const Ro,
                                rt_float const width) {
  int i_lo, i_hi;
  rt_float i_real, rho;

  /* Convert r postion to index position */
  i_real = (rt_float)N * (r - Ro) / width - 0.5;

  /* If outside the usable range, return the end values. */
  if (i_real < 0) return density[0];
  if (i_real >= (rt_float)(N - 1)) return density[N - 1];

  /* Find the elements bracketing our position */
  i_lo = (int)FLOOR(i_real);
  i_hi = i_lo + 1;

  /* if i_real is on a cell boundary, it is fine if either
   * i_lo == i_real  or i_hi == i_real.  Except i_hi can't be
   * > N.  So fix if that is the case.
   */
  if (i_lo == N - 1) {
    i_lo = N - 2;
    i_hi = N - 1;
  }

  /* Sometimes rounding errors make i_real = 0 - epsilon */
  if (i_lo == -1) i_lo = 0;

#ifdef DEBUG
  if ((i_lo < 0) || (i_hi >= N)) {
    printf("ERROR: RT_InterpolateFromGrid: r out of bounds.\n");
    printf("       r=%e gives i=%f; i_lo=%i; i_hi=%i\n", r, i_real, i_lo, i_hi);
    return -1.0;
  }
#endif

  /* Linearly interpolate */
  rho = density[i_lo] +
        (i_real - (rt_float)i_lo) * (density[i_hi] - density[i_lo]);

  return rho;
}
