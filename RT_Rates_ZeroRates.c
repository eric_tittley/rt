/* RT_Rates_ZeroRates: Set rates to zero prior to accumulation.
 *
 * ARGUMENTS
 *  nParticles  The number of rates in each vector
 *  Rates       RT_RatesBlockT structure on the host whose elements are pointers
 * to global memory spaces on the device. double *I_H1 double *I_He1 double
 * *I_He2 double *G_H1 double *G_He1 double *G_He2
 *
 * RETURNS
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include "RT_Precision.h"
#include "RT_RatesT.h"

void RT_Rates_ZeroRates(RT_RatesBlockT Rates) {
  size_t nParticles = Rates.NumCells;

#pragma omp parallel
  {
#pragma omp sections
    {
#pragma omp section
      memset(Rates.I_H1, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.I_He1, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.I_He2, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.G, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.G_H1, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.G_He1, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.G_He2, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.alpha_H1, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.alpha_He1, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.alpha_He2, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.L, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.Gamma_HI, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.n_HI_Equilibrium, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.n_HII_Equilibrium, 0, sizeof(rt_float) * nParticles);
#pragma omp section
      memset(Rates.TimeScale, 0, sizeof(rt_float) * nParticles);
    } /* end of sections block */
  }   /* end of parallel block */
}
