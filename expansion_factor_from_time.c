/* expansion_factor_from_time: The expansion factor from the age of the
 *                             universe.
 *
 * rt_float expansion_factor_from_time(rt_float t)
 *
 * ARGUMENTS
 *  Input, not modified
 *   t   The age of the universe. [s]
 *
 * RETURNS
 *   The expansion factor.
 *
 * EXTERNAL MACROS
 *  DEBUG   Defaults to undefined.
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#ifdef DEBUG
#  include <stdio.h>
#endif

#include <math.h>
#ifdef S_SPLINT_S
extern rt_float CBRT(rt_float);
extern int isfinite(rt_float);
#endif

#include "RT.h"
#include "RT_ConstantsT.h"
#include "isequal.h"

rt_float expansion_factor_from_time(rt_float const t,
                                    RT_ConstantsT const Constants) {
  rt_float const H_0 = Constants.h * 1e5 / Constants.Mpc; /* s^{-1} */
  rt_float a_local, a_term_1, a_term_2;
  /* if Omega_m is unity, else */
  if (isequal(Constants.omega_m, 1.0)) {
    a_local = POW(1.5 * t * H_0, 2. / 3.);
  } else {
    /* a_term_1 = POW(omega_m/(1.-omega_m),1./3.); */
    a_term_1 = CBRT(Constants.omega_m / (1. - Constants.omega_m));
    a_term_2 =
        POW(SINH(3. / 2. * H_0 * SQRT(1. - Constants.omega_m) * t), 2. / 3.);
    a_local = a_term_1 * a_term_2;
  }
#ifdef DEBUG
  if (!(bool)isfinite(a_local)) {
    printf(
        "ERROR: expansion_factor_from_time: %i: a=%f; H_0=%5.3e; "
        "omega_m=%5.3e, t=%5.3e\n",
        __LINE__ - 5,
        a_local,
        H_0,
        Constants.omega_m,
        t);
    (void)fflush(stdout);
  }
#endif

  return a_local;
}
