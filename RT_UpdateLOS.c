/* RT_UpdateLOS: Update Entropy and Ionization states for the next iteration
 *               for all elements in a LOS.
 *
 * ierr = RT_UpdateLOS(rt_float const dt_RT, rt_float const D,
 *                      rt_float const aa, RT_RatesT *Rates, RT_Cell *Data_cell
 * );
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  dt_RT
 *  D
 *  aa
 *  Rates.I_H1
 *  Rates.I_He1
 *  Rates.I_He2
 *  Rates.alpha_H1
 *  Rates.alpha_He1
 *  Rates.alpha_He2
 *  Data_cell.n_H
 *  Data_cell.n_He
 *  Data_cell.f_H1
 *  Data_cell.f_H2
 *  Data_cell.f_He1
 *  Data_cell.f_He2
 *  Data_cell.f_He3
 *  Data_cell.Entropy
 *
 * ARGUMENTS INPUT AND MODIFIED
 *  n_H1
 *  n_H2
 *  n_He1
 *  n_He2
 *  n_He3
 *  Entropy
 *
 * ARGUMENTS INITIALISED
 *  f_*         Ionization fractions of various species.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * PREPROCESSOR MACROS
 *  DEBUG
 *  VERBOSE
 *  ENFORCE_MINIMUM_CMB_TEMPERATURE
 *
 * SEE ALSO
 *  RT_UpdateCell does the same for a single Cell structure
 *  (RT_UpdateLOS differs by updating a structure of arrays, instead of an
 *   element of an array of structures)
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define VERBOSE 0
#  define ENFORCE_MINIMUM_CMB_TEMPERATURE 2.0
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif
#include <math.h>

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_RatesT.h"

int RT_UpdateLOS(rt_float const dt_RT,
                 rt_float const ExpansionFactor,
                 RT_ConstantsT const Constants,
                 RT_RatesT const* const Rates,
                 RT_Data const Data) {
#if VERBOSE > 2
  printf("%s: Updating...\n", __FILE__);
  (void)fflush(stdout);
#endif

  size_t iCell;
#pragma omp parallel for private(iCell)
  for (iCell = 0; iCell < Data.NumCells; ++iCell) {
    RT_UpdateLOSElement(
        iCell, dt_RT, ExpansionFactor, Constants, &(Rates[iCell]), Data);
  }

#if VERBOSE > 2
  printf("%s: ...done\n", __FILE__);
  (void)fflush(stdout);
#endif

  return EXIT_SUCCESS;
}
