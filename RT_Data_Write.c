/* Write a RT_Data structure and associated memory to a file stream. The
 * refinements are not written.
 *
 * ARGUMENTS
 *  Data   Pointer to the RT_Data structure to write
 *  fid    Pointer to the file stream to which to write.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE if unable to write any component.
 *
 * SEE ALSO
 *  RT_Data_Allocate, RT_Data_Assign, RT_Data_Read, RT_SaveData
 *
 * AUTHOR: Eric Tittley
 */

#include <stdio.h>
#include <stdlib.h>

#include "RT_Data.h"

int RT_Data_Write(RT_Data const* const Data, FILE* const fid) {
  size_t Nwrote;

  /* The number of cells in this LOS. */
  Nwrote = fwrite(&(Data->NumCells), sizeof(size_t), 1, fid);
  if (Nwrote != 1) {
    printf("ERROR: %s: %i: Unable to write RT_Data.NumCells.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  /* The cell # for which this LOS is a refinement. */
  Nwrote = fwrite(&(Data->Cell), sizeof(size_t), 1, fid);
  if (Nwrote != 1) {
    printf(
        "ERROR: %s: %i: Unable to write RT_Data.Cell.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  /* The amount of memory in the data space. */
  Nwrote = fwrite(&(Data->NBytes), sizeof(size_t), 1, fid);
  if (Nwrote != 1) {
    printf(
        "ERROR: %s: %i: Unable to write RT_Data.NBytes.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  /* write in the data space */
  Nwrote = fwrite(Data->MemSpace, Data->NBytes, 1, fid);
  if (Nwrote != 1) {
    printf("ERROR: %s: %i: Unable to write RT_Data.MemSpace.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
