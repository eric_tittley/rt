#ifndef _RT_H_
#define _RT_H_

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdio.h>

#include "Logic.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_FileHeaderT.h"
#include "RT_IntegralInvolatilesT.h"
#include "RT_Precision.h"
#include "RT_RatesT.h"
#include "RT_SourceT.h"
#include "RT_TimeScale.h"
#include "RecombApprox.h"

typedef struct {
  double current_time;
  double t_end;
  double dt_RT;
  double t_nextIonizationRate;
  bool updateIonizationTimescale;
  double nextOutputTime;
  int iterSinceIonizationRateUpdate;
  bool LastIterationFlag;
  bool OutputFlag;
  int OutputTimeIndex;
  bool reportStatistics;
} rt_time_t;

/* Time from expansion factor */
rt_float cosmological_time(rt_float const a, RT_ConstantsT const Constants);
rt_float expansion_factor_from_time(rt_float const t,
                                    RT_ConstantsT const Constants);

/* Perform the radiative transfer */
#ifdef RT_MPI
int RT_DistributeLOSs(int const me,
                      int const Master,
                      size_t const Nlos,
                      size_t const nproc,
                      rt_float const t,
                      rt_float const dt,
                      rt_float const ExpansionFactor,
                      RT_SourceT const Source,
                      RT_ConstantsT const Constants,
                      /*@null@*/ RT_Data **const Data);

/* int CheckEntropy_RTDL(RT_Data *Data, int me, int Line); */
#else
int RT_DistributeLOSs(size_t const Nlos,
                      rt_float const t,
                      rt_float const dt,
                      rt_float const ExpansionFactor,
                      RT_SourceT const Source,
                      RT_ConstantsT const Constants,
                      /*@null@*/ RT_Data **const Data);
#endif

int RT_ProcessLOS(rt_float const t,
                  rt_float const dt,
                  rt_float const ExpansionFactor,
                  RT_SourceT const Source,
                  RT_ConstantsT const Constants,
                  RT_Data *const Data_los);

int RT_ProcessUnrefinedCells(size_t const *const CellsNotRefined,
                             size_t const NCellsNotRefined,
                             unsigned char const LastIterationFlag,
                             rt_float const ExpansionFactor,
                             rt_float const tCurrent,
                             rt_float const dt,
                             rt_float const dt_RT,
                             RT_SourceT const Source,
                             RT_ConstantsT const Constants,
                             RT_Data *const Data_los,
                             RT_TimeScale *const dt_RT_sugg);

/* Read in the file that holds the physical and cosmological constants */
int RT_ReadConstantsFile(char const *const ConstantsFile,
                         RT_ConstantsT *const Constants);

/* Dump all the data to file */
int RT_SaveData(char const *const filename,
                RT_Data **const Data,
                size_t const Nlos,
                RT_FileHeaderT const FileHeader);

/* Read the data from file */
size_t RT_LoadData(char const *const filename,
                   /*@null@*/ RT_Data **const Data,
                   RT_FileHeaderT *const FileHeader);

/* Split a cell up for a refinement */
/*@null@*/ RT_Data *RT_SplitCell(RT_Data const *const Data_los,
                                 size_t const cell,
                                 rt_float const tau_H1_cell,
                                 RT_ConstantsT const Constants);

/* Merge a cell */
void RT_MergeCell(RT_Data const *const Data_cell,
                  size_t const cell,
                  RT_Data *const Data_los);

/* Set the density in the refinement Data_los. */
int RT_SetRefinementDensity(RT_Data *const Data_cell,
                            RT_Data const *const Data_los,
                            size_t const cell);

/* Initialise the source spectrum */
int RT_InitialiseSpectrum(int const me,
                          RT_ConstantsT const Constants,
                          RT_SourceT *const Source);

/* Initialize (preferably only once) the absorption cross sections array for
 * HI, HeI, and HeII at the integration frequencies. */
int RT_InitializeCrossSectionArray(RT_ConstantsT *Constants);

/* Load the source luminosity function table from file. */
int RT_LoadSourceTable(int const me,
                       char const *const filename,
                       RT_SourceT *const Source);

/* Set the pointer to the Luminosity function, given the SourceType */
void RT_SetSourceFunction(RT_SourceT *const Source);

/* Interpolate input spectrum from a table (general linear interpolation) */
rt_float RT_InterpolateFromTable(rt_float const xp,
                                 size_t const N,
                                 rt_float const *const x,
                                 rt_float const *const y);

/* Interpolate densities (or any scalar) from a grid */
rt_float RT_InterpolateFromGrid(rt_float const r,
                                float const *const density_los,
                                size_t const N,
                                rt_float const Ro,
                                rt_float const width);

/* The position of the light front */
rt_float RT_LightFrontPosition(rt_float const t,
                               rt_float const t_on,
                               rt_float const DistanceTolerance,
                               RT_ConstantsT const Constants);
rt_float RT_LFP_dadt(rt_float const t, RT_ConstantsT const Constants);
rt_float RT_LFP_integral(rt_float const t_o,
                         rt_float const t,
                         rt_float dt,
                         RT_ConstantsT const Constants);
rt_float RT_LFP_LightFrontPosition(rt_float const t_o,
                                   rt_float const t,
                                   rt_float const delta_r,
                                   RT_ConstantsT const Constants);

void RT_RecombinationRatesInclCooling(size_t const cellindex,
                                      rt_float const ExpansionFactor,
                                      RT_ConstantsT const Constants,
                                      RT_Data const Data,
                                      /*@out@*/ RT_RatesBlockT const Rates);

/* The Recombination Coefficients */
int RT_RecombinationCoefficients(rt_float const tau_H1_cell,
                                 rt_float const T,
                                 /*@out@*/ rt_float *const alpha_H1,
                                 /*@out@*/ rt_float *const alpha_He1,
                                 /*@out@*/ rt_float *const alpha_He2,
                                 /*@out@*/ rt_float *const beta_H1,
                                 /*@out@*/ rt_float *const beta_He1,
                                 /*@out@*/ rt_float *const beta_He2);

/* Calculate Adaptive Timescale */
int RT_AdaptiveTimescale(rt_float const dt,
                         RT_ConstantsT const Constants,
                         RT_RatesT const *const Rates,
                         RT_Cell const *const Data_cell,
                         RT_TimeScale *const dt_RT_sugg);

/* Update the Ionisation states and the Entropy */
int RT_UpdateCell(rt_float const dt_RT,
                  rt_float const ExpansionFactor,
                  RT_ConstantsT const Constants,
                  RT_RatesT const *const Rates,
                  RT_Cell *const Data_cell);

void RT_UpdateCell_RateBlock(rt_float const dt_RT,
                             rt_float const ExpansionFactor,
                             size_t const cellindex,
                             RT_ConstantsT const Constants,
                             RT_RatesBlockT const Rates,
                             /* updated */ RT_Data Data);

void RT_UpdateParticles(rt_float const dt_RT,
                        rt_float const Redshift,
                        RT_ConstantsT const Constants,
                        RT_RatesBlockT const Rates,
                        /* Updated */ RT_Data Data);

/* Update Data_los for the last RT iteration */
int RT_LastIterationUpdate(RT_ConstantsT const Constants,
                           RT_RatesT const *const Rates,
                           RT_Cell *const Data_cell);

/* Determine next timestep, dt_RT */
int RT_NextTimeStep(rt_float const dt,
                    rt_float const t_step,
                    RT_ConstantsT const Constants,
                    RT_TimeScale const *const dt_RT_sugg,
                    rt_float *const dt_RT,
                    unsigned char *const LastIterationFlag);

/* Refine a cell */
int RT_RefineCell(size_t const iz,
                  rt_float const dt,
                  rt_float const dt_RT,
                  rt_float const CurrentTime,
                  rt_float const ExpansionFactor,
                  RT_SourceT const Source,
                  RT_ConstantsT const Constants,
                  RT_Data *const Data_los);

/* Process a single cell */
int RT_ProcessCell(unsigned char const LastIterationFlag,
                   rt_float const ExpansionFactor,
                   rt_float const tCurrent,
                   rt_float const dt,
                   rt_float const dt_RT,
                   RT_SourceT const Source,
                   RT_ConstantsT const Constants,
                   RT_Cell *const Data_cell,
                   RT_TimeScale *const dt_RT_sugg);

/* Factors which determine rates in a cell */
int RT_Rates(rt_float const ExpansionFactor,
             rt_float const tCurrent,
             RT_SourceT const Source,
             RT_ConstantsT const Constants,
             RT_Cell const *const Data_cell,
             /*@out@*/
             RT_RatesT *const Rates);

/* Add Runge-Kutta rates */
int RT_Rates_RK(RT_RatesT const *const Rates0,
                RT_RatesT const *const Rates1,
                RT_RatesT const *const Rates2,
                RT_RatesT const *const Rates3,
                /*@out@*/ RT_RatesT *const Rates);
rt_float RT_Rates_RK_Add(rt_float const R1,
                         rt_float const R2,
                         rt_float const R3,
                         rt_float const R4);

/***** PHOTOIONIZATION CROSS SECTIONS *****/
rt_float H1_H2(rt_float const nu, RT_ConstantsT const Constants);
rt_float He1_He2(rt_float const nu, RT_ConstantsT const Constants);
rt_float He2_He3(rt_float const nu, RT_ConstantsT const Constants);

/***** RECOMBINATION COEFFICIENTS *****/
rt_float recomb_H1(rt_float const T, int const recomb_approx);
rt_float recomb_He1(rt_float const T, int const recomb_approx);
rt_float recomb_He2(rt_float const T, int const recomb_approx);

/***** RECOMBINATION COOLING COEFFICIENTS *****/
rt_float recombcool_H1(rt_float const T, int const recomb_approx);
rt_float recombcool_He1(rt_float const T, int const recomb_approx);
rt_float recombcool_He2(rt_float const T, int const recomb_approx);

/***** Ionisation and Ionisation Heating rates *****/
int RT_PhotoionizationRates(rt_float const N_H1,
                            rt_float const N_He1,
                            rt_float const N_He2,
                            rt_float const N_H1_cum,
                            rt_float const N_He1_cum,
                            rt_float const N_He2_cum,
                            rt_float const z,
                            RT_SourceT const Source,
                            RT_ConstantsT const Constants,
                            RT_IntegralInvolatilesT Involatiles,
                            rt_float const DistanceFromSourceMpc,
                            rt_float const ParticleRadiusMpc,
                            rt_float const DistanceThroughElementMpc,
                            rt_float *const I_H1,
                            rt_float *const I_He1,
                            rt_float *const I_He2,
                            rt_float *const G_H1,
                            rt_float *const G_He1,
                            rt_float *const G_He2,
                            rt_float *const Gamma_HI);

/***** Functions to be integrated *****/
int RT_PhotoionizationRatesFunctions(rt_float const nu,
                                     rt_float const Luminosity,
                                     rt_float const N_H1,
                                     rt_float const N_He1,
                                     rt_float const N_He2,
                                     rt_float const N_H1_cum,
                                     rt_float const N_He1_cum,
                                     rt_float const N_He2_cum,
                                     RT_ConstantsT const *const Constants,
                                     rt_float const *const Alpha,
                                     /*@out@*/ rt_float *const f);

/***** Integrate.  Calls one or more of the methods below. *****/
int RT_PhotoionizationRatesIntegrate(rt_float const N_H1,
                                     rt_float const N_He1,
                                     rt_float const N_He2,
                                     rt_float const N_H1_cum,
                                     rt_float const N_He1_cum,
                                     rt_float const N_He2_cum,
                                     rt_float const z,
                                     RT_SourceT const Source,
                                     RT_ConstantsT Constants,
                                     /*@out@*/ rt_float *const f);

int RT_PhotoionizationRatesIntegrate_I(rt_float const N_H1,
                                       rt_float const N_He1,
                                       rt_float const N_He2,
                                       rt_float const N_H1_cum,
                                       rt_float const N_He1_cum,
                                       rt_float const N_He2_cum,
                                       RT_ConstantsT Constants,
                                       RT_IntegralInvolatilesT Involatiles,
                                       /*@out@*/ rt_float *const f);

int RT_PhotoionizationRatesMonochromatic(rt_float const N_H1,
                                         rt_float const N_He1,
                                         rt_float const N_He2,
                                         rt_float const N_H1_cum,
                                         rt_float const N_He1_cum,
                                         rt_float const N_He2_cum,
                                         rt_float const Redshift,
                                         RT_SourceT Source,
                                         RT_ConstantsT const Constants,
                                         /*@out@*/ rt_float *const f);

/***** Update the columnd densities if processing a Line of Sight *****/
void RT_UpdateColumnDensitiesAlongLOS(rt_float const t_step,
                                      rt_float const dt,
                                      RT_ConstantsT const Constants,
                                      RT_Data *const Data_los);

/***** Integration Methods *****/
int Gauss_Legendre(rt_float const nu_start,
                   rt_float const nu_finish,
                   rt_float const N_H1,
                   rt_float const N_He1,
                   rt_float const N_He2,
                   rt_float const N_H1_cum,
                   rt_float const N_He1_cum,
                   rt_float const N_He2_cum,
                   rt_float const z,
                   RT_SourceT const Source,
                   RT_ConstantsT *Constants,
                   size_t const IntervalIndex,
                   /*@out@*/ rt_float *const f);

int Gauss_Laguerre(rt_float const nu_start,
                   /*@unused@*/ rt_float const nu_finish,
                   rt_float const N_H1,
                   rt_float const N_He1,
                   rt_float const N_He2,
                   rt_float const N_H1_cum,
                   rt_float const N_He1_cum,
                   rt_float const N_He2_cum,
                   rt_float const z,
                   RT_SourceT const Source,
                   RT_ConstantsT *Constants,
                   size_t const IntervalIndex,
                   /*@out@*/ rt_float *const f);

/***** Total photon flux *****/
rt_float PhotonFlux(rt_float const nu_start,
                    rt_float const nu_finish,
                    rt_float const z,
                    size_t const N_Slices,
                    RT_SourceT const Source,
                    RT_ConstantsT const Constants);

int RT_AdaptiveTimescaleLOSCell(rt_float const dt,
                                RT_ConstantsT const Constants,
                                RT_RatesT const *const Rates,
                                RT_Data const Data,
                                size_t const iCell,
                                RT_TimeScale *const dt_RT_sugg);

int RT_UpdateLOS(rt_float const dt_RT,
                 rt_float const ExpansionFactor,
                 RT_ConstantsT const Constants,
                 RT_RatesT const *const Rates,
                 RT_Data const Data);

int RT_UpdateLOSElement(size_t const iCell,
                        rt_float const dt_RT,
                        rt_float const ExpansionFactor,
                        RT_ConstantsT const Constants,
                        RT_RatesT const *const Rates,
                        RT_Data const Data);
void RT_ConstantsPrint(RT_ConstantsT const Constants);

rt_float RT_FindMinimumTimescale(RT_ConstantsT const Constants,
                                 RT_RatesBlockT const Rates,
                                 RT_Data const Data);

void RT_FindNextTimestep(size_t const nParticles,
                         rt_float const t_stop,
                         rt_float const t,
                         RT_Data const Data_dev,
                         RT_RatesBlockT const Rates_dev,
                         RT_ConstantsT const Constants,
                         /* Output */
                         rt_float *dt_RT,
                         unsigned char *LastIterationFlag);

void RT_UpdateCumulativeHeatingCoolingRate(RT_RatesBlockT Rates);

void RT_CalculateRecombinationRatesInclCooling(
    rt_float const ExpansionFactor,
    RT_ConstantsT const Constants,
    RT_Data const Data,
    /* updated */ RT_RatesBlockT Rates);

void RT_CoolingRatesExtra(rt_float const ExpansionFactor,
                          RT_ConstantsT const Constants,
                          RT_Data const Data,
                          RT_RatesBlockT const Rates);

void RT_CoolingRatesExtra_Cell(rt_float const ExpansionFactor,
                               RT_ConstantsT const Constants,
                               RT_Cell const *const Data,
                               RT_RatesT *const Rates);

rt_float RT_EntropyFromTemperature(RT_ConstantsT const Constants,
                                   rt_float const Temperature,
                                   size_t const iCell,
                                   RT_Data const Data);

rt_float RT_TemperatureFromEntropy(RT_ConstantsT const Constants,
                                   rt_float const Entropy,
                                   size_t const iCell,
                                   RT_Data const Data);

int RT_SetHydrogenFractionEquilibriumValues(RT_RatesBlockT const Rates,
                                            RT_Data const Data);

int RT_useEquilibriumValues(size_t const cellindex,
                            RT_Data const *const Data,
                            RT_RatesBlockT const *const Rates,
                            RT_ConstantsT const *const Constants);

#endif /* _RT_H_ */
