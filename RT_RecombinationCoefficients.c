/* RT_RecombinationCoefficients: Recombination coefficients alpha and beta
 *                               for H and He.
 *
 * ierr = RT_RecombinationCoefficients(rt_float const tau_H1_cell,
 *                                     rt_float const T,
 *                                     rt_float *alpha_H1,
 *                                     rt_float *alpha_He1,
 *                                     rt_float *alpha_He2,
 *                                     rt_float *beta_H1,
 *                                     rt_float *beta_He1,
 *                                     rt_float *beta_He2 );
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  tau_H1_cell    The optical depth in the cell. Only used if
 *                 USE_B_RECOMBINATION_CASE set.
 *  T              The temperature [K].
 *
 * ARGUMENTS MODIFIED
 *  alpha_*        Recombination coefficients. [m^3 s^-1]
 *  beta_*         Recombination cooling coefficients. [J m^3 s^-1]
 *
 * RETURNS
 *  EXIT SUCCESS
 *  EXIT FAILURE
 *
 * PREPROCESSOR MACROS
 *  DEBUG
 *  USE_B_RECOMBINATION_CASE
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <math.h>
#  include <stdio.h>
#endif

#include "Logic.h"
#include "RT.h"

#define SET_RETURNS  \
  (*alpha_H1) = 0.;  \
  (*alpha_He1) = 0.; \
  (*alpha_He2) = 0.; \
  (*beta_H1) = 0.;   \
  (*beta_He1) = 0.;  \
  (*beta_He2) = 0.;

int RT_RecombinationCoefficients(rt_float const tau_H1_cell,
                                 rt_float const T,
                                 /*@out@*/ rt_float* const alpha_H1,
                                 /*@out@*/ rt_float* const alpha_He1,
                                 /*@out@*/ rt_float* const alpha_He2,
                                 /*@out@*/ rt_float* const beta_H1,
                                 /*@out@*/ rt_float* const beta_He1,
                                 /*@out@*/ rt_float* const beta_He2) {
  enum RecombApprox recomb_approx;

  /*Calculates recombination coefficients m^3 s^-1 */
#ifdef USE_B_RECOMBINATION_CASE_ONLY
  recomb_approx = Approx_B;
#else
#  ifdef USE_B_RECOMBINATION_CASE
  if (tau_H1_cell < 1.0) {
    recomb_approx = Approx_A;
  } else {
    recomb_approx = Approx_B;
  }
#  else
  /* Default if no MACROS defined */
  recomb_approx = Approx_A;
#  endif
#endif

  (*alpha_H1) = recomb_H1(T, recomb_approx);
#ifdef DEBUG
  if (!(bool)isfinite((*alpha_H1))) {
    printf("ERROR: %s: %i: alpha_H1=%5.3e; T=%5.3e\n",
           __FILE__,
           __LINE__,
           *alpha_H1,
           T);
    (void)fflush(stdout);
    SET_RETURNS;
    return EXIT_FAILURE;
  }
#endif
  (*alpha_He1) = recomb_He1(T, recomb_approx);
#ifdef DEBUG
  if (!(bool)isfinite((*alpha_He1))) {
    printf("ERROR: RT_RecombinationCoefficients: %i: alpha_He1=nan; T=%5.3e\n",
           __LINE__ - 4,
           T);
    (void)fflush(stdout);
    SET_RETURNS;
    return EXIT_FAILURE;
  }
#endif
  (*alpha_He2) = recomb_He2(T, recomb_approx);
#ifdef DEBUG
  if (!(bool)isfinite((*alpha_He2))) {
    printf(
        "ERROR: RT_RecombinationCoefficients: %i (*alpha_He2)=nan; T=%5.3e\n",
        __LINE__ - 4,
        T);
    (void)fflush(stdout);
    SET_RETURNS;
    return EXIT_FAILURE;
  }
#endif

  /* Calculates cooling recombination coefficient m^3 s^-1 */
  (*beta_H1) = recombcool_H1(T, recomb_approx);
#ifdef DEBUG
  if (!(bool)isfinite((*beta_H1))) {
    printf("ERROR: RT_RecombinationCoefficients: %i beta_H1=nan; T=%5.3e\n",
           __LINE__ - 4,
           T);
    (void)fflush(stdout);
    SET_RETURNS;
    return EXIT_FAILURE;
  }
#endif
  (*beta_He1) = recombcool_He1(T, recomb_approx);
#ifdef DEBUG
  if (!(bool)isfinite((*beta_He1))) {
    printf("ERROR: RT_RecombinationCoefficients: %i: beta_He1=nan: T=%5.3e\n",
           __LINE__ - 4,
           T);
    (void)fflush(stdout);
    SET_RETURNS;
    return EXIT_FAILURE;
  }
#endif
  (*beta_He2) = recombcool_He2(T, recomb_approx);
#ifdef DEBUG
  if (!(bool)isfinite((*beta_He2))) {
    printf("ERROR: RT_RecombinationCoefficients: %i: beta_He2=nan: T=%5.3e\n",
           __LINE__ - 4,
           T);
    (void)fflush(stdout);
    SET_RETURNS;
    return EXIT_FAILURE;
  }
#endif

  return EXIT_SUCCESS;
}
