/* Split a cell into a series of slices.
 *
 * ARGUMENTS
 *  Input, not modified
 *   Data_los   The RT_Data structure for the current line of sight.
 *   cell       The cell to split up.
 *   tau_H1_cell  The optical depth of the cell.
 * RETURNS
 *  Pointer to the new refinement.
 *  NULL if malloc failed.
 *
 * AUTHOR: Eric Tittley
 */
#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#ifdef REFINEMENTS

#  include <math.h>
#  include <stdio.h>
#  include <stdlib.h>

#  include "RT.h"
#  include "RT_ConstantsT.h"
#  include "RT_Data.h"

/*@null@*/ RT_Data *RT_SplitCell(RT_Data const *const Data_los,
                                 size_t const cell,
                                 rt_float const tau_H1_cell,
                                 RT_ConstantsT const Constants) {
  /* The maximum number of slices into which a cell can be split.
   * Increase if you need more. */

  size_t const MAX_NSLICES = 4096;

  /* Number of slices in which to divide the cell */
  size_t NumSlices =
      (size_t)CEIL(2. * tau_H1_cell / (Constants.Max_tau_in_cell));
  if (NumSlices > MAX_NSLICES) {
    printf("WARNING: %s: NumSlices=%lu > MAX_NSLICES.\n", __FILE__, NumSlices);
    printf("         Increase MAX_NSLICES\n");
    NumSlices = MAX_NSLICES;
  }
#  if VERBOSE >= 1
  printf("%s: cell=%lu; tau_H1_cell=%6.3f; NumSlices=%lu\n",
         __FILE__,
         cell,
         tau_H1_cell,
         NumSlices);
  (void)fflush(stdout);
#  endif

  /* Allocate the memory for the refinement */
  RT_Data *Data_cell = RT_Data_Allocate(NumSlices);
#  ifdef DEBUG
  if (Data_cell == NULL) {
    printf("ERROR: %s: %i: Failed to create refinement\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return NULL;
  }
#  endif

  /* Find dR in the refinement slices */
  const rt_float dR = Data_los->dR[cell] / (rt_float)NumSlices;
#  ifdef DEBUG
  if (dR < 0) {
    printf("ERROR: %s: %i: dR=%e\n", __FILE__, __LINE__, dR);
    printf("       dR[%lu]=%e; NumSlices=%lu NumCells=%lu\n",
           cell,
           Data_los->dR[cell],
           NumSlices,
           Data_los->NumCells);
    (void)fflush(stdout);
    RT_Data_Free(Data_cell);
    free(Data_cell);
    Data_cell = NULL;
    return NULL;
  }
#  endif

  Data_cell->NumCells = NumSlices;
  Data_cell->Cell = cell;

  /* Fill in the data structure for this cell */
  size_t i;
  for (i = 0; i < NumSlices; i++) {
    Data_cell->R[i] = Data_los->R[cell] + (rt_float)i * dR;
    Data_cell->dR[i] = dR;
    Data_cell->Entropy[i] = Data_los->Entropy[cell];
    Data_cell->T[i] = Data_los->T[cell];
    Data_cell->n_H[i] = Data_los->n_H[cell];
    Data_cell->f_H1[i] = Data_los->f_H1[cell];
    Data_cell->f_H2[i] = Data_los->f_H2[cell];
    Data_cell->n_He[i] = Data_los->n_He[cell];
    Data_cell->f_He1[i] = Data_los->f_He1[cell];
    Data_cell->f_He2[i] = Data_los->f_He2[cell];
    Data_cell->f_He3[i] = Data_los->f_He3[cell];
    Data_cell->Ref_pointer[i] = NULL;
  }
#  ifdef DEBUG
  if (Data_cell->R[0] <= 0.) {
    printf("ERROR: %s: %i: Data_cell->R[0]=%e\n",
           __FILE__,
           __LINE__,
           Data_cell->R[0]);
    printf("       Data_los->R[%lu]=%e; Data_los->R[%lu]=%e\n",
           cell,
           Data_los->R[cell],
           cell + 1,
           Data_los->R[cell + 1]);
    (void)fflush(stdout);
    RT_Data_Free(Data_cell);
    free(Data_cell);
    Data_cell = NULL;
    return NULL;
  }
#  endif

  /* Set Data_cell->Density */
  int ierr = RT_SetRefinementDensity(Data_cell, Data_los, cell);
#  ifdef DEBUG
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: RT_SetRefinementDensity failed for cell %lu",
           __FILE__,
           __LINE__,
           cell);
    (void)fflush(stdout);
    RT_Data_Free(Data_cell);
    free(Data_cell);
    Data_cell = NULL;
    return NULL;
  }
#  endif

  /* NOTE: The only use of RT_SplitCell is in a refinement, but it won't
   * compile if this is not blocked out, even if not used. */
  /* Only the first element of each of these needs to be defined. */
  /*
   * Data_cell->column_H1[0]    = Data_los->column_H1[cell];
   * Data_cell->column_He1[0]   = Data_los->column_He1[cell];
   * Data_cell->column_He2[0]   = Data_los->column_He2[cell];
   */

  /* Set the cumulative column densities up to the cell. They are
   * constant during the interval dt_RT */
  Data_cell->NCol[0] = Data_los->column_H1[cell];
  Data_cell->NCol[1] = Data_los->column_He1[cell];
  Data_cell->NCol[2] = Data_los->column_He2[cell];
  Data_cell->NCol[3] = Data_cell->NCol[0];
  Data_cell->NCol[4] = Data_cell->NCol[1];
  Data_cell->NCol[5] = Data_cell->NCol[2];

  return Data_cell;
}
#endif /* REFINEMENTS */
