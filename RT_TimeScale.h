#ifndef _RT_TIMESCALE_H_
#define _RT_TIMESCALE_H_

#include "RT_Precision.h"

struct RT_TimeScale_struct {
  rt_float Entropy; /* Temperature */
  rt_float I_H1;    /* HI ionization */
  rt_float I_He1;   /* HeI ionistation */
  rt_float I_He2;   /* HeII ionistation */
  rt_float R_H1;    /* HI recombination */
  rt_float R_He1;   /* HeI recombination */
  rt_float R_He2;   /* HeII recombination */
};

typedef struct RT_TimeScale_struct RT_TimeScale;

void RT_TimeScale_MergeTimescales(RT_TimeScale* const TS_base,
                                  RT_TimeScale const* const TS_to_merge);

void RT_TimeScale_SetInfiniteTime(RT_TimeScale* dt_RT_sugg);

#endif /* _RT_TIMESCALE_H_ */
