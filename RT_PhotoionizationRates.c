/* RT_PhotoionizationRates
 *  Evaluate the photoionization and photoionization heating rates.
 *
 * int RT_PhotoionizationRates(rt_float const N_H1,
 *                             rt_float const N_He1,
 *                             rt_float const N_He2,
 *                             rt_float const N_H1_cum,
 *                             rt_float const N_He1_cum,
 *                             rt_float const N_He2_cum,
 *                             rt_float const RedshiftOfSource,
 *                             RT_SourceT const Source,
 *                             RT_ConstantsT const Constants,
 *                             rt_float const R,
 *                             rt_float const dR,
 *                             rt_float *I_H1,
 *                             rt_float *I_He1,
 *                             rt_float *I_He2,
 *                             rt_float *G_H1,
 *                             rt_float *G_He1,
 *                             rt_float *G_He2,
 *                             rt_float *Gamma_HI)
 *
 * ARGUMENTS
 *  Input, not modified
 *   N_H1, N_He1, N_He2         Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum     Cumulative column densities between
 *   RedshiftOfSource           Redshift of the source (used if the source is a
 * function of time). Source     The Source Constants  Physical constants R The
 * distance to the source [m] dR         The depth through the cell or particle
 * [m]
 *
 *  Output
 *   I_H1, I_He1, I_He2         photoionization rates [s^-1]
 *   G_H1, G_He1, G_He2         photoionization heating rates [W]
 *   Gamma_HI                   photoionization rate per particle for HI
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * NOTES
 *
 * CALLS
 *  RT_PhotoionizationRatesIntegrate() which worries about the method used and
 *  splits the integral into three parts.
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif
/* Currently need "gcc -D_GNU_SOURCE --finite-math-only" to get exp10() */
#include <math.h>

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_SourceT.h"

int RT_PhotoionizationRates(rt_float const N_H1,
                            rt_float const N_He1,
                            rt_float const N_He2,
                            rt_float const N_H1_cum,
                            rt_float const N_He1_cum,
                            rt_float const N_He2_cum,
                            rt_float const RedshiftOfSource,
                            RT_SourceT const Source,
                            RT_ConstantsT const Constants,
                            RT_IntegralInvolatilesT Involatiles,
                            rt_float const DistanceFromSourceMpc,
                            rt_float const ParticleRadiusMpc,
                            rt_float const DistanceThroughElementMpc,
                            rt_float* const I_H1,
                            rt_float* const I_He1,
                            rt_float* const I_He2,
                            rt_float* const G_H1,
                            rt_float* const G_He1,
                            rt_float* const G_He2,
                            rt_float* const Gamma_HI) {
  rt_float f[7];
  int ierr;
  if (Source.SourceType != SourceMonochromatic) {
#if 0
  ierr = RT_PhotoionizationRatesIntegrate(N_H1,
                                          N_He1,
                                          N_He2,
                                          N_H1_cum,
                                          N_He1_cum,
                                          N_He2_cum,
                                          RedshiftOfSource,
                                          Source,
                                          Constants,
                                          f);
#else
    ierr = RT_PhotoionizationRatesIntegrate_I(N_H1,
                                              N_He1,
                                              N_He2,
                                              N_H1_cum,
                                              N_He1_cum,
                                              N_He2_cum,
                                              Constants,
                                              Involatiles,
                                              f);
#endif
  } else {
    ierr = RT_PhotoionizationRatesMonochromatic(N_H1,
                                                N_He1,
                                                N_He2,
                                                N_H1_cum,
                                                N_He1_cum,
                                                N_He2_cum,
                                                RedshiftOfSource,
                                                Source,
                                                Constants,
                                                f);
  }
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: RT_PhotoionizationRatesIntegrate failed.\n",
           __FILE__,
           __LINE__);
    return EXIT_FAILURE;
  }
#ifdef DEBUG
  size_t i;
  for (i = 0; i < 7; i++) {
    if ((!isfinite(f[i])) || (f[i] < 0.0)) {
      printf("%s: %i: ERROR: f[%lu]=%e\n", __FILE__, __LINE__, i, f[i]);
      return EXIT_FAILURE;
    }
  }
#endif

  /* AttenuationFactor is
   * (attenuation of source luminosity)^-1 * ( path length through cell)
   * = area of shell * depth
   * = volume of shell (only incidentally)
   * = (4 pi R^2) * dR
   * where R is the distance to the cell.
   * This assumes a non-plane-wave approximation.
   * For the plane-wave approximation, AttenuationFactor = ( 1/(4 pi R_0^2) ) *
   * dR
   */
  rt_float AttenuationFactor = -1.;
  /* The following should set the current expansion factor, not the expansion
   * factor of the source as seen from the current location. The difference is
   * small
   */
#ifndef CONSTANT_DENSITY
  const rt_float ExpansionFactor = 1. / (RedshiftOfSource + 1.);
#endif

  switch (Source.AttenuationGeometry) {
    case PlaneWave:
/* Assuming:
 *  plane-wave geometry
 *  Data[0]->R[0] = (R_0 * Mpc)  or  (R_0 * ExpansionFactor * Mpc / h)
 */
#if defined(CONSTANT_DENSITY) && !defined(CONSTANT_DENSITY_COMOVING)
      AttenuationFactor = 4. * M_PI * Source.R_0 * Source.R_0 *
                          DistanceThroughElementMpc; /* Mpc^-3 */
#else
      AttenuationFactor = 4. * M_PI *
                          POW(Source.R_0 * ExpansionFactor / Constants.h, 2.) *
                          DistanceThroughElementMpc; /* Mpc^-3 */
#endif
      break;
    case SphericalWave:
      /* Assuming spherical geometry */
      AttenuationFactor = 4. * M_PI * DistanceFromSourceMpc *
                          DistanceFromSourceMpc *
                          DistanceThroughElementMpc; /* Mpc^-3 */
      break;
    /* For Ray*, the source luminosity MUST ALREADY BE ATTENUATED by
     * 1/(number of rays) */
    case RayPlaneWave:
      AttenuationFactor = M_PI * ParticleRadiusMpc * ParticleRadiusMpc *
                          DistanceThroughElementMpc; /* Mpc^-3 */
      break;
    case RaySphericalWave:
      if (DistanceFromSourceMpc <= ParticleRadiusMpc) {
        AttenuationFactor = 4. * M_PI * DistanceFromSourceMpc *
                            DistanceFromSourceMpc *
                            DistanceThroughElementMpc; /* Mpc^-3 */
      } else {
        AttenuationFactor = M_PI * ParticleRadiusMpc * ParticleRadiusMpc *
                            DistanceThroughElementMpc; /* Mpc^-3 */
      }
      break;
    default:
      printf("ERROR: %s: Unknown Attenuation Geometry: %i\n",
             __FILE__,
             Source.AttenuationGeometry);
  }
#ifdef DEBUG
  if ((!(bool)isfinite(AttenuationFactor)) || (AttenuationFactor <= 0)) {
    printf(
        "ERROR: %s: %i: AttenuationFactor=%5.3e: DistanceFromSourceMpc=%5.3e; "
        "ParticleRadiusMpc=%5.3e; Constants.Mpc=%5.1e\n",
        __FILE__,
        __LINE__,
        AttenuationFactor,
        DistanceFromSourceMpc,
        ParticleRadiusMpc,
        Constants.Mpc);
    return EXIT_FAILURE;
  }
#endif

  /* MISSING the Constants.Mpc^-3 factor in the following.
   * This implies that f *must* be too large, too.
   * f is 10^14, h_p is 10-34, AttenuationFactor is 10^4, missing (10^22)^3
   * So we have 10^(14 + 34 - 4 - 66) = 10^(-22)
   * which is entirely doable in float so long as we spread out that 10^22
   * properly. Try: 10^( (14-22) + (34-2*22) -4 ) = 10^(-8 - 10 - 4)*/
  *I_H1 = (f[0] / Constants.Mpc) /
          ((Constants.h_p * Constants.Mpc) * Constants.Mpc) / AttenuationFactor;
  *I_He1 = (f[1] / Constants.Mpc) /
           ((Constants.h_p * Constants.Mpc) * Constants.Mpc) /
           AttenuationFactor;
  *I_He2 = (f[2] / Constants.Mpc) /
           ((Constants.h_p * Constants.Mpc) * Constants.Mpc) /
           AttenuationFactor;
  /* The following scales as 10^(29 -4 -3*22) = 10^-41 so won't fit in a float,
     so add a 10^13 (Ma) to factor out later. */
  *G_H1 = ((f[3] / Constants.Mpc) / Constants.Mpc) *
          (Constants.Ma / Constants.Mpc) / AttenuationFactor;
  *G_He1 = ((f[4] / Constants.Mpc) / Constants.Mpc) *
           (Constants.Ma / Constants.Mpc) / AttenuationFactor;
  *G_He2 = ((f[5] / Constants.Mpc) / Constants.Mpc) *
           (Constants.Ma / Constants.Mpc) / AttenuationFactor;
  *Gamma_HI = (f[6] / Constants.Mpc) / (Constants.h_p * Constants.Mpc) /
              AttenuationFactor * DistanceThroughElementMpc;

  /* THE HEATING RATES G_* are too large by Ma. SEE UpdateCell() */

#ifdef DEBUG
  /* For debugging */
  if ((*I_H1 < 0.) || (!isfinite(*I_H1))) {
    printf("%s: %i: ERROR: I_H1=%e\n", __FILE__, __LINE__, *I_H1);
    printf(" f[0]=%e; h_p=%e; AttenuationFactor=%e\n",
           f[0],
           Constants.h_p,
           AttenuationFactor);
    return EXIT_FAILURE;
  }
  if ((*I_He1 < 0.) || (!isfinite(*I_He1))) {
    printf("%s: %i: ERROR: I_He1=%e\n", __FILE__, __LINE__, *I_He1);
    return EXIT_FAILURE;
  }
  if ((*I_He2 < 0.) || (!isfinite(*I_He2))) {
    printf("%s: %i: ERROR: I_He2=%e\n", __FILE__, __LINE__, *I_He2);
    return EXIT_FAILURE;
  }
  if ((*G_H1 < 0.) || (!isfinite(*G_H1))) {
    printf("%s: %i: ERROR: G_H1=%e\n", __FILE__, __LINE__, *G_H1);
    return EXIT_FAILURE;
  }
  if ((*G_He1 < 0.) || (!isfinite(*G_He1))) {
    printf("%s: %i: ERROR: G_He1=%e\n", __FILE__, __LINE__, *G_He1);
    return EXIT_FAILURE;
  }
  if ((*G_He2 < 0.) || (!isfinite(*G_He2))) {
    printf("%s: %i: ERROR: G_He2=%e\n", __FILE__, __LINE__, *G_He2);
    return EXIT_FAILURE;
  }
  if ((*Gamma_HI < 0.) || (!isfinite(*Gamma_HI))) {
    printf("%s: %i: ERROR: Gamma_HI=%e\n", __FILE__, __LINE__, *Gamma_HI);
    return EXIT_FAILURE;
  }
#endif
  return EXIT_SUCCESS;
}
