#ifndef _RT_LUMINOSITY_TYPES_H_
#define _RT_LUMINOSITY_TYPES_H_

/* Because of circularity in RT_Luminosity.h requiring RT_SourceT defined
 * while RT_SourceT.h requires RT_Luminosity_Types defined,
 * separate out so RT_SourceT.h can get RT_Luminosity_Types
 */

enum RT_Luminosity_Types {
  SourceBlackBody,
  SourceHybrid,
  SourceMonochromatic,
  SourcePowerLaw,
  SourcePowerLawMinusOne,
  SourcePowerLawMinusOneHalf,
  SourcePowerLawMinusThreeHalves,
  SourcePowerLawMinusTwo,
  SourceMiniQuasar,
  SourceTabulated,
  SourceGizmo,
  SourceUser
};

#endif /* _RT_LUMINOSITY_TYPES_H_ */
