/* Gauss_Legendre: Evaluate the photoionization and photoionization heating
 *      integrals using Gauss-Legendre Quadrature.
 *
 * int Gauss_Legendre(rt_float const nu_start,
 *                    rt_float const nu_finish,
 *                    rt_float const N_H1,
 *                    rt_float const N_He1, rt_float N_He2,
 *                    rt_float const N_H1_cum,
 *                    rt_float const N_He1_cum, rt_float N_He2_cum,
 *                    rt_float const z,
 *                    size_t const N_Slices,
 *                    RT_SourceT const Source,
 *                    RT_ConstantsT const Constants,
 *                    size_t IntervalIndex,
 *                    rt_float f[7] )
 *
 * ARGUMENTS
 *  Input, not modified
 *   nu_start   The frequency at which to commence the integration. [Hz]
 *   nu_finish  Upper limit of the integration. [Hz]
 *   N_H1, N_He1, N_He2         Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum     Cumulative column densities between
 *                                      the source and the near edge of the
 *                                      cell.  [m^2]
 *   z          Redshift of source (used if the source is a function of time).
 *   Constants->NLevels[IntervalIndex]  The number of levels of integration .
 *   Source     The Source
 *   Constant   Constants
 *   Alpha      Pointer to Constants->NLevels[IntervalIndex] pointers to cross-sections,
 *              one for each of H1-H2, He1-He2, and He2-He3
 *              If Alpha==NULL, then memory is allocated and the table is
 *              filled in. Otherwise it is just used.
 *              Must be passed by reference, since the pointer itself is set
 *              in this routine if it is NULL.
 *
 *  Output
 *   f[7]       Rates at frequency, nu. [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2]
 *              I_* are the photoionization rates (* h_p). [s^-1]
 *              G_* are the heating rates. [J s^-1]
 *              Gamma_HI is the photoionization rate per particle [m^3 s^-1 Hz^-1]
 *
 *  RETURNS
 *   EXIT_SUCCESS
 *   EXIT_FAILURE
 *
 *  EXTERNAL MACROS AND VARIABLES
 *
 *  NOTES
 *   There is a factor of h_p^-1 missing in f[0:2] since it is
 *   more efficient to apply the factor after integrating.
 *   f[0:2]/h_p is what you really want.
 *
 *  CALLS
 *   RT_PhotoionizationRatesFunction()  The rates at a given frequency.
 *
 *  AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#if !(defined F_INTEGRAL_PROB_SHARE) & !(defined F_INTEGRAL_PROP_SHARE) & !(defined F_INTEGRAL_ERT)
# define F_INTEGRAL_PROB_SHARE
#endif

#include <stdlib.h>
#include <stdio.h>
#ifdef DEBUG
# include <math.h>
#endif

#include "RT.h"
#include "GL_weights.h"
#include "RT_ConstantsT.h"
#include "RT_SourceT.h"

/* Convert span [-1,1] to [a,b] */
static inline rt_float Gauss_Legendre_x_trans(rt_float const t, rt_float const a,
                                            rt_float const b) {
 return (a+b)/2. + t*(b-a)/2.;
 /* This should be replaced (after testing) with:
  * return a + (b-a)*(1+t)/2.;
  * which has  1 division,  1 multiplication, and 3 additions
  * instead of 2 divisions, 1 multiplication, and 5 additions
  */
}
 
int Gauss_Legendre(rt_float const nu_start,
                   rt_float const nu_finish,
                   rt_float const N_H1,
                   rt_float const N_He1,
                   rt_float const N_He2,
                   rt_float const N_H1_cum,
                   rt_float const N_He1_cum,
                   rt_float const N_He2_cum,
                   rt_float const z,
                   RT_SourceT const Source,
                   RT_ConstantsT * Constants,
                   size_t const IntervalIndex,
         /*@out@*/ rt_float * const f)
{
 size_t i, j;
 int ierr;
 rt_float const *Xs;
 rt_float const *W;
 rt_float F[7];
 rt_float nu;
 
 for(i=0;i<7;i++) {
  f[i]=0.;
 }
 
 /* Associate the sample positions and weights with tabulated values */
 switch(Constants->NLevels[IntervalIndex]) {
  case   2: Xs = Xs_02_Legendre; W = W_02_Legendre; break;
  case   4: Xs = Xs_04_Legendre; W = W_04_Legendre; break;
 #if 0
  case   8: Xs = Xs_08_Legendre; W = W_08_Legendre; break;
  case  16: Xs = Xs_16_Legendre; W = W_16_Legendre; break;
  case  32: Xs = Xs_32_Legendre; W = W_32_Legendre; break;
  case  64: Xs = Xs_64_Legendre; W = W_64_Legendre; break;
  case 128: Xs = Xs_128_Legendre; W = W_128_Legendre; break;
  case 256: Xs = Xs_256_Legendre; W = W_256_Legendre; break;
 #endif
  default:
   printf("ERROR: Gauss_Legendre: Unknown level Constants->NLevels[IntervalIndex]=%lu\n",Constants->NLevels[IntervalIndex]);
   return EXIT_FAILURE;
 }
 
 if(Constants->Alpha[IntervalIndex]==NULL) {
  printf("%s: %i: Initializing absorption cross sections for Interval %lu with %lu levels\n",
         __FILE__,__LINE__,IntervalIndex,Constants->NLevels[IntervalIndex]);
  /* Note, Alpha will never get free()'d */
  Constants->Alpha[IntervalIndex]=(rt_float **)malloc(Constants->NLevels[IntervalIndex]*sizeof(rt_float *));
  for(i=0;i<Constants->NLevels[IntervalIndex];i++) {
   Constants->Alpha[IntervalIndex][i]=(rt_float *)malloc(3*sizeof(rt_float));
   nu = Gauss_Legendre_x_trans(Xs[i],nu_start,nu_finish);
   Constants->Alpha[IntervalIndex][i][0] = H1_H2(nu, *Constants);
   if(nu > Constants->nu_1) {
    Constants->Alpha[IntervalIndex][i][1] = He1_He2(nu, *Constants);
    if(nu > Constants->nu_2) {
     Constants->Alpha[IntervalIndex][i][2] = He2_He3(nu, *Constants);
    } else {
     Constants->Alpha[IntervalIndex][i][2] = 0.;
    }
   } else {
    Constants->Alpha[IntervalIndex][i][1] = 0.;
    Constants->Alpha[IntervalIndex][i][2] = 0.;
   }
  }
 }

 /* Sum the function evaluations at the sample positions times the weights */
 for(i=0;i<Constants->NLevels[IntervalIndex];i++) {
  nu = Gauss_Legendre_x_trans(Xs[i],nu_start,nu_finish);
  ierr = RT_PhotoionizationRatesFunctions(nu,
                                          N_H1,
                                          N_He1,
                                          N_He2,
                                          N_H1_cum,
                                          N_He1_cum,
                                          N_He2_cum,
                                          z,
                                         &Source,
                                          Constants,
                                          Constants->Alpha[IntervalIndex][i],
                                          F);
/* The following is for tests that need to be done. Look in F_Integral for
   these functions, and note that F_Integral_ERT.c has been renamed as
   RT_PhotoionizationRatesFunctions.c as used above.
 */
#if 0
 #ifdef F_INTEGRAL_PROB_SHARE
  ierr = F_integral(nu, N_H1, N_He1, N_He2, N_H1_cum, N_He1_cum, N_He2_cum, z,
                    Constants->Alpha[IntervalIndex][i], F);
 #endif
 #ifdef F_INTEGRAL_PROP_SHARE
  ierr = F_integral_PropShare(nu, N_H1, N_He1, N_He2, N_H1_cum, N_He1_cum, N_He2_cum, z,
                    Constants->Alpha[IntervalIndex][i], F);
 #endif
 #ifdef F_INTEGRAL_ERT
  ierr = F_integral_ERT(nu, N_H1, N_He1, N_He2, N_H1_cum, N_He1_cum, N_He2_cum, z,
                    Constants->Alpha[IntervalIndex][i], F);
 #endif
#endif 
 #ifdef DEBUG
  if(ierr==EXIT_FAILURE) {
   printf("ERROR: Gauss_Legendre: %i: F_integral failed.\n",__LINE__);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
  for(j=0;j<7;j++) {
   if( !(bool)isfinite(F[j]) ) {
    printf("ERROR: %s: %i: F[%lu]=%f.\n",__FILE__,__LINE__,j,F[j]);
    (void)fflush(stdout);
    return EXIT_FAILURE;
   }
  }
 #endif

  for(j=0;j<7;j++) {
   f[j] += W[i]*F[j];
  }
 }
 for(i=0;i<7;i++) {
  f[i] *= (nu_finish-nu_start)/2.;
 }
 return EXIT_SUCCESS;
}
