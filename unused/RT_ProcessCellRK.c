/* PREPROCESSOR MACROS
 *  VERBOSE (default 0)
 *  DEBUG
 *  PLANE_WAVE
 *  ADAPTIVE_TIMESCALE (default on)
 */

#ifdef HAVE_CONFIG
#include "config.h"
#else
#define VERBOSE 0
#define ADAPTIVE_TIMESCALE
#endif

#include <stdlib.h>
#if (defined DEBUG) || (VERBOSE > 0)
#include <stdio.h>
#endif
#include <math.h>

#include "RT.h"
#include "RT_Data.h"
#include "RT_RatesT.h"
#include "RT_ConstantsT.h"
#include "RT_SourceT.h"
#include "RT_TimeScale.h"
#include "Logic.h"

int RT_ProcessCellRK(bool const LastIterationFlag,
                     rt_float const ExpansionFactor,
                     rt_float const tCurrent,
                     rt_float const dt,
                     rt_float const dt_RT,
                     RT_SourceT const Source,
                     RT_ConstantsT const Constants,
                     RT_Cell * const Data_cell,
                     RT_TimeScale * const dt_RT_sugg ) {

 int ierr;
 
 RT_RatesT Rates, Rates0, Rates1, Rates2, Rates3;
 RT_Cell Cell;

 /* *** Get the intermediate RK rates *** */

 /* Rates0 */
 ierr = RT_Rates(ExpansionFactor, tCurrent, Source, Constants,
                 Data_cell, &Rates0);
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_Rates failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* Cell1 */
 RT_Cell_Copy(Data_cell, &Cell);
 ierr = RT_UpdateCell(dt_RT/2., ExpansionFactor, Constants, &Rates0, &Cell );
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_UpdateCell failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* Rates1 */
 ierr = RT_Rates(ExpansionFactor, tCurrent+dt_RT/2.0, Source, Constants, &Cell, &Rates1);
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_Rates failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* Cell2 */
 RT_Cell_Copy(Data_cell, &Cell);
 ierr = RT_UpdateCell(dt_RT/2., ExpansionFactor, Constants, &Rates0, &Cell );
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_UpdateCell failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* Rates2 */
 ierr = RT_Rates(ExpansionFactor, tCurrent+dt_RT/2.0, Source, Constants, &Cell, &Rates2);
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_Rates failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* Cell3 */
 RT_Cell_Copy(Data_cell, &Cell);
 ierr = RT_UpdateCell(dt_RT, ExpansionFactor, Constants, &Rates2, &Cell );
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_UpdateCell failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* Rates3 */
 ierr = RT_Rates(ExpansionFactor, tCurrent+dt_RT, Source, Constants, &Cell, &Rates3);
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_Rates failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* *** Finished getting the intermediate RK rates *** */

 /* Add the rates together, in the Runge-Kutta form */
 ierr = RT_Rates_RK(&Rates0,&Rates1,&Rates2,&Rates3,&Rates);
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_Rates_RK failed.",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

#ifdef ADAPTIVE_TIMESCALE
 /* Find the minimum timescales. Need to use Rates0, Rates1, or Rates2. */
 ierr = RT_AdaptiveTimescale(dt,
                             Constants,
                             &Rates0,
                             Data_cell,
                             dt_RT_sugg);
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_AdaptiveTimescale failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif
#endif /* ADAPTIVE_TIMESCALE */

 /* Update Temperature & Ionisation states for the cell for this timestep. */
 ierr = RT_UpdateCell(dt_RT, ExpansionFactor, Constants, &Rates, Data_cell );
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_UpdateCell failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 if(LastIterationFlag) { /* Only during last sub-timestep */
  ierr = RT_LastIterationUpdate(Constants, &Rates, Data_cell );
 #ifdef DEBUG
  if(ierr == EXIT_FAILURE) {
   printf("ERROR: %s: %i: RT_LastIterationUpdate failed.\n",__FILE__,__LINE__);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 #endif
 } /* endif LastIterationFlag */

 return EXIT_SUCCESS;
}
