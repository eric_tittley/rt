/* Gauss_Laguerre: Evaluate the photoionization and photoionization heating
 *      integrals using Gauss-Laguerre Quadrature.
 *
 * int Gauss_Laguerre(rt_float const nu_start,
 *                    rt_float const nu_finish,
 *                    rt_float const N_H1,
 *                    rt_float const N_He1,
 *                    rt_float const N_He2,
 *                    rt_float const N_H1_cum,
 *                    rt_float const N_He1_cum,
 *                    rt_float const N_He2_cum,
 *                    rt_float const z,
 *                    RT_SourceT const Source,
 *                    RT_ConstantsT const Constants,
 *                    size_t IntervalIndex,
 *                    rt_float f[7] )
 *
 * ARGUMENTS
 *  Input, not modified
 *   nu_start   The frequency at which to commence the integration. [Hz]
 *   nu_finish  (Ignored. G-Laguerre integration goes from nu_start to infinity.)
 *   N_H1, N_He1, N_He2         Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum     Cumulative column densities between
 *                                      the source and the near edge of the
 *                                      cell.  [m^2]
 *   z          Redshift of source (used if the source is a function of time).
 *   Constants->NLevels[IntervalIndex]  The number of levels of integration.
 *   Source     The Source
 *   Constants  Constant values
 *   Alpha      Pointer to Constants->NLevels[IntervalIndex] pointers to cross-sections,
 *              one for each of H1-H2, He1-He2, and He2-He3
 *              If Alpha==NULL, then memory is allocated and the table is
 *              filled in. Otherwise it is just used.
 *              Must be passed by reference, since the pointer itself is set
 *              in this routine if it is NULL.
 *
 *  Output
 *   There is a factor of h_p^-1 missing in f[0:2] since it is
 *   more efficient to apply the factor after integrating.
 *   f[0:2]/h_p is what you really want.
 *
 *  RETURNS
 *   EXIT_SUCCESS
 *   EXIT_FAILURE
 *
 *  EXTERNAL MACROS AND VARIABLES
 *
 *  NOTES
 *   There is a factor of h_p^-1 missing in f since it is
 *   more efficient to apply the factor after integrating.
 *   f/h_p is what you really want.
 *
 *  CALLS
 *   RT_PhotoinozationRatesFunction()   The rates at a given frequency.
 *
 *  AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#if !(defined F_INTEGRAL_PROB_SHARE) & !(defined F_INTEGRAL_PROP_SHARE) & !(defined F_INTEGRAL_ERT)
# define F_INTEGRAL_PROB_SHARE
#endif

#include <stdlib.h>
#include <stdio.h>

#include "RT.h"
#include "GL_weights.h"
#include "RT_ConstantsT.h"
#include "RT_SourceT.h"

/* nu_finish is accepted as a parameter for compatibility, but never used */
int Gauss_Laguerre(rt_float const nu_start,
      /*@unused@*/ rt_float const nu_finish,
                   rt_float const N_H1,
                   rt_float const N_He1,
                   rt_float const N_He2,
                   rt_float const N_H1_cum,
                   rt_float const N_He1_cum,
                   rt_float const N_He2_cum,
                   rt_float const z,
                   RT_SourceT const Source,
                   RT_ConstantsT * Constants,
                   size_t const IntervalIndex,
         /*@out@*/ rt_float * const f ) {

 size_t i, j;
 int ierr;
 rt_float const *Xs;
 rt_float const *W;
 rt_float F[7];
 rt_float nu;
 
 for(i=0;i<7;i++) {
  f[i]=0.;
 }
 
 /* Associate the sample positions and weights with tabulated values */
 switch(Constants->NLevels[IntervalIndex]) {
  case   2: Xs = Xs_02_Laguerre; W = W_02_Laguerre; break;
  case   4: Xs = Xs_04_Laguerre; W = W_04_Laguerre; break;
  case   8: Xs = Xs_08_Laguerre; W = W_08_Laguerre; break;
#if 0
  case  16: Xs = Xs_16_Laguerre; W = W_16_Laguerre; break;
  case  32: Xs = Xs_32_Laguerre; W = W_32_Laguerre; break;
  case  64: Xs = Xs_64_Laguerre; W = W_64_Laguerre; break;
  case 128: Xs = Xs_128_Laguerre; W = W_128_Laguerre; break;
  case 256: Xs = Xs_256_Laguerre; W = W_256_Laguerre; break;
#endif
  default:
   printf("ERROR: Gauss_Laguerre: Unknown level Constants->NLevels[IntervalIndex]=%lu\n",Constants->NLevels[IntervalIndex]);
   (void)fflush(stdout);
   return EXIT_FAILURE;
 }
 
 if(Constants->Alpha[IntervalIndex]==NULL) {
  printf("%s: %i: Initializing absorption cross sections for Interval %lu with %lu levels\n",
         __FILE__,__LINE__,IntervalIndex,Constants->NLevels[IntervalIndex]);
  /* Note, Alpha will never get free()'d */
  Constants->Alpha[IntervalIndex]=(rt_float **)malloc(Constants->NLevels[IntervalIndex]*sizeof(rt_float *));
  for(i=0;i<Constants->NLevels[IntervalIndex];i++) {
   Constants->Alpha[IntervalIndex][i]=(rt_float *)malloc(3*sizeof(rt_float));
   nu = nu_start*(Xs[i]+1.);
   Constants->Alpha[IntervalIndex][i][0] = H1_H2(nu, *Constants);
   if(nu > Constants->nu_1) {
    Constants->Alpha[IntervalIndex][i][1] = He1_He2(nu, *Constants);
    if(nu > Constants->nu_2) {
     Constants->Alpha[IntervalIndex][i][2] = He2_He3(nu, *Constants);
    } else {
     Constants->Alpha[IntervalIndex][i][2] = 0.;
    }
   } else {
    Constants->Alpha[IntervalIndex][i][1] = 0.;
    Constants->Alpha[IntervalIndex][i][2] = 0.;
   }
  }
 }
 
 /* Sum the function evaluations at the sample positions times the weights */
 for(i=0;i<Constants->NLevels[IntervalIndex];i++) {
  nu = nu_start*(Xs[i]+1.);
  ierr = RT_PhotoionizationRatesFunctions(nu,
                                          N_H1,
                                          N_He1,
                                          N_He2,
                                          N_H1_cum,
                                          N_He1_cum,
                                          N_He2_cum,
                                          z,
                                         &Source,
                                          Constants,
                                          Constants->Alpha[IntervalIndex][i],
                                          F);
/* The following is for tests that need to be done. Look in F_Integral for
   these functions, and note that F_Integral_ERT.c has been renamed as
   RT_PhotoionizationRatesFunctions.c as used above.
 */
#if 0
 #ifdef F_INTEGRAL_PROB_SHARE
  ierr = F_integral(nu, N_H1, N_He1, N_He2, N_H1_cum, N_He1_cum, N_He2_cum, z,
                    Constants->Alpha[IntervalIndex][i], F);
 #endif
 #ifdef F_INTEGRAL_PROP_SHARE
  ierr = F_integral_PropShare(nu, N_H1, N_He1, N_He2, N_H1_cum, N_He1_cum, N_He2_cum, z,
                    Constants->Alpha[IntervalIndex][i], F);
 #endif
 #ifdef F_INTEGRAL_ERT
  ierr = F_integral_ERT(nu, N_H1, N_He1, N_He2, N_H1_cum, N_He1_cum, N_He2_cum, z,
                    Constants->Alpha[IntervalIndex][i], F);
 #endif
#endif 
 #ifdef DEBUG
  if(ierr==EXIT_FAILURE) {
   printf("ERROR: Gauss_Laguerre: %i: F_integral failed.\n",__LINE__-2);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 #endif
  for(j=0;j<7;j++) {
   f[j] += W[i]*F[j];
  }
 }
 for(i=0;i<7;i++) {
  f[i] *= nu_start;
 }
 return EXIT_SUCCESS;
}
