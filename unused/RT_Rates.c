/* PREPROCESSOR MACROS
 *  VERBOSE (default 0)
 *  DEBUG
 *  PLANE_WAVE
 *  CONSTANT_DENSITY
 *  CONSTANT_DENSITY_COMOVING
 */

#ifdef HAVE_CONFIG
# include "config.h"
#else
# define VERBOSE 0
#endif

#include <stdlib.h>
#if (defined DEBUG) || (VERBOSE > 0)
# include <stdio.h>
#endif
#include <math.h>
#ifndef M_PI
# define M_PI           3.14159265358979323846  /* pi */
#endif

#include "RT.h"
#include "RT_Data.h"
#include "RT_RatesT.h"
#include "RT_SourceT.h"
#include "RT_ConstantsT.h"
#include "Logic.h"


int RT_Rates(rt_float const ExpansionFactor,
             rt_float const tCurrent,
             RT_SourceT const Source,
             RT_ConstantsT const Constants,
             RT_Cell const * const Data_cell,
   /*@out@*/ RT_RatesT * const Rates ) {

 /* Find the neutral & ionized number densities. */
 rt_float const n_H1  = Data_cell->n_H  * Data_cell->f_H1;
 rt_float const n_H2  = Data_cell->n_H  * Data_cell->f_H2;
 rt_float const n_He1 = Data_cell->n_He * Data_cell->f_He1;
 rt_float const n_He2 = Data_cell->n_He * Data_cell->f_He2;
 rt_float const n_He3 = Data_cell->n_He * Data_cell->f_He3;

 rt_float const tau_H1_cell = Constants.sigma_0 * n_H1 * Data_cell->dR ;

 /* Column density through the shell */
 rt_float const N_H1  = n_H1  * Data_cell->dR;
 rt_float const N_He1 = n_He1 * Data_cell->dR;
 rt_float const N_He2 = n_He2 * Data_cell->dR;

#if VERBOSE > 1
 printf("%s: %i: Ionising and Heating...",__FILE__,__LINE__);
 (void)fflush(stdout);
#endif

 /* Find the redshift of the *source*, in the frame of reference of the
  * observer.  Simply to correct the source spectrum for the light-travel
  * time. This is only correct over small R. */
#ifdef NO_EXPANSION
 /* Pretend the cell is at the present epoch */
 rt_float const tLocal = cosmological_time(1.0, Constants);
 rt_float const a_source = expansion_factor_from_time(tLocal-Data_cell->R/Constants.c,
                                                      Constants);
#else
 rt_float const a_source = expansion_factor_from_time(tCurrent-Data_cell->R/Constants.c,
                                                      Constants);
#endif

 rt_float const DistanceFromSourceMpc = Data_cell->R/Constants.Mpc;
 rt_float const ParticleRadiusMpc = Data_cell->dR/Constants.Mpc;
 rt_float const DistanceThroughElementMpc = (4./3.)*ParticleRadiusMpc;

 /* ******* Start of CPU-intensive code ******* */
 /* ****** 100% of the time is spent here ***** */
 int ierr;
 ierr = RT_PhotoionizationRates(N_H1,
                                N_He1,
                                N_He2,
                                Data_cell->column_H1,
                                Data_cell->column_He1,
                                Data_cell->column_He2,
                                (1./a_source-1.),
                                Source,
                                Constants,
                                DistanceFromSourceMpc,
                                ParticleRadiusMpc,
                                DistanceThroughElementMpc,
                                &(Rates->I_H1),
                                &(Rates->I_He1),
                                &(Rates->I_He2),
                                &(Rates->G_H1),
                                &(Rates->G_He1),
                                &(Rates->G_He2),
                                &(Rates->Gamma_HI) );

#if VERBOSE > 1
 printf("done.\n"); (void)fflush(stdout);
#endif
#ifdef DEBUG
 if (ierr==EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_PhotoionizationRates failed\n",
         __FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif
 /* ******* End of CPU-intensive code ******* */

 /* Total heating rate J m^-3 s^-1 * (*Ma) */
 Rates->G = Rates->G_H1 + Rates->G_He1 + Rates->G_He2;

 /* Electron density */
 rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);
#ifdef DEBUG
 if (!(bool)isfinite(n_e) || (n_e<0) ) {
  printf("ERROR: %s: %i: n_e=%5.3e; n_H2=%5.3e; n_He2=%5.3e; n_He3=%5.3e\n",
         __FILE__,__LINE__,n_e,n_H2,n_He2,n_He3);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
 /* Maximum possible free electron density */
 rt_float const n_emax = Data_cell->n_H + (2.0 * Data_cell->n_He);
 if ( n_e > n_emax*Constants.OnePlusEps ) {
  printf("ERROR: %s: %i: n_e=%5.3e > n_emax=%5.3e;\n",
         __FILE__,__LINE__,n_e,n_emax);
  printf("      n_e - n_emax = %5.3e\n",n_e - n_emax);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

#ifndef CONSTANT_TEMPERATURE
 /* Total number density */
 rt_float const n = Data_cell->n_H + Data_cell->n_He + n_e;
#ifdef DEBUG
 if (!(bool)isfinite(n) || (n <= 0.0)) {
  printf("ERROR: %s: %i: n=%5.3e; n_H=%5.3e; n_He=%5.3e; n_e=%5.3e\n",
         __FILE__,__LINE__,n,Data_cell->n_H,Data_cell->n_He,n_e);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 /* Temperature is a function of Entropy */
 rt_float const mu = (Data_cell->n_H + 4.*Data_cell->n_He)/n; /* Assumes m_He = 4 m_H and mass_e = 0 */
#ifdef DEBUG
 if (!(bool)isfinite(mu) || (mu<=0)) {
  printf("ERROR: %s %i: mu=%5.3f; n_H=%5.3e; n_He=%5.3e; n=%5.3e\n",
         __FILE__,__LINE__,mu,Data_cell->n_H,Data_cell->n_He,n);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif
 /* Why can't we use Data_cell->Density ? */
 rt_float const D = ( Constants.mass_p + Constants.mass_e )
                 *( Data_cell->n_H + 4.0*Data_cell->n_He );
#ifdef DEBUG
 if( !(bool)isfinite(D) || (D<=0.) ) {
  printf("ERROR: %s: %i: D=%5.3e\n",__FILE__,__LINE__,D);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif
 rt_float const T =  mu * ( Constants.mass_p / Constants.k_B )
                 * Data_cell->Entropy
                 * POW(D,Constants.gamma_ad-1.0);
#ifdef DEBUG
 if ( !(bool)isfinite(T) || (T<=0) ) {
  printf("ERROR: %s: %i: T=%5.3e; Entropy=%5.3e; mu=%5.3f; D=%5.3e\n",
         __FILE__,__LINE__, T, Data_cell->Entropy, mu, D);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif
#else
 rt_float const T = CONSTANT_TEMPERATURE;
#endif

 /* The Recombination Coefficients */
 rt_float beta_H1, beta_He1, beta_He2;
 ierr = RT_RecombinationCoefficients(tau_H1_cell,
                                     T,
                                    &(Rates->alpha_H1),
                                    &(Rates->alpha_He1),
                                    &(Rates->alpha_He2),
                                    &beta_H1,
                                    &beta_He1,
                                    &beta_He2);
#ifdef DEBUG
 if(ierr == EXIT_FAILURE) {
  printf("ERROR: %s: %i: RT_RecombinationCoefficients failed.\n",
         __FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

 Rates->L += n_e * n_H2  * (beta_H1  * Constants.Ma);
 Rates->L += n_e * n_He2 * (beta_He1 * Constants.Ma);
 Rates->L += n_e * n_He3 * (beta_He2 * Constants.Ma);

#ifndef NO_COOLING
 RT_CoolingRatesExtra_Cell(ExpansionFactor,
                           Constants,
                           Data_cell,
                           Rates );
#endif

 return EXIT_SUCCESS;
}
