/* RT_PhotoionizationRatesIntegrate: Integrate the photoionisation and photoionisation
 *      heating rate functions.  Specifically, divide the integral into
 *      three parts and evaluate the integral using a method depending on
 *      compile-time configuration.
 *
 * int RT_PhotoionizationRatesIntegrate(rt_float N_H1, rt_float N_He1, rt_float N_He2,
 *                       rt_float N_H1_cum, rt_float N_He1_cum, rt_float N_He2_cum,
 *                       rt_float z, RT_Source *Source, rt_float f[7])
 *
 * ARGUMENTS
 *  Input, not modified
 *   N_H1, N_He1, N_He2         Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum     Cumulative column densities between
 *   z          Redshift of source (used if the source is a function of time).
 *   Source     The Source
 *
 *  Output
 *   f[7]       Rates at frequency, nu. [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2]
 *              I_* are the photoionisation rates (* h_p). [s^-1]
 *              G_* are the heating rates. [J s^-1]
 *              Gamma_HI is the photoionization rate per particle [m^3 s^-1 Hz^-1]
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * NOTE
 *  Splits the integral into three spans (nu_0 to nu_1), (nu_1 to nu_2), and
 *  (nu_2 to infinity).  Over these spans, the attenuated source spectrum is
 *  continuous.
 *
 * CALLS
 *  Gauss_Legendre()
 *  Gauss_Laguerre()
 *
 * AUTHOR: Eric Tittley
 */

/* A note about the "Alpha" variable.
 *
 * The cross-sections (Alphas) are expensive to calclulate but are used
 * repetitively in the integration over nu at each of the sample frequencies,
 * nu_i.
 *
 * Since the cross-sections are only a function of frequency and, for a given
 * problem, the sample points, nu_i, are consistent from iteration to
 * iteration, it makes sense to calculate the cross-sections only once and
 * store the values for reuse. Unfortunately, to keep the integration as
 * general as possible (permitting, for example, easy swapping of integration
 * methods), a certain amount of clunky bookkeeping is required.
 *
 * Alpha[] contains pointers, one for each integration range.
 * Each Alpha[i] points to a 2D pointer which contains the three
 * cross-sections (H1 to H2, He1 to He2, He2 to He3), at each of the
 * frequencies the integration method in the integration range intends to use.
 *
 * Since we can't know, a priori, what each of the methods will choose for
 * the sample frequencies, let the methods calculate the values on the first
 * pass, signified to them by NULL for the values of the Alpha[i] pointers.
 *
 * Since the integration methods are allocating the memory and setting the
 * pointers, the pointers must be sent to the methods by address, which is
 * always odd-looking.
 *
 * Since Alpha is static, that memory will be retained between calls.
 *
 * Note that nowhere is the memory free()'d, but it shouldn't grow.
 */

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>
#ifdef DEBUG
# include <stdio.h>
# include <math.h>
#endif

#include "RT.h"
#include "RT_SourceT.h"
#include "RT_ConstantsT.h"
#include "Logic.h"

/**********************************************************************
 * Integration via parts: nu_0 to nu_1, nu_1 to nu_2, nu_2 to nu_end. *
 * This assists convergence since it avoid the discontinuities at     *
 * these frequencies.                                                 *
 **********************************************************************/
int RT_PhotoionizationRatesIntegrate(rt_float const N_H1,
                                     rt_float const N_He1,
                                     rt_float const N_He2,
                                     rt_float const N_H1_cum,
                                     rt_float const N_He1_cum,
                                     rt_float const N_He2_cum,
                                     rt_float const z,
                                     RT_SourceT const Source,
                                     RT_ConstantsT Constants,
                           /*@out@*/ rt_float * const f)
{
 rt_float sum[7];
 size_t i;
 int ierr;

 for(i=0;i<7;i++) {
  f[i]=0.;
 }

 ierr = Gauss_Legendre(Constants.nu_0,
                       Constants.nu_1,
                       N_H1,
                       N_He1,
                       N_He2,
                       N_H1_cum,
                       N_He1_cum,
                       N_He2_cum,
                       z,
                       Source,
                      &Constants,
                       0,
                       sum);
#ifdef DEBUG
 if(ierr==EXIT_FAILURE) {
  printf("ERROR: %s: %i: Integration failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
 for(i=0;i<7;i++) {
  if( !(bool)isfinite(sum[i]) ) {
   printf("ERROR: %s: %i: sum[%lu]=%f.\n",__FILE__,__LINE__,i,sum[i]);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 }

#endif
 for(i=0;i<7;i++) {
  f[i] += sum[i];
 }
 
 ierr = Gauss_Legendre(Constants.nu_1,
                       Constants.nu_2,
                       N_H1,
                       N_He1,
                       N_He2,
                       N_H1_cum,
                       N_He1_cum,
                       N_He2_cum,
                       z,
                       Source,
                      &Constants,
                       1,
                       sum);
#ifdef DEBUG
 if(ierr==EXIT_FAILURE) {
  printf("ERROR: %s: %i: Integration failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
 for(i=0;i<7;i++) {
  if( !(bool)isfinite(sum[i]) ) {
   printf("ERROR: %s: %i: sum[%lu]=%f.\n",__FILE__,__LINE__,i,sum[i]);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 }
#endif
 for(i=0;i<7;i++) {
  f[i] += sum[i];
 }

 ierr = Gauss_Laguerre(Constants.nu_2,
                       Source.nu_end,
                       N_H1,
                       N_He1,
                       N_He2,
                       N_H1_cum,
                       N_He1_cum,
                       N_He2_cum,
                       z,
                       Source,
                      &Constants,
                       2,
                       sum);
#ifdef DEBUG
 if(ierr==EXIT_FAILURE) {
  printf("ERROR: %s: %i: Integration failed.\n",__FILE__,__LINE__);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
 for(i=0;i<7;i++) {
  if( !(bool)isfinite(sum[i]) ) {
   printf("ERROR: %s: %i: sum[%lu]=%f.\n",__FILE__,__LINE__,i,sum[i]);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 }
#endif
 for(i=0;i<7;i++) {
  f[i] += sum[i];
 }

 return EXIT_SUCCESS;
}
