#include <stdlib.h>

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include "RT_RatesT.h"
#include "RT.h"

rt_float RT_Rates_RK_Add(rt_float const R1, rt_float const R2, rt_float const R3, rt_float const R4) {
 return (R1 + 2.0*(R2+R3) + R4)/6.0;
}

int RT_Rates_RK(RT_RatesT const * const Rates0,
                RT_RatesT const * const Rates1,
                RT_RatesT const * const Rates2,
                RT_RatesT const * const Rates3,
      /*@out@*/ RT_RatesT * const Rates) {

 Rates->I_H1      = RT_Rates_RK_Add(Rates0->I_H1,      Rates1->I_H1,      Rates2->I_H1,     Rates3->I_H1      );
 Rates->I_He1     = RT_Rates_RK_Add(Rates0->I_He1,     Rates1->I_He1,     Rates2->I_He1,     Rates3->I_He1    );
 Rates->I_He2     = RT_Rates_RK_Add(Rates0->I_He2,     Rates1->I_He2,     Rates2->I_He2,     Rates3->I_He2    );
 Rates->G         = RT_Rates_RK_Add(Rates0->G,         Rates1->G,         Rates2->G,         Rates3->G        );
 Rates->G_H1      = RT_Rates_RK_Add(Rates0->G_H1,      Rates1->G_H1,      Rates2->G_H1,      Rates3->G_H1     );
 Rates->G_He1     = RT_Rates_RK_Add(Rates0->G_He1,     Rates1->G_He1,     Rates2->G_He1,     Rates3->G_He1    );
 Rates->G_He2     = RT_Rates_RK_Add(Rates0->G_He2,     Rates1->G_He2,     Rates2->G_He2,     Rates3->G_He2    );
 Rates->alpha_H1  = RT_Rates_RK_Add(Rates0->alpha_H1,  Rates1->alpha_H1,  Rates2->alpha_H1,  Rates3->alpha_H1 );
 Rates->alpha_He1 = RT_Rates_RK_Add(Rates0->alpha_He1, Rates1->alpha_He1, Rates2->alpha_He1, Rates3->alpha_He1);
 Rates->alpha_He2 = RT_Rates_RK_Add(Rates0->alpha_He2, Rates1->alpha_He2, Rates2->alpha_He2, Rates3->alpha_He2);
 Rates->L         = RT_Rates_RK_Add(Rates0->L,         Rates1->L,         Rates2->L,         Rates3->L        );
 return EXIT_SUCCESS;
}
