/* RT_Data_InitializeTemperatureAndFractions: Initialize the temperature and
 * species fractions of an RT_Data grid.
 *
 *
 * ARGUMENTS
 *  Input, not modified
 *   Nlos       Number of lines of sight
 *   Constants  Universal Constants
 *   Redshift   The redshift at the time of initialization
 *  Modified
 *   Data       The data grid to initialize
 *
 * NOTES
 *  If a constant density is not being set, then to calculate initial entropy,
 * the cosmic mean value is assumed.
 *
 * AUTHOR
 *  Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT.h"

void RT_Data_InitializeTemperatureAndFractions(size_t const Nlos,
                                               RT_ConstantsT const Constants,
                                               rt_float const Redshift,
                                               RT_Data** const Data) {
  /* Initialize temperature and entropy */
#if defined(CONSTANT_DENSITY) && !defined(CONSTANT_DENSITY_COMOVING)
  rt_float const T_init = Constants.T_CMB_0;
#else
  rt_float const T_init = Constants.T_CMB_0 * (1. + Redshift);
#endif
  rt_float const mu = 4. / (4. - 3. * Constants.Y);
#ifndef CONSTANT_DENSITY
  rt_float const rho_c = 1.879e-26 * Constants.h * Constants.h *
                         POW((1. + Redshift), 3.); /* critical density kg/m^3 */
  rt_float const rho = Constants.omega_b * rho_c;
#else
#  ifdef CONSTANT_DENSITY_COMOVING
  rt_float const rho = 1. * POW((1. + Redshift), 3.);
#  else
  rt_float const rho = 1.;
#  endif
#endif
  rt_float const Entropy_init =
      Constants.k_B * T_init /
      (mu * Constants.mass_p * POW(rho, Constants.gamma_ad - 1));
  size_t j, i;
  for (j = 0; j < Nlos; ++j) {
    for (i = 0; i < Ncells; ++i) {
      Data[j]->Entropy[i] = Entropy_init;
      Data[j]->T[i] = T_init;
      Data[j]->f_H1[i] = 1.;
      Data[j]->f_H2[i] = 0.;
      Data[j]->f_He1[i] = 1.;
      Data[j]->f_He2[i] = 0.;
      Data[j]->f_He3[i] = 0.;
    }
#if (defined REFINEMENTS) || (defined SEGMENTED_LOS)
    /* The cumulative column densities up to the start of the line of sight */
    for (i = 0; i < 12; i++) {
      Data[j]->NCol[i] = 0.;
    }
#endif
  }
}
