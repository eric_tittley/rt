#ifndef _RT_PRECISION_H_
#define _RT_PRECISION_H_

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math.h>

#ifdef RT_DOUBLE
typedef double rt_float;
#  define RT_FLOAT DOUBLE
#  define EXP exp
#  define EXP10 exp10
#  define POW pow
#  define SQRT sqrt
#  define CBRT cbrt
#  define LOG log
#  define LOG10 log10
#  define COSH cosh
#  define SINH sinh
#  define ASINH asinh
#  define FABS fabs
#  define FMAX fmax
#  define FMIN fmin
#  define FLOOR floor
#  define CEIL ceil
#  define PETA 1e15L
#  define FEMTO 1e-15L
#  define FLOATSTR "%lf"
#  define RT_FLT_C(num) num
#  define RT_PI M_PI
#else
typedef float rt_float;
#  define RT_FLOAT FLOAT
#  define EXP expf
#  define EXP10 exp10f
#  define POW powf
#  define SQRT sqrtf
#  define CBRT cbrtf
#  define LOG logf
#  define LOG10 log10f
#  define COSH coshf
#  define SINH sinhf
#  define ASINH asinhf
#  define FABS fabsf
#  define FMAX fmaxf
#  define FMIN fminf
#  define FLOOR floorf
#  define CEIL ceilf
#  define PETA 1e15F
#  define FEMTO 1e-15F
#  define FLOATSTR "%f"
#  define RT_FLT_C(num) num ## f
#  define RT_PI ((float) M_PI)
#endif

#endif
