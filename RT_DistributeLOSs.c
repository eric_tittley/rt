/* Farm out the LOSs.  RT_ProcessLOS() does the RT on each LOS.
 *
 * ARGUMENTS
 *  Input, not modified
 *   Nlos       The number of LOS's to farm out.
 *   t          Simulation time (s)
 *   dt         Timestep (s)
 *   aa         Expansion factor at the *end* of the iteration.
 *   Source     The source parameters
 *   Constants  Physical constants
 *  Input, modified
 *   Data       The state of the gas in all cells
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE If unable to write output file or RT_ProcessLOS fails.
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _OPENMP
#  include <omp.h>
#endif

#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_SourceT.h"

int RT_DistributeLOSs(size_t const Nlos,
                      rt_float const t,
                      rt_float const dt,
                      rt_float const aa,
                      RT_SourceT const Source,
                      RT_ConstantsT const Constants,
                      RT_Data** const Data) {
#ifdef DEBUG
  if (dt < 0.) {
    printf("ERROR: %s: dt < 0: dt=%f\n", __FILE__, dt);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  size_t los;
#pragma omp parallel for schedule(dynamic, 1) private(los) \
    shared(stdout)
  for (los = 0; los < Nlos; los++) {
#if VERBOSE >= 1
    printf("%s: los=%lu of %lu\n", __FILE__, los, Nlos - 1);
    (void)fflush(stdout);
#endif
    /* Main Loop for a 1D line of sight */
    /* RT_ProcessLOS modifies contents of the structure Data_los */
#if VERBOSE >= 1
    printf("%s: Calling RT_ProcessLOS for los=%lu.\n", __FILE__, los);
    (void)fflush(stdout);
#endif
    /* aa is the expansion factor at the end of the iteration.
     * Strictly, aa should grow during the integration in RT_ProcessLOS. */
#if (defined DEBUG) & !(defined _OPENMP)
    int ierr = RT_ProcessLOS(t, dt, aa, Source, Constants, Data[los]);
    if (ierr == EXIT_FAILURE) {
      printf("ERROR: %s: %i: RT_ProcessLOS failed. los=%lu\n",
             __FILE__,
             __LINE__,
             los);
      (void)fflush(stdout);
      return EXIT_FAILURE;
    }
#else
    RT_ProcessLOS(t, dt, aa, Source, Constants, Data[los]);
#endif
  }
#if VERBOSE >= 1
  printf("%s: Leaving.\n", __FILE__);
  (void)fflush(stdout);
#endif

  return EXIT_SUCCESS;
}
