/*
 * PREPROCESSOR MACROS
 *  RT_DATA_VELOCITIES
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_Data.h"

void RT_Cell_Pack(size_t const iz,
                  RT_Data const* const Data_los,
                  /*@out@*/ RT_Cell* const Data_cell) {
  Data_cell->R = Data_los->R[iz];
  Data_cell->dR = Data_los->dR[iz];
  Data_cell->Density = Data_los->Density[iz];
  Data_cell->Entropy = Data_los->Entropy[iz];
  Data_cell->T = Data_los->T[iz];
  Data_cell->n_H = Data_los->n_H[iz];
  Data_cell->f_H1 = Data_los->f_H1[iz];
  Data_cell->f_H2 = Data_los->f_H2[iz];
  Data_cell->n_He = Data_los->n_He[iz];
  Data_cell->f_He1 = Data_los->f_He1[iz];
  Data_cell->f_He2 = Data_los->f_He2[iz];
  Data_cell->f_He3 = Data_los->f_He3[iz];
  Data_cell->column_H1 = Data_los->column_H1[iz];
  Data_cell->column_He1 = Data_los->column_He1[iz];
  Data_cell->column_He2 = Data_los->column_He2[iz];
#ifdef RT_DATA_VELOCITIES
  Data_cell->v_z = Data_los->v_z[iz];
  Data_cell->v_x = Data_los->v_x[iz];
#endif
}
