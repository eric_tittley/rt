#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "RT.h"
#include "RT_ConstantsT.h"
#include "libPF/PF.h"

int RT_ReadConstantsFile(char const *const ConstantsFile,
                         RT_ConstantsT *const Constants) {
  int ierr;
  PF_ParameterEntry *ConstantEntries = NULL;
  FILE *File;

  /* Initialize Constants with defaults */
  /* Physical Constants */
  /* Ionisation thresholds */
  Constants->nu_0 = 3.282e15;
  Constants->nu_1 = 5.933e15;
  Constants->nu_2 = 1.313e16;
  /* Constants for interpolation formula for cross-section (m^2),
   * Osterbrock p.36 */
  Constants->sigma_0 = 6.30e-22;
  Constants->sigma_1 = 7.83e-22;
  Constants->sigma_2 = 1.58e-22;
  /* From CODATA (physics.nist.gov) */
  Constants->c = 2.99792458e8;        /* Speed of light (m/s) */
  Constants->h_p = 6.6260688e-34;     /* Planck constant. (J s) */
  Constants->k_B = 1.380650e-23;      /* Boltzmann constant (J/K) */
  Constants->mass_e = 9.1093819e-31;  /* electron mass (kg) */
  Constants->mass_p = 1.6726217e-27;  /* proton mass (kg) */
  Constants->sigma_T = 6.6524585e-29; /* Thomson cross section (m^2) */
  Constants->a = 7.565760e-16; /* Radiation density constant ( J m^-3 K^-4 ) */
                               /* From Astrophysical Formulae (Lang) */
  Constants->Mpc = 3.085677582e22; /* 1 Mpc (m) */
  /* From Kaplinghat & Turner (2001 PhRvL 86 385K) */
  Constants->T_CMB_0 = 2.725;    /* Present CMB temperature (K) */
  Constants->gamma_ad = 5. / 3.; /* Adiabatic index for a monoatomic gas */
  Constants->Ma = 3600.0 * 24.0 * 365.25 * 1.0e6; /* Ma in s */

  /* Cosmological Constants */
  Constants->Y = 0.235;        /* Helium mass fraction */
                               /* (from WMAP 7-yr data) */
  Constants->h = 0.73;         /* h_100.  Must match H_0, omega_[bmv] below */
  Constants->omega_b = 0.0458; /* Chosen so that omega_b*h^2 = 0.022 */
  Constants->omega_m = 0.275;  /* Density of all matter / critical density */
  Constants->omega_v = 0.725;  /* Vacuum energy density */

  /* Configuration Parameters */
  /* The maximum allowable optical depth in a cell. Otherwise, refine.*/
  Constants->Max_tau_in_cell = 1.0;
  /* The maximum cumulative optical depth in order to do refinements.
   * If the optical depth in a cell is high (see above) only do refinements if
   * photons are actually reaching the cell (the cumulative optical depth is
   * low).
   */
  Constants->Max_cumulative_tau_for_refinement = 2.0;
  /* MIN_DT is misleadingly named. It is *not* a hard lower limit to timesteps.
   * If ADAPTIVE_TIMESCALE is defined, then MIN_DT is applied when timescales
   * are not yet known: on the first RT iteration (1 per PM iteration) and when
   * the lightfront first enters a cell.  So mostly in the epoch before the
   * light front has swept through the volume.
   * If ADAPTIVE_TIMESCALE is not defined, then MIN_DT is the time step in the
   * Radiative Transfer Section. As a guide, MIN_DT <= 1e14 s */
  Constants->MIN_DT = 2e11;
  /* MAX_DT sets a hard maximum timestep. Should not be needed with adaptive
   * timesteps, but may be needed to sychronise LOS segments on different
   * grids. For reference, the Hubble time is 4e17s.
   * Tests (10 04 27) indicate very little runtime penalty (and potentially
   * a benefit) until MAX_DT < 5e11 but clear benefits for MAX_DT < 5e13 .
   * MAX_DT < MIN_DT will probably break things. */
  Constants->MAX_DT = 5e12;
  /* If ADAPTIVE_TIMESCALE, then the RT timestep is set to
   * (Timestep_Factor * the suggested adaptive timescale).  Tests find
   * convergence at 0.2 */
  Constants->Timestep_Factor = 0.2;
  /* xHIEquilibriumThreshold defines the HI fraction below which species
   * fractions for Hydrogen are set to equilibrium values.
   * This functionality is necessary to prevent timestepping instabilities which
   * crop up when fractions approach equilibrium values. */
  Constants->xHIEquilibriumThreshold = 0.01;
  Constants->OnePlusEps = 1.000001;
  Constants->OneMinusEps = 0.999999;

  /* Allocate memory */
  ConstantEntries =
      (PF_ParameterEntry *)malloc(sizeof(PF_ParameterEntry) * nConstants);

  /* Compile ConstantEntries array of structures */

  /* Physical Constants */
  strncpy(ConstantEntries[inu_0].Parameter, "nu_0", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[inu_0].Type = RT_FLOAT;
  ConstantEntries[inu_0].Pointer = &(Constants->nu_0);
  ConstantEntries[inu_0].IsBoolean = 0;
  ConstantEntries[inu_0].IsArray = 0;

  strncpy(ConstantEntries[inu_1].Parameter, "nu_1", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[inu_1].Type = RT_FLOAT;
  ConstantEntries[inu_1].Pointer = &(Constants->nu_1);
  ConstantEntries[inu_1].IsBoolean = 0;
  ConstantEntries[inu_1].IsArray = 0;

  strncpy(ConstantEntries[inu_2].Parameter, "nu_2", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[inu_2].Type = RT_FLOAT;
  ConstantEntries[inu_2].Pointer = &(Constants->nu_2);
  ConstantEntries[inu_2].IsBoolean = 0;
  ConstantEntries[inu_2].IsArray = 0;

  strncpy(ConstantEntries[isigma_0].Parameter,
          "sigma_0",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[isigma_0].Type = RT_FLOAT;
  ConstantEntries[isigma_0].Pointer = &(Constants->sigma_0);
  ConstantEntries[isigma_0].IsBoolean = 0;
  ConstantEntries[isigma_0].IsArray = 0;

  strncpy(ConstantEntries[isigma_1].Parameter,
          "sigma_1",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[isigma_1].Type = RT_FLOAT;
  ConstantEntries[isigma_1].Pointer = &(Constants->sigma_1);
  ConstantEntries[isigma_1].IsBoolean = 0;
  ConstantEntries[isigma_1].IsArray = 0;

  strncpy(ConstantEntries[isigma_2].Parameter,
          "sigma_2",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[isigma_2].Type = RT_FLOAT;
  ConstantEntries[isigma_2].Pointer = &(Constants->sigma_2);
  ConstantEntries[isigma_2].IsBoolean = 0;
  ConstantEntries[isigma_2].IsArray = 0;

  strncpy(ConstantEntries[ih_p].Parameter, "h_p", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[ih_p].Type = RT_FLOAT;
  ConstantEntries[ih_p].Pointer = &(Constants->h_p);
  ConstantEntries[ih_p].IsBoolean = 0;
  ConstantEntries[ih_p].IsArray = 0;

  strncpy(ConstantEntries[ik_B].Parameter, "k_B", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[ik_B].Type = RT_FLOAT;
  ConstantEntries[ik_B].Pointer = &(Constants->k_B);
  ConstantEntries[ik_B].IsBoolean = 0;
  ConstantEntries[ik_B].IsArray = 0;

  strncpy(ConstantEntries[ic].Parameter, "c", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[ic].Type = RT_FLOAT;
  ConstantEntries[ic].Pointer = &(Constants->c);
  ConstantEntries[ic].IsBoolean = 0;
  ConstantEntries[ic].IsArray = 0;

  strncpy(
      ConstantEntries[imass_e].Parameter, "mass_e", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[imass_e].Type = RT_FLOAT;
  ConstantEntries[imass_e].Pointer = &(Constants->mass_e);
  ConstantEntries[imass_e].IsBoolean = 0;
  ConstantEntries[imass_e].IsArray = 0;

  strncpy(
      ConstantEntries[imass_p].Parameter, "mass_p", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[imass_p].Type = RT_FLOAT;
  ConstantEntries[imass_p].Pointer = &(Constants->mass_p);
  ConstantEntries[imass_p].IsBoolean = 0;
  ConstantEntries[imass_p].IsArray = 0;

  strncpy(ConstantEntries[isigma_T].Parameter,
          "sigma_T",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[isigma_T].Type = RT_FLOAT;
  ConstantEntries[isigma_T].Pointer = &(Constants->sigma_T);
  ConstantEntries[isigma_T].IsBoolean = 0;
  ConstantEntries[isigma_T].IsArray = 0;

  strncpy(ConstantEntries[ia].Parameter, "a", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[ia].Type = RT_FLOAT;
  ConstantEntries[ia].Pointer = &(Constants->a);
  ConstantEntries[ia].IsBoolean = 0;
  ConstantEntries[ia].IsArray = 0;

  strncpy(ConstantEntries[iMa].Parameter, "Ma", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iMa].Type = RT_FLOAT;
  ConstantEntries[iMa].Pointer = &(Constants->Ma);
  ConstantEntries[iMa].IsBoolean = 0;
  ConstantEntries[iMa].IsArray = 0;

  strncpy(ConstantEntries[iMpc].Parameter, "Mpc", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iMpc].Type = RT_FLOAT;
  ConstantEntries[iMpc].Pointer = &(Constants->Mpc);
  ConstantEntries[iMpc].IsBoolean = 0;
  ConstantEntries[iMpc].IsArray = 0;

  strncpy(ConstantEntries[igamma_ad].Parameter,
          "gamma_ad",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[igamma_ad].Type = RT_FLOAT;
  ConstantEntries[igamma_ad].Pointer = &(Constants->gamma_ad);
  ConstantEntries[igamma_ad].IsBoolean = 0;
  ConstantEntries[igamma_ad].IsArray = 0;

  /* Cosmological Constants */
  strncpy(ConstantEntries[iY].Parameter, "Y", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iY].Type = RT_FLOAT;
  ConstantEntries[iY].Pointer = &(Constants->Y);
  ConstantEntries[iY].IsBoolean = 0;
  ConstantEntries[iY].IsArray = 0;

  strncpy(ConstantEntries[ih].Parameter, "h", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[ih].Type = RT_FLOAT;
  ConstantEntries[ih].Pointer = &(Constants->h);
  ConstantEntries[ih].IsBoolean = 0;
  ConstantEntries[ih].IsArray = 0;

  strncpy(ConstantEntries[iomega_m].Parameter,
          "omega_m",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iomega_m].Type = RT_FLOAT;
  ConstantEntries[iomega_m].Pointer = &(Constants->omega_m);
  ConstantEntries[iomega_m].IsBoolean = 0;
  ConstantEntries[iomega_m].IsArray = 0;

  strncpy(ConstantEntries[iomega_b].Parameter,
          "omega_b",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iomega_b].Type = RT_FLOAT;
  ConstantEntries[iomega_b].Pointer = &(Constants->omega_b);
  ConstantEntries[iomega_b].IsBoolean = 0;
  ConstantEntries[iomega_b].IsArray = 0;

  strncpy(ConstantEntries[iomega_v].Parameter,
          "omega_v",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iomega_v].Type = RT_FLOAT;
  ConstantEntries[iomega_v].Pointer = &(Constants->omega_v);
  ConstantEntries[iomega_v].IsBoolean = 0;
  ConstantEntries[iomega_v].IsArray = 0;

  strncpy(ConstantEntries[iT_CMB_0].Parameter,
          "T_CMB_0",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iT_CMB_0].Type = RT_FLOAT;
  ConstantEntries[iT_CMB_0].Pointer = &(Constants->T_CMB_0);
  ConstantEntries[iT_CMB_0].IsBoolean = 0;
  ConstantEntries[iT_CMB_0].IsArray = 0;

  strncpy(ConstantEntries[iMax_tau_in_cell].Parameter,
          "Max_tau_in_cell",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iMax_tau_in_cell].Type = RT_FLOAT;
  ConstantEntries[iMax_tau_in_cell].Pointer = &(Constants->Max_tau_in_cell);
  ConstantEntries[iMax_tau_in_cell].IsBoolean = 0;
  ConstantEntries[iMax_tau_in_cell].IsArray = 0;

  strncpy(ConstantEntries[iMax_cumulative_tau_for_refinement].Parameter,
          "Max_cumulative_tau_for_refinement",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iMax_cumulative_tau_for_refinement].Type = RT_FLOAT;
  ConstantEntries[iMax_cumulative_tau_for_refinement].Pointer =
      &(Constants->Max_cumulative_tau_for_refinement);
  ConstantEntries[iMax_cumulative_tau_for_refinement].IsBoolean = 0;
  ConstantEntries[iMax_cumulative_tau_for_refinement].IsArray = 0;

  strncpy(
      ConstantEntries[iMIN_DT].Parameter, "MIN_DT", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iMIN_DT].Type = RT_FLOAT;
  ConstantEntries[iMIN_DT].Pointer = &(Constants->MIN_DT);
  ConstantEntries[iMIN_DT].IsBoolean = 0;
  ConstantEntries[iMIN_DT].IsArray = 0;

  strncpy(
      ConstantEntries[iMAX_DT].Parameter, "MAX_DT", MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iMAX_DT].Type = RT_FLOAT;
  ConstantEntries[iMAX_DT].Pointer = &(Constants->MAX_DT);
  ConstantEntries[iMAX_DT].IsBoolean = 0;
  ConstantEntries[iMAX_DT].IsArray = 0;

  strncpy(ConstantEntries[iTimestep_Factor].Parameter,
          "Timestep_Factor",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iTimestep_Factor].Type = RT_FLOAT;
  ConstantEntries[iTimestep_Factor].Pointer = &(Constants->Timestep_Factor);
  ConstantEntries[iTimestep_Factor].IsBoolean = 0;
  ConstantEntries[iTimestep_Factor].IsArray = 0;

  strncpy(ConstantEntries[ixHIEquilibriumThreshold].Parameter,
          "xHIEquilibriumThreshold",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[ixHIEquilibriumThreshold].Type = RT_FLOAT;
  ConstantEntries[ixHIEquilibriumThreshold].Pointer =
      &(Constants->xHIEquilibriumThreshold);
  ConstantEntries[ixHIEquilibriumThreshold].IsBoolean = 0;
  ConstantEntries[ixHIEquilibriumThreshold].IsArray = 0;

  strncpy(ConstantEntries[iOnePlusEps].Parameter,
          "OnePlusEps",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iOnePlusEps].Type = RT_FLOAT;
  ConstantEntries[iOnePlusEps].Pointer = &(Constants->OnePlusEps);
  ConstantEntries[iOnePlusEps].IsBoolean = 0;
  ConstantEntries[iOnePlusEps].IsArray = 0;

  strncpy(ConstantEntries[iOneMinusEps].Parameter,
          "OneMinusEps",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iOneMinusEps].Type = RT_FLOAT;
  ConstantEntries[iOneMinusEps].Pointer = &(Constants->OneMinusEps);
  ConstantEntries[iOneMinusEps].IsBoolean = 0;
  ConstantEntries[iOneMinusEps].IsArray = 0;

  strncpy(ConstantEntries[iNLevels].Parameter,
          "NLevels",
          MAX_PARAMETER_NAME_LENGTH);
  ConstantEntries[iNLevels].Type = UNSIGNED_LONG_INTEGER;
  ConstantEntries[iNLevels].Pointer = &(Constants->NLevels);
  ConstantEntries[iNLevels].IsBoolean = 0;
  ConstantEntries[iNLevels].IsArray = 1;
  ConstantEntries[iNLevels].NArrayElements = &(Constants->NLevels_NIntervals);

  /* Open Constants file for reading */
  File = fopen(ConstantsFile, "r");
  if (File == NULL) {
    printf("%s: %i: ERROR: failed to load file %s\n",
           __FILE__,
           __LINE__,
           ConstantsFile);
    return EXIT_FAILURE;
  }

  /* Read the Constants */
  ierr = PF_ReadParameterFile(File, ConstantEntries, nConstants);
  if (ierr != EXIT_SUCCESS) {
    printf("%s: %i: ERROR: PF_ReadParameterFile failed\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  /* Sanity checks
   * These should actually be done by the PF library */
  if ((Constants->Y < 0.) || (Constants->Y > 1.)) {
    printf("%s: %i: ERROR: Y is out of range [0 1]: %lf\n",
           __FILE__,
           __LINE__,
           Constants->Y);
    return EXIT_FAILURE;
  }

  /* Print the Constants using PF_WriteConstants */
  ierr = PF_WriteParameters(ConstantEntries, nConstants);
  if (ierr != EXIT_SUCCESS) {
    printf("%s: %i: ERROR: PF_WriteConstants failed\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  /* Free the structure */
  free(ConstantEntries);

  /* Close the file */
  fclose(File);

  return EXIT_SUCCESS;
}
