/* Read a RT_Data structure and associated memory from a file stream.  Does not
 * read the refinements.  The necessary memory is allocated here and must later
 * be freed.
 *
 * ARGUMENTS
 *  fid  Pointer to the file stream from which to read.
 *
 * RETURNS
 *  Pointer to a RT_Data structure containing the data.
 *
 * SEE ALSO
 *  RT_Data_Write, RT_Data_Free
 *
 * AUTHOR: Eric Tittley
 */

#include <stdio.h>
#include <stdlib.h>

#include "RT_Data.h"

/*@null@*/ RT_Data *RT_Data_Read(FILE *const fid) {
  size_t Nread;

  /* The number of cells in this LOS. */
  size_t NumCells;
  Nread = fread(&(NumCells), sizeof(size_t), 1, fid);
  if (Nread != 1) {
    printf("ERROR: %s: %i: Unable to read RT_Data.NumCells.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return NULL;
  }

  /* The cell # for which this LOS is a refinement. */
  size_t Cell;
  Nread = fread(&(Cell), sizeof(size_t), 1, fid);
  if (Nread != 1) {
    printf("ERROR: %s: %i: Unable to read RT_Data.Cell.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return NULL;
  }

  /* The amount of memory in the data space. */
  size_t NBytes;
  Nread = fread(&(NBytes), sizeof(size_t), 1, fid);
  if (Nread != 1) {
    printf(
        "ERROR: %s: %i: Unable to read RT_Data.NBytes.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return NULL;
  }

  /* Allocate memory for the index and the data space */
  RT_Data *Data = RT_Data_Allocate(NumCells);
  if (Data == NULL) {
    printf("ERROR: %s: %i: Allocation of memory failed.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return NULL;
  }

  /* Consistency Check */
  if (Data->NBytes != NBytes) {
    printf("ERROR: %s: %i: Internal consistency check failed.\n",
           __FILE__,
           __LINE__);
    printf(
        "       Perhaps the data file was produced by a different version.\n");
    printf("Data->NBytes = %lu; NBytes = %lu\n", Data->NBytes, NBytes);
    (void)fflush(stdout);
    free(Data);
    return NULL;
  }

  Data->Cell = Cell;

  /* Read in the data space */
  Nread = fread(Data->MemSpace, Data->NBytes, 1, fid);
  if (Nread != 1) {
    printf("ERROR: %s: %i: Unable to read RT_Data.MemSpace.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    free(Data);
    return NULL;
  }

  return Data;
}
