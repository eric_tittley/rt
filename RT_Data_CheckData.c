#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_Data.h"

#include <stdlib.h>

#define NPP_MINABS_32F ( 1.175494351e-38f )

int RT_Data_CheckData(RT_Data const Data, const char * File, const int Line) {

 unsigned int ErrorFlag=0;

#if 0
  for (size_t i = 0; i < 4; ++i) {
    printf("\nR[%lu]=%5.3e; dR[%lu]=%5.3e; Density[%lu]=%5.3e; Entropy[%lu]=%5.3e; T[%lu]=%5.3e\n",
           i,Data.R[i],
           i,Data.dR[i],
           i,Data.Density[i],
           i,Data.Entropy[i],
           i,Data.T[i]
          );
  }
  for (size_t i = Data.NumCells-4; i < Data.NumCells; ++i) {
    printf("\nR[%lu]=%5.3e; dR[%lu]=%5.3e; Density[%lu]=%5.3e; Entropy[%lu]=%5.3e; T[%lu]=%5.3e\n",
           i,Data.R[i],
           i,Data.dR[i],
           i,Data.Density[i],
           i,Data.Entropy[i],
           i,Data.T[i]
          );
  }
#endif

#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells; ++i) {
    if ((Data.R[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.R[i])) {
     printf("ERROR: %s: %i: R[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.R[i]);
      ErrorFlag=1;
    }
  }

#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.dR[i] <= NPP_MINABS_32F) ||
        !isfinite(Data.dR[i])) {
      printf("ERROR: %s: %i: dR[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.dR[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.Density[i] <= NPP_MINABS_32F) ||
        !isfinite(Data.Density[i])) {
      printf("ERROR: %s: %i: Density[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.Density[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.Entropy[i] <= NPP_MINABS_32F) ||
        !isfinite(Data.Entropy[i])) {
      printf("ERROR: %s: %i: Entropy[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.Entropy[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.T[i] <= NPP_MINABS_32F) ||
        !isfinite(Data.T[i])) {
      printf("ERROR: %s: %i: T[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.T[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.n_H[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.n_H[i])) {
      printf("ERROR: %s: %i: n_H[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.n_H[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.f_H1[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.f_H1[i])) {
      printf("ERROR: %s: %i: f_H1[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.f_H1[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.f_H2[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.f_H2[i])) {
      printf("ERROR: %s: %i: f_H2[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.f_H2[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.n_He[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.n_He[i])) {
      printf("ERROR: %s: %i: n_He[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.n_He[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.f_He1[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.f_He1[i])) {
      printf("ERROR: %s: %i: f_He1[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.f_He1[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.f_He2[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.f_He2[i])) {
      printf("ERROR: %s: %i: f_He2[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.f_He2[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.f_He3[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.f_He3[i])) {
      printf("ERROR: %s: %i: f_He3[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.f_He3[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.column_H1[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.column_H1[i])) {
      printf("ERROR: %s: %i: column_H1[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.column_H1[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.column_He1[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.column_He1[i])) {
      printf("ERROR: %s: %i: column_He1[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.column_He1[i]);
      ErrorFlag=1;
    }
  }
#pragma omp parallel for
  for (size_t i = 0; i < Data.NumCells;
       ++i) {
    if ((Data.column_He2[i] <= -NPP_MINABS_32F) ||
        !isfinite(Data.column_He2[i])) {
      printf("ERROR: %s: %i: column_He2[%lu] is out of range: %5.3e;\n",
             File, Line,
             i,
             Data.column_He2[i]);
      ErrorFlag=1;
    }
  }
  if(ErrorFlag) {
    return EXIT_FAILURE;
  } else {
    return EXIT_SUCCESS;
  }
}