#include <stdio.h>
#include <stdlib.h>

#include "RT_IntegralInvolatilesT.h"

int RT_IntegralInvolatiles_allocate(
    RT_ConstantsT const Constants,
    RT_IntegralInvolatilesT* const Involatiles) {
  /* Number of frequencies */
  size_t sum = 0;
  for (size_t i = 0; i < Constants.NLevels_NIntervals; ++i) {
    sum += Constants.NLevels[i];
  }
  Involatiles->nFrequencies = sum;

#if 0
 if(Constants.Y>0.0) {
  Involatiles->nSpecies=3;
 } else {
  Involatiles->nSpecies=1;
 }
#else
  /* Must currently be hard-wired at 3, even if there is no Helium */
  Involatiles->nSpecies = 3;
#endif

  size_t MemoryRequired = Involatiles->nFrequencies * sizeof(rt_float);
  Involatiles->Frequencies = malloc(MemoryRequired);
  Involatiles->Weights = malloc(MemoryRequired);
  Involatiles->Luminosities = malloc(MemoryRequired);
  Involatiles->Alphas = malloc(Involatiles->nSpecies * MemoryRequired);

  if ((Involatiles->Frequencies == NULL) || (Involatiles->Weights == NULL) ||
      (Involatiles->Luminosities == NULL) || (Involatiles->Alphas == NULL)) {
    printf(
        "ERROR: %s: %i: Unable to allocate memory for Integral Involatiles.\n",
        __FILE__,
        __LINE__);
    printf(" MemoryRequired=%lu; nFrequencies=%lu; nSpecies=%lu;\n",
           MemoryRequired,
           Involatiles->nFrequencies,
           Involatiles->nSpecies);
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
