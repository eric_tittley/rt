/* Interpolate from a table.
 *
 * ARGUMENTS
 *  xp  The point at which to interpolate a value
 *  N   The length of the table
 *  x   The independent value in the table (vector of length N)
 *  y   The dependent value in the table (vector of length N)
 *
 * RETURNS
 *  y interpolated at xp
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  04 05 10 First version.
 *  04 05 10 Bracket range test in a DEBUG block.
 *  06 11 15 Major Bug!  I've been using | when I meant ||.
 *  07 07 02 Renamed this function from RT_interp() to
 * RT_InterpolateFromTable().
 */
#include <stdio.h>

#include "RT.h"
#include "config.h"

/* Interpolate input spectrum from a table */
rt_float RT_InterpolateFromTable(rt_float const xp,
                                 size_t const N,
                                 rt_float const* const x,
                                 rt_float const* const y) {
  int i, i_lo = 0, i_hi;
  rt_float frac;

#ifdef DEBUG
  /* We don't want to extrapolate */
  if ((xp < x[0]) || (xp > x[N - 1])) {
    printf("ERROR: RT_InterpolateFromTable: xp=%e is out of range (%e to %e)\n",
           xp,
           x[0],
           x[N - 1]);
    (void)fflush(stdout);
    return -999.0;
  }
#endif

  /* Find the indices bracketing xp */
  /* First, try an inefficient algorithm */
  for (i = 1; i < N; i++) {
    if (xp <= x[i]) {
      i_lo = i - 1;
      break;
    }
  }
  i_hi = i_lo + 1;
  /* We will use an algorithm that is efficient if successive calls
   * have similar yp values. */
  /* NOT YET DONE */

  /* Now do a linear interpolation.  A logarithmic interpolation may be more
   * appropriate, but it will be substantially less efficient. */
  frac = (xp - x[i_lo]) / (x[i_hi] - x[i_lo]);
  return y[i_lo] + frac * (y[i_hi] - y[i_lo]);
}
