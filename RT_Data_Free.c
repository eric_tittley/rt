/* Free a RT_Data structure and associated memory, including refinements.
 *
 * ARGUMENTS
 *  Data      Pointer to the RT_Data structure to free
 *
 * RETURNS
 *  Nothing
 *
 * SEE ALSO
 *  RT_Data_Allocate, RT_Data_Assign
 *
 * AUTHOR: Eric Tittley
 */

#include <stdio.h>
#include <stdlib.h>

#include "config.h"

#if (VERBOSE >= 1) && (defined RT_MPI)
#  include <mpi.h>
#endif

#include "RT_Data.h"

void RT_Data_Free(/*@null@*/ RT_Data* Data) {
#ifdef REFINEMENTS
  int i;
#endif
#if VERBOSE >= 1
#  ifdef RT_MPI
  int me;
  MPI_Comm_rank(MPI_COMM_WORLD, &me);
#  else
  const int me = 0;
#  endif
#endif

  if (Data != NULL) {
#ifdef REFINEMENTS
    /* Free refinements.  This is a recursive call. */
    for (i = 0; i < Data->NumCells; i++) {
      RT_Data_Free(Data->Ref_pointer[i]);
      Data->Ref_pointer[i] = NULL;
    }
#endif
    /* Free memory space. */
#if VERBOSE >= 1
    printf("(%2i) RT_Data_Free: Freeing %lu bytes @ %p.\n",
           me,
           Data->NBytes,
           Data->MemSpace);
    (void)fflush(stdout);
#endif
    free(Data->MemSpace);
    Data->MemSpace = NULL;
    /* Free RT_Data structure */
#if VERBOSE >= 1
    printf("(%2i) RT_Data_Free: Freeing structure: %lu bytes @ %p.\n",
           me,
           sizeof(RT_Data),
           Data);
    (void)fflush(stdout);
#endif
    free(Data);
    /* The routine calling RT_Data_Free should set Data=NULL.  It would be
     * meaningless to do it here, since Data is a copy of the pointer which is
     * destroyed when this function terminates. */
#if VERBOSE >= 1
    printf("(%2i) RT_Data_Free: Done\n", me);
    (void)fflush(stdout);
#endif
  }
}
