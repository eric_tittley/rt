/* RT_Data_AllocateGridMemory: Allocate memory for a grid of RT data, where the
 * grid is a series of Lines of Sight with Ncells each.
 *
 * ARGUMENTS
 *  Input, not modified
 *   Nlos     Number of Lines Of Sight
 *   Ncells   Number of cells per Line Of Sight
 *
 * RETURNS
 *   Data     Address of a pointer to an array of pointers to RT_Data
 * structures.
 *
 * SEE ALSO
 *  RT_Data_Allocate
 *
 * AUTHOR: Eric Tittley
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "RT.h"
#include "RT_Data.h"
#include "config.h"

/*@null@*/ RT_Data **RT_Data_AllocateGridMemory(size_t const Nlos,
                                                size_t const Ncells) {
  int i, j;
#if VERBOSE > 0
  size_t AllocatedMemory = 0;
#endif

  RT_Data **Data;

#if VERBOSE >= 1
  printf("%s: Begin...\n", __FILE__);
  (void)fflush(stdout);

  printf("%s: sizeof(RT_Data)=%i\n", __FILE__, (int)sizeof(RT_Data));
  (void)fflush(stdout);

  AllocatedMemory += Nlos * sizeof(RT_Data);
#endif
  Data = (RT_Data **)malloc(Nlos * sizeof(RT_Data *));
  if (Data == NULL) {
    printf("ERROR: %s %i: Allocation of memory failed: Data\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return NULL;
  }
  for (i = 0; i < Nlos; i++) {
    Data[i] = RT_Data_Allocate(Ncells);
    if (Data[i] == NULL) {
      printf("ERROR: %s %i: Allocation of memory failed: Data[%i]\n",
             __FILE__,
             __LINE__,
             i);
      (void)fflush(stdout);
      free(Data);
      return NULL;
    }
  }

  /* Initialize the refinement cell index, number of cells per line of sight,
   * and refinement pointers */
  for (j = 0; j < Nlos; j++) {
    Data[j]->Cell = 0;
    Data[j]->NumCells = Ncells;
#ifdef REFINEMENTS
    for (i = 0; i < Ncells; i++) {
      Data[j]->Ref_pointer[i] = NULL;
    }
#endif
  }

#if VERBOSE > 0
  printf("%s: Allocated %i bytes = %9.2f kb = %6.2f Mb\n",
         __FILE__,
         (int)AllocatedMemory,
         (float)AllocatedMemory / 1024.0,
         (float)AllocatedMemory / 1048576.0);
  (void)fflush(stdout);

  printf("%s: Done.\n", __FILE__);
  (void)fflush(stdout);
  (void)fflush(stdout);
#endif

  return Data;
}
