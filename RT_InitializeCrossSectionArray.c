/* RT_InitializeCrossSectionArray: Initialize (preferably only once) the
 *        absorption cross sections array for HI, HeI, and HeII at the
 *        integration frequencies.
 *
 * ierr=RT_InitializeCrossSectionArray(&Constants);
 *
 * ARGUMENTS
 *  Input, not modified
 *
 *  Output
 *   Alpha[Number of intervals==3][Number of species==3][N_Levels_*]
 *   On input, Alpha[0], Alpha[1], and Alpha[2] are unset pointers to rt_float**
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * EXTERNAL MACROS AND VARIABLES
 *  HAVE_CONFIG
 *  DEBUG
 *
 * CALLS
 *
 * AUTHOR: Eric Tittley
 */

/* A note about the "Alpha" variable.
 *
 * The cross-sections (Alphas) are expensive to calclulate but are used
 * repetatively in the integration over nu at each of the sample frequencies,
 * nu_i.
 *
 * Since the cross-sections are only a function of frequency and, for a given
 * problem, the sample points, nu_i, are consistent from iteration to
 * iteration, it makes sense to calculate the cross-sections only once and
 * store the values for reuse. Unfortunately, to keep the integration as
 * general as possible (permitting, for example, easy swapping of integration
 * methods), a certain amount of clunky bookkeeping is required.
 *
 * Alpha[] contains pointers, one for each integration range.
 * Each Alpha[i] points to a 2D pointer which contains the three
 * cross-sections (H1 to H2, He1 to He2, He2 to He3), at each of the
 * frequencies the integration method in the integration range intends to use.
 *
 * Since we can't know, a priori, what each of the methods will choose for
 * the sample frequencies, let the methods calculate the values on the first
 * pass, signified to them by NULL for the values of the Alpha[i] pointers.
 *
 * Since the integration methods are allocating the memory and setting the
 * pointers, the pointers must be sent to the methods by address, which is
 * always odd-looking.
 *
 * Since Alpha is static, that memory will be retained between calls.
 *
 * Note that nowhere is the memory free()'d, but it shouldn't grow.
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>

#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Luminosity.h"
#include "RT_SourceT.h"

int RT_InitializeCrossSectionArray(RT_ConstantsT *Constants) {
  size_t i;
  int ierr;

  /* The following are all just fillers and won't affect the result, which is to
   * populate Alpha */
  rt_float N_H1 = 1.0;
  rt_float N_He1 = 1.0;
  rt_float N_He2 = 1.0;
  rt_float N_H1_cum = 0.0;
  rt_float N_He1_cum = 0.0;
  rt_float N_He2_cum = 0.0;
  rt_float z = 1;
  rt_float sum[6];
  RT_SourceT Source;

  Source.LuminosityFunction =
      (SourceFunctionT)&RT_Luminosity_POWER_LAW_MINUS_ONE_HALF;
  Source.z_on = 1.0;
  Source.z_off = 1.0;
  Source.L_0 = 1.0;

  /* Sanity check */
  if (Constants->NLevels_NIntervals != 3) {
    printf(
        "ERROR: %s: %i: Expecting there to be only 3 integration intervals.\n",
        __FILE__,
        __LINE__);
    printf(" Was told there are %lu\n", Constants->NLevels_NIntervals);
    return EXIT_FAILURE;
  }

  /* Allocate memory space */
  Constants->Alpha =
      malloc(Constants->NLevels_NIntervals * sizeof(rt_float **));

  /* Set the pointers to the rt_float** to NULL to trigger Gauss_L*() to
   * initialize A */
  for (i = 0; i < Constants->NLevels_NIntervals; ++i) {
    Constants->Alpha[i] = NULL;
  }

  ierr = Gauss_Legendre(Constants->nu_0,
                        Constants->nu_1,
                        N_H1,
                        N_He1,
                        N_He2,
                        N_H1_cum,
                        N_He1_cum,
                        N_He2_cum,
                        z,
                        Source,
                        Constants,
                        0,
                        sum);
#ifdef DEBUG
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: Gauss_Legendre failed.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  ierr = Gauss_Legendre(Constants->nu_1,
                        Constants->nu_2,
                        N_H1,
                        N_He1,
                        N_He2,
                        N_H1_cum,
                        N_He1_cum,
                        N_He2_cum,
                        z,
                        Source,
                        Constants,
                        1,
                        sum);
#ifdef DEBUG
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: Gauss_Legendre failed.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  ierr = Gauss_Laguerre(Constants->nu_2,
                        0.0, /* Unused */
                        N_H1,
                        N_He1,
                        N_He2,
                        N_H1_cum,
                        N_He1_cum,
                        N_He2_cum,
                        z,
                        Source,
                        Constants,
                        2,
                        sum);
#ifdef DEBUG
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: Gauss_Laguerre failed.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  return EXIT_SUCCESS;
}
