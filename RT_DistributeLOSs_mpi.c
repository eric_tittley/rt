/* Farm the LOSs out to different processes.  RT_ProcessLOS() does the RT on
 * each LOS.
 *
 * ARGUMENTS
 *  Input, not modified
 *   me         Node number of this instance of the programme
 *   Master     The node ID for the process farming out LOSs
 *   Nlos       The number of LOS's to farm out. Only master needs this.
 *   nproc      Number of processors
 *   t          Simulation time (s)
 *   dt         Timestep (s)
 *   aa         Expansion factor at the *end* of the iteration.
 *   Source     The source parameters
 *   Constants  Physical constants
 *  Input, modified
 *   Data       The state of the gas in all cells
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE If unable to write output file or RT_ProcessLOS fails.
 *
 * AUTHOR: Eric Tittley
 */

#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_SourceT.h"
#include "config.h"

#define FREE_MEMORY       \
  RT_Data_Free(Data_los); \
  free(Data_los);         \
  Data_los = NULL;

#ifdef DEBUG
int CheckEntropy_RTDL(RT_Data *Data, int me, int Line) {
  int i;
  if (Data != NULL) {
    for (i = 0; i < Data->NumCells; i++) {
      if (Data->Entropy[i] <= 0 || !(bool)isfinite(Data->Entropy[i])) {
        printf("(%2i) ERROR: RT_DistributeLOSs %i: Entropy[%3i]=%5.3e\n",
               me,
               Line,
               i,
               Data->Entropy[i]);
        printf(" NumCells=%i\n", Data->NumCells);
        (void)fflush(stdout);
        return EXIT_FAILURE;
      }
    }
  }
  return EXIT_SUCCESS;
}
#endif

int RT_DistributeLOSs(int const me,
                      int const Master,
                      size_t const Nlos,
                      size_t const nproc,
                      rt_float const t,
                      rt_float const dt,
                      rt_float const aa,
                      RT_SourceT const Source,
                      RT_ConstantsT const Constants,
                      /*@null@*/ RT_Data **const Data) {
  int const TAG = 10; /* This can be any integer? */
  int proc, los_done, los = -2, los_tmp, ierr;
  MPI_Status status;

  RT_Data *Data_los = NULL;

#ifdef DEBUG
  if (dt < 0.) {
    printf("ERROR: %s: dt < 0: dt=%f\n", __FILE__, dt);
    (void)fflush(stdout);
    FREE_MEMORY;
    return EXIT_FAILURE;
  }
#endif

  if (me == Master) {
    proc = 0;
    los_done = 0;
    for (los = 0; los < Nlos; los++) {
#if VERBOSE >= 1
      printf("(%2i) %s: los=%i of %i\n", me, __FILE__, los, Nlos);
      (void)fflush(stdout);
#endif
      if (los < (nproc - 1)) {  /* Processes are waiting for something to do. */
        if (proc == me) proc++; /* Don't send anything to myself. */
        (void)MPI_Ssend(&los, 1, MPI_INT, proc, TAG, MPI_COMM_WORLD);
        ierr =
            RT_Data_MPI_Send(Data[los], proc); /* This also frees the memory */
#ifdef DEBUG
        if (ierr == EXIT_FAILURE) {
          printf("(%2i) ERROR: %s: %i: Failed to send los=%i to %i\n",
                 me,
                 __FILE__,
                 __LINE__,
                 los,
                 proc);
          (void)fflush(stdout);
          FREE_MEMORY;
          return EXIT_FAILURE;
        }
#endif
        Data[los] = NULL;
        proc++;
        /* } else if (los == nproc - 1) { Send a process to myself } */
      } else { /* Wait until someone is done.  Then assign a new LOS */
        MPI_Recv(
            &los_tmp, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
#if VERBOSE >= 1
        printf("(%2i) %s: Receiving los=%i from %i\n",
               me,
               __FILE__,
               los_tmp,
               status.MPI_SOURCE);
        (void)fflush(stdout);
#endif
        Data[los_tmp] = RT_Data_MPI_Recv(status.MPI_SOURCE);
        if (Data[los_tmp] == NULL) {
          printf(
              "(%2i) ERROR: %s: %i: Failed to receive data for LOS %i from "
              "%i.\n",
              me,
              __FILE__,
              __LINE__,
              los_tmp,
              status.MPI_SOURCE);
          (void)fflush(stdout);
          FREE_MEMORY;
          return EXIT_FAILURE;
        }
#if VERBOSE >= 1
        printf("(%2i) %s: Done receiving los=%i from %i\n",
               me,
               __FILE__,
               los_tmp,
               status.MPI_SOURCE);
        (void)fflush(stdout);
#endif
        los_done++;
        /* status.MPI_SOURCE contains the processor number which just returned
         * data. Give it a new LOS. */
#if VERBOSE >= 1
        printf("(%2i) %s: Sending los=%i to %i\n",
               me,
               __FILE__,
               los,
               status.MPI_SOURCE);
        (void)fflush(stdout);
#endif
        (void)MPI_Ssend(
            &los, 1, MPI_INT, status.MPI_SOURCE, TAG, MPI_COMM_WORLD);
        ierr = RT_Data_MPI_Send(Data[los], status.MPI_SOURCE);
        Data[los] = NULL;
#ifdef DEBUG
        if (ierr == EXIT_FAILURE) {
          printf("(%2i) ERROR: %s: %i: Failed to send los=%i to %i\n",
                 me,
                 __FILE__,
                 __LINE__,
                 los,
                 status.MPI_SOURCE);
          (void)fflush(stdout);
          FREE_MEMORY;
          return EXIT_FAILURE;
        }
#endif
        Data[los] = NULL;
      } /* end if los < nproc-1 else */
    }   /* end loop over all los.  Now all los have
         * been farmed out. */
    /* Collect all the outstanding jobs left. */
    while (los_done < Nlos) {
#if VERBOSE >= 1
      printf("(%2i) %s: los_done=%i of %i\n", me, __FILE__, los_done, Nlos);
      (void)fflush(stdout);
#endif
      MPI_Recv(
          &los_tmp, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, &status);
#if VERBOSE >= 1
      printf("(%2i) %s: Receiving los=%i from %i\n",
             me,
             __FILE__,
             los_tmp,
             status.MPI_SOURCE);
      (void)fflush(stdout);
#endif
      Data[los_tmp] = RT_Data_MPI_Recv(status.MPI_SOURCE);
      if (Data[los_tmp] == NULL) {
        printf(
            "(%2i) ERROR: %s: %i: Failed to receive data for LOS %i from %i.\n",
            me,
            __FILE__,
            __LINE__,
            los_tmp,
            status.MPI_SOURCE);
        (void)fflush(stdout);
        FREE_MEMORY;
        return EXIT_FAILURE;
      }
#if VERBOSE >= 1
      printf("(%2i) %s: Done receiving los=%i from %i\n",
             me,
             __FILE__,
             los_tmp,
             status.MPI_SOURCE);
      (void)fflush(stdout);
#endif
      los_done++;
    } /* end final collection while loop.
       * I (Master) have collected all the results. */
    /* ******* DONE WITH THE SLAVE PROCESSES. ******** */
    /* Tell all the processes we are done (los=-1). */
    los = -1;
    for (proc = 0; proc < nproc; proc++) {
      if (proc != me) {
        MPI_Ssend(&los, 1, MPI_INT, proc, TAG, MPI_COMM_WORLD);
      }
    }
  } else { /* I'M A SLAVE */
    do {
#if VERBOSE >= 1
      printf("(%2i) %s: Got to slave\n", me, __FILE__);
      (void)fflush(stdout);
#endif
      (void)MPI_Recv(&los, 1, MPI_INT, Master, TAG, MPI_COMM_WORLD, &status);
#if VERBOSE >= 1
      printf("(%2i) %s: los=%i\n", me, __FILE__, los);
      (void)fflush(stdout);
#endif
      if (los == -1) {
#if VERBOSE >= 1
        printf("(%2i) %s: Finished with RT. Moving on.\n", me, __FILE__);
        (void)fflush(stdout);
#endif
      } else {
#ifdef DEBUG
        if (los == -2) {
          printf("(%2i) %s: %i: Impossible State: los == -2",
                 me,
                 __FILE__,
                 __LINE__);
          (void)fflush(stdout);
          FREE_MEMORY;
          return EXIT_FAILURE;
        }
#endif
        Data_los = RT_Data_MPI_Recv(Master);
        if (Data_los == NULL) {
          printf("(%2i) ERROR: %s: %i: Failed to receive data for LOS.\n",
                 me,
                 __FILE__,
                 __LINE__);
          (void)fflush(stdout);
          FREE_MEMORY;
          return EXIT_FAILURE;
        }
#if VERBOSE >= 1
        printf("(%2i) %s: Received Data_los for los=%i\n", me, __FILE__, los);
        (void)fflush(stdout);
#endif

        /* Main Loop for a 1D line of sight */
        /* RT_ProcessLOS modifies contents of the structure Data_los */
#if VERBOSE >= 1
        printf(
            "(%2i) %s: Calling RT_ProcessLOS for los=%i.\n", me, __FILE__, los);
        (void)fflush(stdout);
#endif
        /* aa is the expansion factor at the end of the iteration.
         * Strictly, aa should grow during the integration in RT_ProcessLOS. */
        ierr = RT_ProcessLOS(t, dt, aa, Source, Constants, me, Data_los);
#ifdef DEBUG
        if (ierr == EXIT_FAILURE) {
          printf(
              "(%2i) ERROR: %s: %i: RT_ProcessLOS failed. los=%i; Master=%i\n",
              me,
              __FILE__,
              __LINE__,
              los,
              Master);
          (void)fflush(stdout);
          FREE_MEMORY;
          return EXIT_FAILURE;
        }
#endif

        /* Return the data structure to the Master. */
#if VERBOSE >= 1
        printf("(%2i) %s: Sending to Master\n", me, __FILE__);
        (void)fflush(stdout);
#endif
        (void)MPI_Ssend(&los, 1, MPI_INT, Master, TAG, MPI_COMM_WORLD);
        ierr = RT_Data_MPI_Send(Data_los, Master);
        Data_los = NULL;
#ifdef DEBUG
        if (ierr == EXIT_FAILURE) {
          printf("(%2i) ERROR: %s: %i: Failed to send los=%i to %i\n",
                 me,
                 __FILE__,
                 __LINE__,
                 los,
                 Master);
          (void)fflush(stdout);
          FREE_MEMORY;
          return EXIT_FAILURE;
        }
#endif
        Data_los = NULL;
      } /* If los == -1 ... else ... endif */
    } while (los != -1);
  }

#if VERBOSE >= 1
  printf("(%2i) %s: Leaving.\n", me, __FILE__);
  (void)fflush(stdout);
#endif

  FREE_MEMORY;
  return EXIT_SUCCESS;
}
