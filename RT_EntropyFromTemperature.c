#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math.h>

#include "RT.h"

rt_float RT_EntropyFromTemperature(RT_ConstantsT const Constants,
                                   rt_float const Temperature,
                                   size_t const iCell,
                                   RT_Data const Data) {
  /* Update the neutral & ionised number densities. */
  rt_float const n_H2 = Data.n_H[iCell] * Data.f_H2[iCell];
  rt_float const n_He2 = Data.n_He[iCell] * Data.f_He2[iCell];
  rt_float const n_He3 = Data.n_He[iCell] * Data.f_He3[iCell];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2. * n_He3);

  /* Total number density */
  rt_float const n = Data.n_H[iCell] + Data.n_He[iCell] + n_e;

  rt_float const mu = (Data.n_H[iCell] + 4. * Data.n_He[iCell]) / n;
#ifdef DEBUG
  if (!(bool)isfinite(mu)) {
    printf("ERROR: %s: %i: mu=%5.3e; n_H=%5.3e; n_He=%5.3e; n=%5.3e\n",
           __FILE__,
           __LINE__,
           mu,
           Data.n_H[iCell],
           Data.n_He[iCell],
           n);
    (void)fflush(stdout);
    return -1.0;
  }
#endif

  /* Numeric bounds check:
   *  10^( (-23 + 4 - -27) - ( 0 + 2/3 * (-28) )
   *  10^( +8  - ( -18 ) )
   *  10^26
   */
  /* GCC 7.3 --ffast-math has been observed to break the order of operations
   * on the following. */
  rt_float const Entropy =
      (Constants.k_B * Temperature / Constants.mass_p) /
      (mu * POW(Data.Density[iCell], Constants.gamma_ad - 1.));

#ifdef DEBUG
  if (!(bool)isfinite(Entropy)) {
    printf(
        "ERROR: %s: %i: Entropy=%5.3e; Temperature=%5.3e; mu=%5.3e; "
        "Density=%5.3e\n",
        __FILE__,
        __LINE__,
        Entropy,
        Temperature,
        mu,
        Data.Density[iCell]);
    (void)fflush(stdout);
    return -1.0;
  }
#endif

  return Entropy;
}
