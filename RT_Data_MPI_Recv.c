/* Receive via MPI a RT_Data structure and associated memory, including
 * refinements.
 *
 * ARGUMENTS
 *  orig   The node from which to receiving.
 *
 * RETURNS
 *  A pointer to RT_Data structure containing the received data.
 *
 * SEE ALSO
 *  RT_Data_Send
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  06 04 11 First version.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "RT_Data.h"
#include "config.h"

/*@null@*/ RT_Data* RT_Data_MPI_Recv(int const orig) {
  int const TAG = 10; /* This can be any integer? */
  int NumCells, Cell, NBytes;
  MPI_Status status;
  RT_Data* Data;
#ifdef REFINEMENTS
  int i;
#endif
#if (VERBOSE >= 1) | (defined DEBUG)
  int me;
  (void)MPI_Comm_rank(MPI_COMM_WORLD, &me);
#endif

  /* Receive the details */
  (void)MPI_Recv(&NumCells, 1, MPI_INT, orig, TAG, MPI_COMM_WORLD, &status);
  (void)MPI_Recv(&Cell, 1, MPI_INT, orig, TAG, MPI_COMM_WORLD, &status);
  (void)MPI_Recv(&NBytes, 1, MPI_INT, orig, TAG, MPI_COMM_WORLD, &status);

#if (defined DEBUG) && 0
  if (NumCells >= 4096) { /* MAX_NSLICES in RT_SplitCell */
    printf("(%2i) ERROR: RT_Data_MPI_Recv: %i: NumCells=%i > MAX_NSLICES.\n",
           me,
           __LINE__,
           NumCells);
    (void)fflush(stdout);
    return NULL;
  }
#endif

  /* Allocate the memory space */
  Data = RT_Data_Allocate(NumCells);
#ifdef DEBUG
  if (Data == NULL) {
    printf(
        "(%2i) ERROR: RT_Data_MPI_Recv: %i: Unable to allocate memory for %i "
        "cells.\n",
        me,
        __LINE__ - 4,
        NumCells);
    (void)fflush(stdout);
    return NULL;
  }
#endif
  Data->Cell = Cell;
  Data->NBytes = NBytes;

  /* Receive the memory space */
  (void)MPI_Recv(Data->MemSpace,
                 Data->NBytes,
                 MPI_BYTE,
                 orig,
                 TAG,
                 MPI_COMM_WORLD,
                 &status);

#ifdef REFINEMENTS
  /* Loop through all cells.  If the refinement pointer is not NULL, then it
     should contain a pointer to a refinement and the sender will be
     transmitting it next. */
  for (i = 0; i < Data->NumCells; i++) {
    if (Data->Ref_pointer[i]) {
#  if VERBOSE >= 1
      printf(
          "(%2i) RT_Data_MPI_Recv: Receiving refinement for cell %i from %i\n",
          me,
          i,
          orig);
      (void)fflush(stdout);
#  endif
      /* The pointer in Data->Ref_pointer[i] is for the memory space of the
       * Sending machine.  We need to replace it with the address of where we
       * are going to store it, here. */
      Data->Ref_pointer[i] = RT_Data_MPI_Recv(orig);
#  ifdef DEBUG
      if (Data->Ref_pointer[i] == NULL) {
        printf(
            "(%2i) ERROR: RT_Data_MPI_Recv: %i: Failed to receive refinement "
            "for cell %i via MPI.\n",
            me,
            __LINE__ - 4,
            i);
        (void)fflush(stdout);
        return NULL;
      }
#  endif
    }
  }
#endif

  return Data;
}
