#include <stdio.h>
#include <stdlib.h>

#include "RT.h"

void RT_FindNextTimestep(size_t const nParticles,
                         rt_float const t_stop,
                         rt_float const t,
                         RT_Data const Data_dev,
                         RT_RatesBlockT const Rates_dev,
                         RT_ConstantsT const Constants,
                         /* Output */
                         rt_float *dt_RT,
                         unsigned char *LastIterationFlag) {
  /* Reset the timesteps */
  RT_TimeScale dt_RT_sugg;
  RT_TimeScale_SetInfiniteTime(&dt_RT_sugg);

  /* Only "Entropy" is used, and it fills in for all timescales searched in
   * fMT() */
  dt_RT_sugg.Entropy = RT_FindMinimumTimescale(Constants, Rates_dev, Data_dev);

  printf("dt_RT_sugg=%5.3e\n", dt_RT_sugg.Entropy);

  /* The appropriate time step */
  RT_NextTimeStep(t_stop, t, Constants, &dt_RT_sugg, dt_RT, LastIterationFlag);

  if (!*LastIterationFlag) {
    if (*dt_RT < Constants.MIN_DT) *dt_RT = Constants.MIN_DT;
    if (*dt_RT > Constants.MAX_DT) *dt_RT = Constants.MAX_DT;
  }
  return;
}
