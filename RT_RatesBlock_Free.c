#include <stdio.h>
#include <stdlib.h>

#include "RT_RatesT.h"

void RT_RatesBlock_Free(RT_RatesBlockT Rates) {
  free(Rates.I_H1);
  free(Rates.I_He1);
  free(Rates.I_He2);
  free(Rates.G);
  free(Rates.G_H1);
  free(Rates.G_He1);
  free(Rates.G_He2);
  free(Rates.alpha_H1);
  free(Rates.alpha_He1);
  free(Rates.alpha_He2);
  free(Rates.L);
  free(Rates.Gamma_HI);
  free(Rates.n_HI_Equilibrium);
  free(Rates.n_HII_Equilibrium);
  free(Rates.TimeScale);
}
