/* PREPROCESSOR MACROS
 *  DEBUG
 *  ADAPTIVE_TIMESCALE (default on)
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define ADAPTIVE_TIMESCALE
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_RatesT.h"
#include "RT_SourceT.h"
#include "RT_TimeScale.h"

int RT_ProcessCell(unsigned char const LastIterationFlag,
                   rt_float const ExpansionFactor,
                   rt_float const tCurrent,
                   rt_float const dt,
                   rt_float const dt_RT,
                   RT_SourceT const Source,
                   RT_ConstantsT const Constants,
                   RT_Cell* const Data_cell,
                   RT_TimeScale* const dt_RT_sugg) {
  int ierr;

  RT_RatesT Rates;

  /* Calculate all rates in the cell */
  ierr =
      RT_Rates(ExpansionFactor, tCurrent, Source, Constants, Data_cell, &Rates);
#ifdef DEBUG
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: RT_Rates failed.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

#ifdef ADAPTIVE_TIMESCALE
  /* Find the minimum timescales */
  ierr = RT_AdaptiveTimescale(dt, Constants, &Rates, Data_cell, dt_RT_sugg);
#  ifdef DEBUG
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: RT_AdaptiveTimescale failed.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#  endif
#endif /* ADAPTIVE_TIMESCALE */

  /* Update Temperature & Ionization states for the cell for this timestep. */
  ierr = RT_UpdateCell(dt_RT, ExpansionFactor, Constants, &Rates, Data_cell);
#ifdef DEBUG
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: RT_UpdateCell failed.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  if (LastIterationFlag) { /* Only during last sub-timestep */
    ierr = RT_LastIterationUpdate(Constants, &Rates, Data_cell);
#ifdef DEBUG
    if (ierr == EXIT_FAILURE) {
      printf("ERROR: %s: %i: RT_LastIterationUpdate failed.\n",
             __FILE__,
             __LINE__);
      (void)fflush(stdout);
      return EXIT_FAILURE;
    }
#endif
  }

  return EXIT_SUCCESS;
}
