/* PREPROCESSOR MACROS
 *  VERBOSE (default 0)
 *  DEBUG
 *  USE_MCTfR
 */

/* AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define VERBOSE 0
#endif

/* Block the entire function */
#ifdef REFINEMENTS

#  include <stdlib.h>
#  if (defined DEBUG) || (VERBOSE > 0)
#    include <stdio.h>
#  endif
#  ifdef DEBUG
#    include <math.h>
#  endif

#  include "RT.h"
#  include "RT_ConstantsT.h"
#  include "RT_Data.h"
#  include "RT_SourceT.h"

int RT_RefineCell(size_t const iz,
                  rt_float const dt,
                  rt_float const dt_RT,
                  rt_float const CurrentTime,
                  rt_float const ExpansionFactor,
                  RT_SourceT const Source,
                  RT_ConstantsT const Constants,
                  RT_Data *const Data_los) {
  int ierr;

  /* Find the neutral & ionised number densities. */
  rt_float n_H1 = Data_los->n_H[iz] * Data_los->f_H1[iz];
#  if VERBOSE >= 1
#    ifdef USE_MCTfR
  /* Cumulative optical depth at Lyman edge up to the lower edge of the cell */
  rt_float const tau_H1 = Constants.sigma_0 * Data_los->column_H1[iz];
#    endif
#  endif
  /* The optical depth in the cell */
  rt_float tau_H1_cell = Constants.sigma_0 * n_H1 * Data_los->dR[iz];

  RT_Data *Data_cell;

  /* If the cell is not already split, split it */
  if (Data_los->Ref_pointer[iz] == NULL) {
#  if VERBOSE >= 1
    printf("%s: Splitting cell %lu\n", __FILE__, iz);
    (void)fflush(stdout);
#  endif
    Data_cell = RT_SplitCell(Data_los, iz, tau_H1_cell, Constants);
    /* RT_SplitCell returns NULL if malloc failed. */
#  ifdef DEBUG
    if (Data_cell == NULL) {
      /* Find another way to treat this */
      printf("ERROR: %s: %i: Unable to split cell.\n", __FILE__, __LINE__);
      (void)fflush(stdout);
      return EXIT_FAILURE;
    }
#  endif
    /* Remember the memory address of this refinement */
    Data_los->Ref_pointer[iz] = Data_cell;
  } else { /* The cell was already split */
#  if VERBOSE >= 1
    printf("%s: Cell %lu previously split.\n", __FILE__, iz);
    (void)fflush(stdout);
#  endif
    /* Use the previously stored memory address of this refinement */
    Data_cell = Data_los->Ref_pointer[iz];
#  if VERBOSE >= 1
#    ifdef USE_MCTfR
    if ((tau_H1_cell <= Constants.Max_tau_in_cell) ||
        (tau_H1 >= Constants.Max_cumulative_tau_for_refinement)) {
#    else
    if (tau_H1_cell <= Constants.Max_tau_in_cell) {
#    endif
      printf(
          "WARNING: %s: I would not have normally created a refinement for "
          "cell iz=%lu\n",
          __FILE__,
          iz);
      printf("             tau_H1_cell = %f\n", tau_H1_cell);
      (void)fflush(stdout);
    }
#  endif
    /* Update the previous refinement for this cell */

    /* Update the cumulative column densities. */
    Data_cell->NCol[0] = Data_los->column_H1[iz];
    Data_cell->NCol[1] = Data_los->column_He1[iz];
    Data_cell->NCol[2] = Data_los->column_He2[iz];
    Data_cell->NCol[3] = Data_cell->NCol[0];
    Data_cell->NCol[4] = Data_cell->NCol[1];
    Data_cell->NCol[5] = Data_cell->NCol[2];

    /* Update dR.  Only necessary if ExpansionFactor has changed. */
    rt_float dR_ref = Data_cell->R[1] - Data_cell->R[0];
    rt_float const dR_ref_tot = dR_ref * Data_cell->NumCells;
    if (dR_ref_tot <= Data_los->dR[iz] * Constants.OneMinusEps) {
      dR_ref = Data_los->dR[iz] / (rt_float)Data_cell->NumCells;
#  ifdef DEBUG
      if (dR_ref < 0) {
        printf("ERROR: %s: %i: dR_ref=%6.3e; dR=%6.3e; NumCells=%lu\n",
               __FILE__,
               __LINE__,
               dR_ref,
               Data_los->dR[iz],
               Data_cell->NumCells);
        (void)fflush(stdout);
        RT_Data_Free(Data_cell);
        free(Data_cell);
        Data_cell = NULL;
        return EXIT_FAILURE;
      }
#  endif
      size_t i;
      for (i = 0; i < Data_cell->NumCells; i++) {
        Data_cell->R[i] = Data_los->R[iz] + (rt_float)i * dR_ref;
      }
      for (i = 0; i < Data_cell->NumCells; i++) {
        Data_cell->dR[i] = dR_ref;
      }
#  ifdef DEBUG
      if (Data_cell->R[0] <= 0.) {
        printf(
            "ERROR: %s: %i: Data_cell.R[0]=%6.3e; Data_los.R[%lu]=%6.3e; "
            "Data_los.R[%lu]=%6.3e\n",
            __FILE__,
            __LINE__,
            Data_cell->R[0],
            iz,
            Data_los->R[iz],
            iz + 1,
            Data_los->R[iz + 1]);
        (void)fflush(stdout);
        RT_Data_Free(Data_cell);
        free(Data_cell);
        Data_cell = NULL;
        return EXIT_FAILURE;
      }
#  endif
    }

    /* Set Data_cell->Density */
    ierr = RT_SetRefinementDensity(Data_cell, Data_los, iz);
#  ifdef DEBUG
    if (ierr != EXIT_SUCCESS) {
      printf("ERROR: %s: %i: RT_SetRefinementDensity failed for cell %lu",
             __FILE__,
             __LINE__,
             iz);
      (void)fflush(stdout);
      RT_Data_Free(Data_cell);
      free(Data_cell);
      Data_cell = NULL;
      return EXIT_FAILURE;
    }
#  endif

  } /* if the cell was not already split, else ... */
#  if VERBOSE >= 2
  printf("%s: iz=%lu; Data_cell->NumCells=%lu\n",
         __FILE__,
         iz,
         Data_cell->NumCells);
  (void)fflush(stdout);
#  endif

  /* *** Call RT_ProcessLOS recursively *** */
#  if VERBOSE >= 1
  printf("%s: Calling RT_ProcessLOS recursively for cell %lu using buffer %p\n",
         __FILE__,
         iz,
         Data_los->Ref_pointer[iz]);
  printf("       Using dt_RT=%e\n", dt_RT);
  (void)fflush(stdout);
#  endif

  ierr = RT_ProcessLOS(
      CurrentTime, dt_RT, ExpansionFactor, Source, Constants, Data_cell);
#  ifdef DEBUG
  if (ierr != EXIT_SUCCESS) {
    printf(
        "ERROR: %s: %i: Recursive call to RT_ProcessLOS returned in error.\n",
        __FILE__,
        __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#  endif

  /* Merge the contents of the refinement, but do not destroy the
   * refinement. */
#  if VERBOSE >= 1
  printf("%s: Merging refinement\n", __FILE__);
  (void)fflush(stdout);
#  endif
  RT_MergeCell(Data_cell, iz, Data_los);

  /* If the optical depth is below a threshold, delete refinement */
  /* Find the optical depth in the cell */
  n_H1 = Data_los->n_H[iz] * Data_los->f_H1[iz];
  tau_H1_cell = Constants.sigma_0 * n_H1 * Data_los->dR[iz];
  if (tau_H1_cell < Constants.Max_tau_in_cell) {
    /* Free up the refinement buffer */
#  if VERBOSE >= 1
    printf("%s: Deleting refinement for cell %lu\n", __FILE__, iz);
    (void)fflush(stdout);
#  endif
    free(Data_cell);
    Data_los->Ref_pointer[iz] = NULL;
  }
  return EXIT_SUCCESS;
}

#endif /* ifdef REFINEMENTS */
