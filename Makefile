ifndef MAKE_CONFIG
 MAKE_CONFIG = ../make.config
endif
include $(MAKE_CONFIG)
# Expected definitions:
#  DEFS
#  INCS_BASE, INC_MPI
#  SERIAL, MPI, and OPENMP (1 or 0)
#  AR, AR_FLAGS
#  LD, LD_FLAGS
#  DEPEND, DEPEND_FLAGS

INCS = $(INCS_BASE) $(INC_MPI) $(INC_SUPPORT)

#       Gauss_Laguerre.o \
#       Gauss_Legendre.o \
#       RT_PhotoionizationRatesIntegrate.o \
#       RT_Rates.o \

OBJS = \
       cosmological_time.o \
       CrossSections.o \
       expansion_factor_from_time.o \
       GL_weights.o \
       isequal.o \
       PhotonFlux.o \
       RecombCoef.o \
       RT_AdaptiveTimescale.o \
       RT_CalculateRecombinationRatesInclCooling.o \
       RT_Cell_Copy.o \
       RT_Cell_Pack.o \
       RT_Cell_Unpack.o \
       RT_CoolingRatesExtra.o \
       RT_CoolingRatesExtra_Cell.o \
       RT_Data_Allocate.o \
       RT_Data_AllocateGridMemory.o \
       RT_Data_Assign.o \
       RT_Data_Free.o \
       RT_Data_Read.o \
       RT_Data_Write.o \
       RT_Data_CheckData.o \
       RT_EntropyFromTemperature.o \
       RT_FindMinimumTimescale.o \
       RT_FindNextTimestep.o \
       RT_InitialiseSpectrum.o \
       RT_InitializeCrossSectionArray.o \
       RT_IntegralInvolatiles_allocate.o \
       RT_IntegralInvolatiles_free.o \
       RT_IntegralInvolatiles_setFrequenciesWeightsAlphas.o \
       RT_IntegralInvolatiles_setLuminosity.o \
       RT_InterpolateFromGrid.o \
       RT_InterpolateFromTable.o \
       RT_LastIterationUpdate.o \
       RT_LightFrontPosition.o \
       RT_LoadData.o \
       RT_LoadSourceTable.o \
       RT_Luminosity.o \
       RT_MergeCell.o \
       RT_NextTimeStep.o \
       RT_PhotoionizationRates.o \
       RT_PhotoionizationRatesFunctions.o \
       RT_PhotoionizationRatesIntegrate_I.o \
       RT_PhotoionizationRatesMonochromatic.o \
       RT_ProcessCell.o \
       RT_ProcessLOS.o \
       RT_ProcessUnrefinedCells.o \
       RT_Rates_Write.o \
       RT_Rates_ZeroRates.o \
       RT_RatesBlock_Allocate.o \
       RT_RatesBlock_Free.o \
       RT_ReadConstantsFile.o \
       RT_RecombinationCoefficients.o \
       RT_RecombinationRatesInclCooling.o \
       RT_RefineCell.o \
       RT_SaveData.o \
       RT_SetHydrogenFractionEquilibriumValues.o \
       RT_SetRefinementDensity.o \
       RT_SetSourceFunction.o \
       RT_SplitCell.o \
       RT_TemperatureFromEntropy.o \
       RT_TimeScale_MergeTimescales.o \
       RT_TimeScale_SetInfiniteTime.o \
       RT_UpdateCell.o \
       RT_UpdateCell_RateBlock.o \
       RT_UpdateColumnDensitiesAlongLOS.o \
       RT_UpdateCumulativeHeatingCoolingRate.o \
       RT_UpdateLOS.o \
       RT_UpdateLOSElement.o \
       RT_UpdateParticles.o \
       RT_useEquilibriumValues.o \
       RT_AdaptiveTimescaleLOSCell.o \
       RT_ConstantsPrint.o

ifeq ($(MPI),1)
 OBJS += RT_DistributeLOSs_mpi.o \
	 RT_Data_MPI_Recv.o \
	 RT_Data_MPI_Send.o
else
 OBJS += RT_DistributeLOSs.o
endif

SRC = $(OBJS:.o=.c)
LIB = libRT.a

all: $(LIB)

$(LIB): $(OBJS)
	$(AR) $(AR_FLAGS) r $(LIB) $(OBJS)

.PHONY: clean
clean:
	-rm *.o
	-rm *~
	-rm splint.log

.PHONY: distclean
distclean: clean
	-rm $(LIB)
	-rm dep.mak

.PHONY: indent
indent:
	indent *.c
	indent *.h

.PHONY: clang-format
clang-format:
	clang-format -i *.c
	clang-format -i *.h

TestIntegration: $(OBJS) TestIntegration.o
	$(LD) -o TestIntegration TestIntegration.o $(OBJS) \
		 $(LD_FLAGS) $(LIBS)

.PHONY: dep
dep: dep.mak

dep.mak: $(SRC)
	-rm dep.mak
	for file in $(SRC) ; do \
	 ( $(DEPEND) $(DEPEND_FLAGS) $(INCS) $(DEFS) $$file >> dep.mak ) \
	done

.PHONY: check
check:
	for file in $(SRC) ; do \
	 ( splint +posixlib -nullderef -nullpass -realcompare +trytorecover -booltype bool $(INCS) $(DEFS) $$file >> splint.log ) \
	done

-include dep.mak

.SUFFIXES: .o .c

.c.o:
	$(CC) $(CFLAGS) $(DEFS) $(INCS) -c $<
