/* Send via MPI a RT_Data structure and associated memory, including
 * refinements.
 *
 * ARGUMENTS
 *  Data  Pointer to the RT_Data structure to send.
 *  dest  The destination node.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * SEE ALSO
 *  RT_Data_MPI_Recv
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  06 04 11 First version.
 *  06 05 19 Wee problem with the order of preprocessor conditionals.
 *           Changed MPI_Send to MPI_Ssend.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "RT_Data.h"
#include "config.h"

int RT_Data_MPI_Send(RT_Data const* const Data, int const dest) {
  int const TAG = 10; /* This can be any integer? */
#ifdef REFINEMENTS
  int i;
  int ierr;
#endif
#if (VERBOSE >= 1) || (defined DEBUG)
  int me;
  (void)MPI_Comm_rank(MPI_COMM_WORLD, &me);
#endif

#if VERBOSE >= 1
  printf("(%2i) RT_Data_MPI_Send: Sending RT_Data to %i\n", me, dest);
  (void)fflush(stdout);
#endif

#if (defined DEBUG) && 0
  if (Data->NumCells >= 4096) { /* MAX_NSLICES in RT_SplitCell */
    printf("(%2i) ERROR: RT_Data_MPI_Send: %i: NumCells=%i > MAX_NSLICES.\n",
           me,
           __LINE__,
           Data->NumCells);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  /* Send the details so the receiver can allocate the memory space. */
  (void)MPI_Ssend(&(Data->NumCells), 1, MPI_INT, dest, TAG, MPI_COMM_WORLD);
  (void)MPI_Ssend(&(Data->Cell), 1, MPI_INT, dest, TAG, MPI_COMM_WORLD);
  (void)MPI_Ssend(&(Data->NBytes), 1, MPI_INT, dest, TAG, MPI_COMM_WORLD);

  /* Send the memory space */
  (void)MPI_Ssend(
      Data->MemSpace, Data->NBytes, MPI_BYTE, dest, TAG, MPI_COMM_WORLD);

#ifdef REFINEMENTS
  /* Loop through all cells.  If the refinement pointer is not NULL, then send
   * what it points to. Then free the refinement since it is now someone else's
   * problem. */
  for (i = 0; i < Data->NumCells; i++) {
    if (Data->Ref_pointer[i]) {
#  if VERBOSE >= 1
      printf("(%2i) RT_Data_MPI_Send: Sending refinement for cell %i to %i\n",
             me,
             i,
             dest);
      (void)fflush(stdout);
#  endif
#  if (defined DEBUG) && 0
      if (Data->Ref_pointer[i]->NumCells >=
          4096) { /* MAX_NSLICES in RT_SplitCell */
        printf(
            "(%2i) ERROR: RT_Data_MPI_Send: %i: Refinement %i has NumCells=%i "
            "> MAX_NSLICES.\n",
            me,
            __LINE__,
            i,
            Data->Ref_pointer[i]->NumCells);
        (void)fflush(stdout);
        return EXIT_FAILURE;
      }
#  endif
      ierr = RT_Data_MPI_Send(Data->Ref_pointer[i], dest);
#  ifdef DEBUG
      if (ierr == EXIT_FAILURE) {
        printf(
            "(%2i) ERROR: RT_Data_MPI_Send: %i: Failed to send refinement "
            "%i.\n",
            me,
            __LINE__ - 4,
            i);
        (void)fflush(stdout);
        return EXIT_FAILURE;
      }
#  endif
      Data->Ref_pointer[i] = NULL;
    }
  }
#endif

  RT_Data_Free(Data);
  /* Data needs to be set Data=NULL by the calling routine. Setting here is
   * meaningless since we only have a copy of the pointer. */
  return EXIT_SUCCESS;
}
