/* Merge a split cell back into a single cell.
 *
 * ARGUMENTS
 *  Input, not modified
 *   Data_cell  The RT_Data for the cell to be merged.
 *   cell       The Data_los[cell] to merge Data_cell into.
 *  Initialized
 *   Data_los   The RT_Data for the LOS for which cell, cell, has been refined.
 *
 * RETURNS nothing
 *
 * AUTHOR: Eric Tittley
 */
#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#ifdef REFINEMENTS

#  include <math.h>
#  include <stdlib.h>

#  include "RT.h"
#  include "RT_Data.h"

void RT_MergeCell(RT_Data const* const Data_cell,
                  size_t const cell,
                  RT_Data* const Data_los) {
  /* Get the number of slices */
  size_t const N = Data_cell->NumCells;
#  ifdef DEBUG
  if (N <= 0) {
    printf("ERROR: %s: %i: N=%lu\n", __FILE__, __LINE__, N);
    (void)fflush(stdout);
    return;
  }
#  endif

#  if VERBOSE >= 2
  printf("%s: Merging %lu slices\n", __FILE__, N);
  (void)fflush(stdout);
#  endif

  size_t i;
  rt_float Entropy = 0.;
  rt_float T = 0.;
  rt_float n_H = 0.;
  rt_float f_H1 = 0.;
  rt_float f_H2 = 0.;
  rt_float n_He = 0.;
  rt_float f_He1 = 0.;
  rt_float f_He2 = 0.;
  rt_float f_He3 = 0.;
  for (i = 0; i < N; i++) {
    Entropy += Data_cell->Entropy[i];
    T += Data_cell->T[i];
    n_H += Data_cell->n_H[i];
    f_H1 += Data_cell->f_H1[i];
    f_H2 += Data_cell->f_H2[i];
    n_He += Data_cell->n_He[i];
    f_He1 += Data_cell->f_He1[i];
    f_He2 += Data_cell->f_He2[i];
    f_He3 += Data_cell->f_He3[i];
    /* R calculated */
    /* D calculated */
    /* column_*[0], v_x, and v_los do not change */
  }

  /* Fill the cell values with the applicable averages or sums. */
  Data_los->Entropy[cell] = Entropy / (rt_float)N;
  Data_los->T[cell] =
      T / (rt_float)N; /* Not entirely correct, since T = f(Entropy) */
  Data_los->n_H[cell] = n_H / (rt_float)N;
  Data_los->f_H1[cell] = f_H1 / (rt_float)N;
  Data_los->f_H2[cell] = f_H2 / (rt_float)N;
  Data_los->n_He[cell] = n_He / (rt_float)N;
  Data_los->f_He1[cell] = f_He1 / (rt_float)N;
  Data_los->f_He2[cell] = f_He2 / (rt_float)N;
  Data_los->f_He3[cell] = f_He3 / (rt_float)N;
  /* R calculated */
  /* D calculated */
  /* column_*[0], v_x, and v_los do not change */
}
#endif /* REFINEMENTS */
