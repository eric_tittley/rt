#ifndef RT_RATEST_H
#define RT_RATEST_H

#include <stdio.h>

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_Precision.h"

/* A structure that can be used as a single structure or an array of structures
 */
struct RT_Rates_struct {
  rt_float I_H1;
  rt_float I_He1;
  rt_float I_He2;
  rt_float G;
  rt_float G_H1;
  rt_float G_He1;
  rt_float G_He2;
  rt_float alpha_H1;
  rt_float alpha_He1;
  rt_float alpha_He2;
  rt_float L;
  rt_float Gamma_HI;
  rt_float n_HI_Equilibrium;
  rt_float n_HII_Equilibrium;
  rt_float TimeScale;
};
typedef struct RT_Rates_struct RT_RatesT;

struct RT_RatesBlock_struct {
  size_t NumCells; /* Length of each of the rates */
  rt_float *I_H1;
  rt_float *I_He1;
  rt_float *I_He2;
  rt_float *G;
  rt_float *G_H1;
  rt_float *G_He1;
  rt_float *G_He2;
  rt_float *alpha_H1;
  rt_float *alpha_He1;
  rt_float *alpha_He2;
  rt_float *L;
  rt_float *Gamma_HI;
  rt_float *n_HI_Equilibrium;
  rt_float *n_HII_Equilibrium;
  rt_float *TimeScale;
};
typedef struct RT_RatesBlock_struct RT_RatesBlockT;

void RT_Rates_ZeroRates(RT_RatesBlockT Rates);

int RT_RatesBlock_Allocate(size_t const NumCells, RT_RatesBlockT *const Rates);

void RT_RatesBlock_Free(RT_RatesBlockT Rates);

int RT_RatesBlock_Write(RT_RatesBlockT const Rates, FILE *const fid);

#endif
