/* RT_UpdateLOSElementElement: Update Entropy and Ionization states for the
 *                             next iteration for all elements in a LOS.
 *
 * ierr = RT_UpdateLOSElement(rt_float const dt_RT, rt_float const D,
 *                            rt_float const ExpansionFactor, RT_RatesT *Rates,
 * RT_Cell *Data_cell );
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  dt_RT
 *  D
 *  ExpansionFactor
 *  Rates.I_H1
 *  Rates.I_He1
 *  Rates.I_He2
 *  Rates.alpha_H1
 *  Rates.alpha_He1
 *  Rates.alpha_He2
 *  Data_cell.n_H
 *  Data_cell.n_He
 *  Data_cell.f_H1
 *  Data_cell.f_H2
 *  Data_cell.f_He1
 *  Data_cell.f_He2
 *  Data_cell.f_He3
 *  Data_cell.Entropy
 *
 * ARGUMENTS INPUT AND MODIFIED
 *  n_H1
 *  n_H2
 *  n_He1
 *  n_He2
 *  n_He3
 *  Entropy
 *
 * ARGUMENTS INITIALISED
 *  f_*         Ionization fractions of various species.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * PREPROCESSOR MACROS
 *  DEBUG
 *  VERBOSE
 *  ENFORCE_MINIMUM_CMB_TEMPERATURE
 *
 * SEE ALSO
 *  RT_UpdateCell does the same for a single Cell structure
 *  (RT_UpdateLOSElement differs by updating a structure of arrays, instead of
 * an element of an array of structures)
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define VERBOSE 0
#  define ENFORCE_MINIMUM_CMB_TEMPERATURE 2.0
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif
#include <math.h>

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_RatesT.h"

int RT_UpdateLOSElement(size_t const iCell,
                        rt_float const dt_RT,
                        rt_float const ExpansionFactor,
                        RT_ConstantsT const Constants,
                        RT_RatesT const* const Rates,
                        RT_Data const Data) {
  /* Find the neutral & ionised number densities. */
  rt_float n_H1 = Data.n_H[iCell] * Data.f_H1[iCell];
  rt_float n_H2 = Data.n_H[iCell] * Data.f_H2[iCell];
  rt_float n_He1 = Data.n_He[iCell] * Data.f_He1[iCell];
  rt_float n_He2 = Data.n_He[iCell] * Data.f_He2[iCell];
  rt_float n_He3 = Data.n_He[iCell] * Data.f_He3[iCell];

  /* Electron density */
  rt_float n_e = n_H2 + n_He2 + (2.0 * n_He3);
#ifdef DEBUG
  if (!(bool)isfinite(n_e) || (n_e < 0)) {
    printf("ERROR: %s: %i: n_e=%5.3e; n_H2=%5.3e; n_He2=%5.3e; n_He3=%5.3e\n",
           __FILE__,
           __LINE__,
           n_e,
           n_H2,
           n_He2,
           n_He3);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  /* Maximum possible free electron density */
  rt_float const n_emax = Data.n_H[iCell] + (2.0 * Data.n_He[iCell]);
  if (n_e > n_emax * Constants.OnePlusEps) {
    printf("ERROR: %s: %i: n_e=%5.3e > n_emax=%5.3e\n",
           __FILE__,
           __LINE__,
           n_e,
           n_emax);
    printf("      n_e - n_emax = %5.3e\n", n_e - n_emax);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  /* dn_e/dt due to ionization/recombination (not compression/expansion) */
  rt_float dn_edt = Rates->I_H1 + Rates->I_He1 + Rates->I_He2 -
                    n_e * (n_H2 * Rates->alpha_H1 + n_He3 * Rates->alpha_He2 +
                           n_He2 * Rates->alpha_He1);

  /* Cannot liberate more electrons than available in un or partially ionised
   * species. */
  rt_float const max_dn_edt = (n_H1 + 2 * n_He1 + n_He2) / dt_RT;
  if (dn_edt > max_dn_edt) {
#if VERBOSE >= 3
    printf("WARNING: %s: Setting dn_edt from %5.3e to %5.3e\n",
           __FILE__,
           dn_edt,
           max_dn_edt);
    (void)fflush(stdout);
#endif
    dn_edt = max_dn_edt;
  }

  /* Cannot remove more electrons than are available */
  rt_float const min_dn_edt = -1.0 * n_e / dt_RT;
#ifdef DEBUG
  if (min_dn_edt > 0) {
    printf("ERROR: %s: %i: min_dn_edt > 0: n_e=%5.3e; dt_RT=%5.3e\n",
           __FILE__,
           __LINE__,
           n_e,
           dt_RT);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif
  if (dn_edt < min_dn_edt) {
#if VERBOSE >= 1
    printf("WARNING: %s: Setting dn_edt from %5.3e to %5.3e\n",
           __FILE__,
           dn_edt,
           min_dn_edt);
    (void)fflush(stdout);
#endif
    dn_edt = min_dn_edt;
  }

  /*Solves for Hydrogen species density */
  rt_float const n_H1new =
      n_H1 + dt_RT * (n_H2 * n_e * Rates->alpha_H1 - Rates->I_H1);
  rt_float const n_H2new =
      n_H2 + dt_RT * (Rates->I_H1 - n_H2 * n_e * Rates->alpha_H1);
#ifdef DEBUG
  if (!(bool)isfinite(n_H2new)) {
    printf(
        "ERROR: %s: %i: n_H2new=%5.3e: dt_RT=%5.3e n_H2=%5.3e "
        "Rates->I_H1=%5.3e n_e=%5.3e Rates->alpha_H1=%5.3e\n",
        __FILE__,
        __LINE__,
        n_H2new,
        dt_RT,
        n_H2,
        Rates->I_H1,
        n_e,
        Rates->alpha_H1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif
  n_H1 = n_H1new;
  n_H2 = n_H2new;

  /* The following conditions should never really be triggered. */
  if ((n_H1 < 0) || (n_H2 > Data.n_H[iCell])) {
#if VERBOSE >= 2
    printf("WARNING: %s: H ionizations/recombinations too fast.\n", __FILE__);
#endif
    n_H1 = 0.;
    n_H2 = Data.n_H[iCell];
  }
  if ((n_H1 > Data.n_H[iCell]) || (n_H2 < 0)) {
#if VERBOSE >= 2
    printf("WARNING: %s: H ionizations/recombinations too fast.\n", __FILE__);
#endif
    n_H1 = Data.n_H[iCell];
    n_H2 = 0.;
  }

  /*Solves for helium species densities */
  rt_float const n_He1new =
      n_He1 + dt_RT * (n_e * n_He2 * Rates->alpha_He1 - Rates->I_He1);
  rt_float const n_He2new =
      n_He2 +
      dt_RT * (Rates->I_He1 - Rates->I_He2 +
               n_e * (n_He3 * Rates->alpha_He2 - n_He2 * Rates->alpha_He1));
  rt_float const n_He3new =
      n_He3 + dt_RT * (Rates->I_He2 - n_e * n_He3 * Rates->alpha_He2);
#ifdef DEBUG
  if (!(bool)isfinite(n_He1new)) {
    printf(
        "ERROR: %s: %i: n_He1new=%5.3e: n_He1=%5.3e; dt_RT=%5.3e; n_e=%5.3e; "
        "n_He2=%5.3e; Rates->alpha_He2=%5.3e; Rates->I_He1=%5.3e\n",
        __FILE__,
        __LINE__,
        n_He1new,
        n_He1,
        dt_RT,
        n_e,
        n_He2,
        Rates->alpha_He1,
        Rates->I_He1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if (!(bool)isfinite(n_He2new)) {
    printf(
        "ERROR: %s: %i: n_He2new=%5.3e: n_He2=%5.3e; n_He3=%5.3e; n_e=%5.3e; "
        "dt_RT=%5.3e Rates->I_He1=%5.3e Rates->I_He2=%5.3e\n",
        __FILE__,
        __LINE__,
        n_He2new,
        n_He2,
        n_He3,
        n_e,
        dt_RT,
        Rates->I_He1,
        Rates->I_He2);
    printf(" Rates->alpha_He1=%5.3e; Rates->alpha_He2=%5.3e;\n",
           Rates->alpha_He1,
           Rates->alpha_He2);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if (!(bool)isfinite(n_He3new)) {
    printf(
        "ERROR: %s: %i: n_He3new=%5.3e: n_He3=%5.3e; dt_RT=%5.3e "
        "Rates->I_He2=%5.3e n_e=%5.3e Rates->alpha_He2=%5.3e\n",
        __FILE__,
        __LINE__,
        n_He3new,
        n_He3,
        dt_RT,
        Rates->I_He2,
        n_e,
        Rates->alpha_He2);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif
  n_He1 = n_He1new;
  n_He2 = n_He2new;
  n_He3 = n_He3new;

  if (n_He1 > Data.n_He[iCell]) {
    n_He1 = Data.n_He[iCell];
    n_He2 = 0.;
    n_He3 = 0.;
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  if (n_He2 > Data.n_He[iCell]) {
    n_He1 = 0.;
    n_He2 = Data.n_He[iCell];
    n_He3 = 0.;
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  if (n_He3 > Data.n_He[iCell]) {
    n_He1 = 0.;
    n_He2 = 0.;
    n_He3 = Data.n_He[iCell];
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  rt_float f;
  if (n_He1 < 0.) {
    n_He1 = 0.;
    if (n_He2 < n_He3) {
      f = n_He2 / (n_He2 + n_He3);
      n_He2 = f * Data.n_He[iCell];
      n_He3 = (1. - f) * Data.n_He[iCell];
    } else {
      f = n_He3 / (n_He2 + n_He3);
      n_He3 = f * Data.n_He[iCell];
      n_He2 = (1. - f) * Data.n_He[iCell];
    }
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  if (n_He2 < 0.) {
    n_He2 = 0.;
    if (n_He1 < n_He3) {
      f = n_He1 / (n_He1 + n_He3);
      n_He1 = f * Data.n_He[iCell];
      n_He3 = (1. - f) * Data.n_He[iCell];
    } else {
      f = n_He3 / (n_He1 + n_He3);
      n_He3 = f * Data.n_He[iCell];
      n_He1 = (1. - f) * Data.n_He[iCell];
    }
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }
  if (n_He3 < 0.) {
    n_He3 = 0.;
    if (n_He2 < n_He1) {
      f = n_He2 / (n_He2 + n_He1);
      n_He2 = f * Data.n_He[iCell];
      n_He1 = (1. - f) * Data.n_He[iCell];
    } else {
      f = n_He1 / (n_He2 + n_He1);
      n_He1 = f * Data.n_He[iCell];
      n_He2 = (1. - f) * Data.n_He[iCell];
    }
#if VERBOSE >= 2
    printf("WARNING: %s: He ionizations/recombinations too fast.\n", __FILE__);
#endif
  }

  /* Update ionization fractions */
  if (Data.n_H[iCell] > 0.) {
    Data.f_H1[iCell] = n_H1 / Data.n_H[iCell];
    Data.f_H2[iCell] = n_H2 / Data.n_H[iCell];
  } else {
    Data.f_H1[iCell] = 1.;
    Data.f_H2[iCell] = 0.;
  }
  /* remember: small = big - big leads to truncation errors.
   * Always perform the substraction: big = big - small */
  if (Data.f_H1[iCell] > Data.f_H2[iCell]) {
    Data.f_H1[iCell] = 1. - Data.f_H2[iCell];
  } else {
    Data.f_H2[iCell] = 1. - Data.f_H1[iCell];
  }
  if (Data.n_He[iCell] > 0.) {
    Data.f_He1[iCell] = n_He1 / Data.n_He[iCell];
    Data.f_He2[iCell] = n_He2 / Data.n_He[iCell];
    Data.f_He3[iCell] = n_He3 / Data.n_He[iCell];
    if ((Data.f_He1[iCell] >= Data.f_He2[iCell]) &&
        (Data.f_He1[iCell] >= Data.f_He3[iCell])) {
      Data.f_He1[iCell] = 1. - Data.f_He2[iCell] - Data.f_He3[iCell];
    } else if ((Data.f_He2[iCell] >= Data.f_He1[iCell]) &&
               (Data.f_He2[iCell] >= Data.f_He3[iCell])) {
      Data.f_He2[iCell] = 1. - Data.f_He1[iCell] - Data.f_He3[iCell];
    } else {
      Data.f_He3[iCell] = 1. - Data.f_He1[iCell] - Data.f_He2[iCell];
    }
  } else { /* No Helium */
    Data.f_He1[iCell] = 1.;
    Data.f_He2[iCell] = 0.;
    Data.f_He3[iCell] = 0.;
  }

  /* Update the entropy */
#ifdef NO_COOLING
  rt_float Entropy_new =
      Data.Entropy[iCell] + dt_RT * (Constants.gamma_ad - 1.) /
                                POW(Data.Density[iCell], Constants.gamma_ad) *
                                (Rates->G);
#else
  rt_float Entropy_new =
      Data.Entropy[iCell] + dt_RT * (Constants.gamma_ad - 1.) /
                                POW(Data.Density[iCell], Constants.gamma_ad) *
                                (Rates->G - Rates->L);
#endif

#ifdef DEBUG
  if (!(bool)isfinite(Entropy_new)) {
    printf(
        "ERROR: %s: %i: Entropy_new=%5.3e; Entropy=%5.3e; dt_RT=%5.3e; "
        "Density[%lu]=%5.3e; G=%5.3e; L=%5.3e;\n",
        __FILE__,
        __LINE__,
        Entropy_new,
        Data.Entropy[iCell],
        dt_RT,
        iCell,
        Data.Density[iCell],
        Rates->G,
        Rates->L);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

#ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
  /* The entropy cannot go below that set by the CMB temperature */

  /* Temperature is a function of Entropy */
  rt_float const Entropy_Min = RT_EntropyFromTemperature(
      Constants, ENFORCE_MINIMUM_CMB_TEMPERATURE, iCell, Data);
  if (Entropy_new < Entropy_Min) {
    Entropy_new = Entropy_Min;
  }
#  ifdef DEBUG
  if (!(bool)isfinite(Entropy_new) || (Entropy_new <= 0)) {
    printf("ERROR: %s: %i: Entropy_new=%5.3e; Data.Density[%lu]=%5.3e\n",
           __FILE__,
           __LINE__,
           Entropy_new,
           iCell,
           Data.Density[iCell]);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#  endif
#else
  if (Entropy_new < 0.) {
    /* NOTE: This is bad.  Entropy can never be zero. The code will crash
     * elsewhere if Entropy is zero.  Need something to put here. */
    Entropy_new = 0;
  }
#endif

  Data.Entropy[iCell] = Entropy_new;

#if VERBOSE > 2
  printf("%s: ...done\n", __FILE__);
  (void)fflush(stdout);
#endif

  return EXIT_SUCCESS;
}
