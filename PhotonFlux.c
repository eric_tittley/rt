/* PhotoFlux: Integrate the number of ionising photons produced by the source.
 *
 * rt_float PhotonFlux(rt_float nu_start,
 *                     rt_float nu_finish,
 *                     rt_float z,
 *                     int N_Slices,
 *                     RT_SourceT *Source )
 *
 * ARGUMENTS
 *  Input, not modified
 *   nu_start    The frequency at which to commence the integration. [Hz]
 *   nu_finish   Upper limit of the integration. [Hz]
 *   z           The redshift, in case the source evolves with time.
 *   N_Slices    The number of points to use in Simpson's Rule.
 *   Source      The source parameters
 *
 * RETURNS
 *  The rate of ionising photons emission by the source. [s^-1]
 *
 * NOTES
 *  Uses Simpson's rule.
 *
 * CALLS
 *  RT_Luminosity() function pointed to by Source.LuminosityFunction
 *
 * AUTHOR: Eric Tittley
 */

#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Luminosity.h"
#include "RT_SourceT.h"
#include "config.h"

rt_float PhotonFlux(rt_float const nu_start,
                    rt_float const nu_finish,
                    rt_float const z,
                    size_t const N_Slices,
                    RT_SourceT const Source,
                    RT_ConstantsT const Constants) {
  rt_float const dnu = (nu_finish - nu_start) / N_Slices;
  rt_float nu;
  rt_float F_start, F_mid, F_end;
  rt_float total = 0;
  int k;

  /* Re-cast the LuminosityFunction from void (*)(void *) */
  rt_float (*LuminosityFunction)(RT_LUMINOSITY_ARGS) =
      (rt_float(*)(RT_LUMINOSITY_ARGS))Source.LuminosityFunction;

  /* For the first iteration, set up the values assumed to already be calculated
   */
  F_end = (*(LuminosityFunction))(nu_start, z, &Source, &Constants) / nu_start;

  for (k = 0; k < N_Slices; k++) {
    /* The frequency step, a (units of nu), and a+dnu/2 and a+dnu where the
     * function will be evaluated. */
    nu = (rt_float)k * dnu + nu_start;

    /* Evaluate the function at the three points */
    F_start = F_end; /* reuse from previous iteration */
    F_mid = (*(LuminosityFunction))(nu + dnu / 2.0, z, &Source, &Constants) /
            (nu + dnu / 2.0);
    F_end =
        (*(LuminosityFunction))(nu + dnu, z, &Source, &Constants) / (nu + dnu);

    /* Simpson's rule */
    total += dnu / 6.0 * (F_start + 4.0 * F_mid + F_end) / Constants.h_p;
  }

  return total;
}
