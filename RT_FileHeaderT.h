#ifndef _RT_FILEHEADERT_H_
#define _RT_FILEHEADERT_H_

struct RT_FileHeader_struct {
  float ExpansionFactor;
  float Redshift;
  float Time;
};

typedef struct RT_FileHeader_struct RT_FileHeaderT;

#endif
