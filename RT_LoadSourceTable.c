/* RT_LoadSourceTable: Load the source luminosity function table from file.
 *
 * ierr = RT_LoadSourceTable(me, filename);
 *
 * ARGUMENTS
 *  me        The MPI ID. Ignored if MPI is not set.
 *  filename  The file from which to load the source spectrum.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * NOTES
 *  Allocates memory and loads the data into globally-declared pointers.
 *  I'm not happy with this.  I like to minimise the use of global variables.
 *  But it makes the call to RT_Luminosity independent of whether it uses a
 *  table or not.  Perhaps I should overload the function call.
 *  The table must be two columns:
 *    Frequency [Hz]
 *    Intensity [W/Hz]
 *
 * EXTERNAL MACROS
 *  MASTER   The node number for the master node. (constants.h)
 *  MPI      Turns on broadcasting the table to the other nodes.
 *  VERBOSE  (config.h)
 *
 * EXTERNAL VARIABLES
 *  source_N   The number of elements in the table.
 *  source_nu  Pointer to the vector of table frequencies.
 *  source_f   Pointer to the vector of table intensities.
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define VERBOSE 0
#endif

#ifdef SOURCE_TABLE

#  include <stdio.h>
#  include <stdlib.h>

#  ifdef RT_MPI
#    include <mpi.h>
#  endif

#  include "RT.h"
#  include "RT_ConstantsT.h"
#  include "RT_SourceT.h"
#  include "Support.h"

/* Load in the table for the source spectrum from a file */
/* source_N, source_nu, & source_f are global in scope */
int RT_LoadSourceTable(int const me,
                       char const *const filename,
                       RT_SourceT const *const Source) {
  FILE *fp = NULL;
  int i;

#  if VERBOSE >= 1
  printf("(%i) RT_LoadSourceTable: Begin...\n", me);
  (void)fflush(stdout);
#  endif
#  ifdef RT_MPI
  if (me == MASTER) {
#  endif
    fp = fopen(filename, "r");
    if (fp == NULL) {
      printf("ERROR: RT_LoadSourceTable: Unable to open file: %s\n", filename);
      ReportFileError(filename);
      return EXIT_FAILURE;
    }
    (void)fscanf(fp, "%i", &(Source->source_N));
#  ifdef RT_MPI
  }
  (void)MPI_Bcast(&source_N, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
#  endif
  Source->source_nu = (rt_float *)malloc(Source->source_N * sizeof(rt_float));
  if (Source->source_nu == NULL) {
    printf(
        "(%i) ERROR: RT_LoadSourceTable %i: Allocation of memory failed: "
        "source_nu\n",
        me,
        __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Source->source_f = (rt_float *)malloc(Source->source_N * sizeof(rt_float));
  if (Source->source_f == NULL) {
    printf(
        "(%i) ERROR: RT_LoadSourceTable %i: Allocation of memory failed: "
        "source_f\n",
        me,
        __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#  ifdef RT_MPI
  if (me == MASTER) {
#  endif
    for (i = 0; i < Source->source_N; i++) {
      (void)fscanf(fp, "%lf %lf", Source->source_nu + i, Source->source_f + i);
    }
    (void)fclose(fp);
#  ifdef RT_MPI
  }
  (void)MPI_Bcast(
      Source->source_nu, Source->source_N, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
  (void)MPI_Bcast(
      Source->source_f, Source->source_N, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
#  endif

#  if (defined SOURCE_STARBURST) || (defined SOURCE_SB_QSO_HYBRID) || \
      (defined SOURCE_NORMALISE_SPECTRUM)
  /* Find the end of the table. If we are normalising the spectrum , then we
   * need nu_end. If we are using the hybrid model we need nu_end_table.
   * If we are normalising *and* using the hybrid model, then nu_end gets
   * set elsewhere. */
#    ifdef RT_MPI
  if (me == MASTER) {
#    endif
#    if (defined SOURCE_STARBURST) || (defined SOURCE_NORMALISE_SPECTRUM)
    Source->nu_end = Source->source_nu[Source->source_N - 1];
#      if VERBOSE >= 1
    printf("RT_LoadSourceTable: nu_end=%e\n", Source->nu_end);
    (void)fflush(stdout);
#      endif
#    endif
#    ifdef SOURCE_SB_QSO_HYBRID
    Source->nu_end_table = Source->source_nu[Source->source_N - 1];
#      if VERBOSE >= 1
    printf("RT_LoadSourceTable: nu_end_table=%e\n", Source->nu_end_table);
    (void)fflush(stdout);
#      endif
#    endif
#    ifdef RT_MPI
  }
#      if (defined SOURCE_STARBURST) || (defined SOURCE_NORMALISE_SPECTRUM)
  (void)MPI_Bcast(&Source->nu_end, 1, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
#      endif
#      ifdef SOURCE_SB_QSO_HYBRID
  (void)MPI_Bcast(&Source->nu_end_table, 1, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
#      endif
#    endif /* endif RT_MPI */
#  endif   /* endif hybrid or normalise */

#  if VERBOSE >= 1
  printf("(%i) RT_LoadSourceTable: Done\n", me);
  (void)fflush(stdout);
#  endif
  return EXIT_SUCCESS;
}

#endif /* SOURCE_TABLE */
