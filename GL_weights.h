#ifndef _GL_WEIGHTS_H_
#define _GL_WEIGHTS_H_

#include "RT_Precision.h"

/* Sample positions and Weights for Gauss-Legendre integration */
extern rt_float const Xs_02_Legendre[2], W_02_Legendre[2], Xs_04_Legendre[4],
    W_04_Legendre[4], Xs_08_Legendre[8], W_08_Legendre[8], Xs_16_Legendre[16],
    W_16_Legendre[16], Xs_32_Legendre[32], W_32_Legendre[32],
    Xs_64_Legendre[64], W_64_Legendre[64], Xs_128_Legendre[128],
    W_128_Legendre[128], Xs_256_Legendre[256], W_256_Legendre[256];

/* Sample positions and Weights for Gauss-Laguerre integration */
extern rt_float const Xs_02_Laguerre[2], W_02_Laguerre[2], Xs_04_Laguerre[4],
    W_04_Laguerre[4], Xs_08_Laguerre[8], W_08_Laguerre[8], Xs_16_Laguerre[16],
    W_16_Laguerre[16], Xs_32_Laguerre[32], W_32_Laguerre[32];
/*
                    Xs_64_Laguerre[64], W_64_Laguerre[64],
                    Xs_128_Laguerre[128], W_128_Laguerre[128],
                    Xs_256_Laguerre[256], W_256_Laguerre[256];
*/
#endif
