/* NOTE: Cooling rates are multiplied by Ma to keep them scaled within
 * a float range. */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math.h>

#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_Precision.h"
#include "RT_RatesT.h"

void RT_RecombinationRatesInclCooling(size_t const cellindex,
                                      rt_float const ExpansionFactor,
                                      RT_ConstantsT const Constants,
                                      RT_Data const Data,
                                      /*@out@*/ RT_RatesBlockT const Rates) {
  rt_float const n_H1 = Data.n_H[cellindex] * Data.f_H1[cellindex];
  rt_float const n_H2 = Data.n_H[cellindex] * Data.f_H2[cellindex];
  rt_float const n_He2 = Data.n_He[cellindex] * Data.f_He2[cellindex];
  rt_float const n_He3 = Data.n_He[cellindex] * Data.f_He3[cellindex];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

  rt_float const Temperature = RT_TemperatureFromEntropy(
      Constants, Data.Entropy[cellindex], cellindex, Data);

  rt_float const tau_H1_cell = Constants.sigma_0 * n_H1 * Data.dR[cellindex];

  /* The Recombination Coefficients */
  rt_float beta_H1, beta_He1, beta_He2;
  RT_RecombinationCoefficients(tau_H1_cell,
                               Temperature,
                               &(Rates.alpha_H1[cellindex]),
                               &(Rates.alpha_He1[cellindex]),
                               &(Rates.alpha_He2[cellindex]),
                               &beta_H1,
                               &beta_He1,
                               &beta_He2);

  Rates.L[cellindex] += n_e * n_H2 * (beta_H1 * Constants.Ma);
  Rates.L[cellindex] += n_e * n_He2 * (beta_He1 * Constants.Ma);
  Rates.L[cellindex] += n_e * n_He3 * (beta_He2 * Constants.Ma);
}
