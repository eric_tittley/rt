/* RT_ProcessUnrefinedCells: Process the cells that don't need refining.
 *
 * ARGUMENTS
 *  Input, not modified
 *   CellsNotRefined    List of cells that need to be processed
 *   NCellsNotRefined   Number of cells that need to be processed
 *   LastIterationFlag  Is this the last iteration?
 *   D                  The current density at time tCurrent
 *   ExpansionFactor                 The current expansion factor
 *   tCurrent           The current time (t + t_step)
 *   dt                 The timestep of the top level integrator
 *   dt_RT              The timestep for this iteration
 *   Source             The source parameters
 *
 *  Input, modified
 *   Data_los
 *   dt_RT_sugg
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * TODO
 *  ExpansionFactor can be calculated from t, or t can be calculated from
 * ExpansionFactor.
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else /* Set defaults */
#  define INTEGRATE_ODE_EULER
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_SourceT.h"
#include "RT_TimeScale.h"

int RT_ProcessUnrefinedCells(size_t const* const CellsNotRefined,
                             size_t const NCellsNotRefined,
                             unsigned char const LastIterationFlag,
                             rt_float const ExpansionFactor,
                             rt_float const tCurrent,
                             rt_float const dt,
                             rt_float const dt_RT,
                             RT_SourceT const Source,
                             RT_ConstantsT const Constants,
                             RT_Data* const Data_los,
                             RT_TimeScale* const dt_RT_sugg) {
  int i, iz;
#if defined(INTEGRATE_ODE_RUNGE_KUTTA) || defined(INTEGRATE_ODE_EULER)
  int ierr;
#endif

  RT_Cell Data_cell;

  //#pragma omp parallel for
  for (i = 0; i < NCellsNotRefined; i++) {
    iz = CellsNotRefined[i];

    /* Pack the data for the cell into a cell container */
    RT_Cell_Pack(iz, Data_los, &Data_cell);

    /* Integrate the ODE's through one time step. */
#ifdef INTEGRATE_ODE_EULER
    ierr = RT_ProcessCell(LastIterationFlag,
                          ExpansionFactor,
                          tCurrent,
                          dt,
                          dt_RT,
                          Source,
                          Constants,
                          &Data_cell,
                          dt_RT_sugg);
#  ifdef DEBUG
    if (ierr == EXIT_FAILURE) {
      printf("ERROR: %s: %i: RT_ProcessCell failed for cell %i\n",
             __FILE__,
             __LINE__,
             iz);
      (void)fflush(stdout);
      return EXIT_FAILURE;
    }
#  endif
#endif
#ifdef INTEGRATE_ODE_RUNGE_KUTTA
    if (dt_RT <= (MIN_DT * OnePlusEps)) { /* Use an Euler timestep if we're only
                                             after the rates. */
      ierr = RT_ProcessCell(LastIterationFlag,
                            ExpansionFactor,
                            tCurrent,
                            dt,
                            dt_RT,
                            Source,
                            Constants,
                            &Data_cell,
                            dt_RT_sugg);
#  ifdef DEBUG
      if (ierr == EXIT_FAILURE) {
        printf("ERROR: %s: %i: RT_ProcessCell failed for cell %i\n",
               __FILE__,
               __LINE__,
               iz);
        (void)fflush(stdout);
        return EXIT_FAILURE;
      }
#  endif
    } else {
      /* 4th-order Runge-Kutta */
      ierr = RT_ProcessCellRK(LastIterationFlag,
                              ExpansionFactor,
                              tCurrent,
                              dt,
                              dt_RT,
                              Source,
                              Constants,
                              &Data_cell,
                              dt_RT_sugg);
#  ifdef DEBUG
      if (ierr == EXIT_FAILURE) {
        printf("ERROR: %s: %i: RT_ProcessCellRK failed for cell %i\n",
               __FILE__,
               __LINE__,
               iz);
        (void)fflush(stdout);
        return EXIT_FAILURE;
      }
#  endif
    }
#endif
    RT_Cell_Unpack(iz, Data_los, &Data_cell);

  } /* end loop over iz */

  return EXIT_SUCCESS;
}
