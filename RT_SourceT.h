#ifndef _RT_SOURCE_T_H_
#define _RT_SOURCE_T_H_

#include "RT_ConstantsT.h"
#include "RT_Luminosity_Types.h"

enum RT_AttenuationGeometry_Types {
  PlaneWave,
  SphericalWave,
  RayPlaneWave,
  RaySphericalWave
};

typedef rt_float (*SourceFunctionT)(void *);

/* Note: Cannot currently deal with a table for the source */

struct RT_SourceT_struct {
  /* Position for sperical waves. Ray direction and originating plane for
   * plane waves.
   * r = {1, 0, 0}  is a plane wave originating at x = 0 in the y-z plane,
   *                moving in the +x direction.
   * r = {0, -1, 0} is a plane wave originating at y = BoxSize in the x-z plane,
   *                moving in the -y direction.
   * r = {1, 1, 0}  is not allowed. Only one non-zero term is allowed.
   */
  rt_float r[3];
  enum RT_Luminosity_Types SourceType; /* REQUIRED */
  /* The Luminosity Function for this source.
   * Can be set by a call to RT_SetSourceFunction.
   * The arguments list below must match RT_LUMINOSITY_ARGS set in
   * RT_Luminosity.h  Unfortunately circularity requires explicit declaration.
   */
  /* I would like to do the following, but if LuminosityFunction needs to point
   * to a __device__ function on the GPU, I'd need to define it here as
   * __device__, which isn't generic enough.
   * rt_float (*LuminosityFunction)(rt_float nu,
   *                              rt_float z,
   *                              struct RT_SourceT_struct const *Source,
   *                              RT_ConstantsT const *Constants);
   */
  /* void (*LuminosityFunction)(void *); */
  enum RT_AttenuationGeometry_Types AttenuationGeometry;
  SourceFunctionT LuminosityFunction;
  rt_float z_on;  /* The redshift the source turns on.  REQUIRED */
  rt_float z_off; /* The redshift the source turns off. REQUIRED */
  rt_float L_0;   /* The source luminosity. REQUIRED unless set
                   in Source1 & Source2 or BLACK BODY */
  rt_float alpha; /* Arbitrary power law. Don't use for simple ones, like ^-2 */
  rt_float T_BB;  /* BLACK BODY temperature */
  rt_float SurfaceArea; /* BLACK BODY surface area */
  rt_float PlaneSide;   /* Length of each side of a plane wave. */
  /* The following are for tabulated data */
  rt_float nu_end;
  rt_float nu_end_table;
  rt_float source_N;
  rt_float *source_f;
  rt_float *source_nu;
  rt_float z_trans_start;  /* Hybrid models: start of transition */
  rt_float z_trans_finish; /* Hybrid models: start of transition */
  /* For the Hybrid models, the parameters of the first and second components */
  struct RT_SourceT_struct *Source1;
  struct RT_SourceT_struct *Source2;
  rt_float nu; /* The frequency of a monochromatic source */
  rt_float
      R_0; /* The distance of the source from the near edge for PLANE_WAVE */

  /*tabulated starburst99 sources*/
  size_t particle_id;
  rt_float alphas[3];
  rt_float L_0s[3];
  rt_float *source_table; /*feels like a conveniant palce to store a pointer to
                            the table storing the alphas/luminosities*/
  rt_float halo_mass;
  rt_float luminosity_scale_factor;
};
typedef struct RT_SourceT_struct RT_SourceT;

#endif
