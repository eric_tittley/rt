#ifndef _RT_DATA_H_
#define _RT_DATA_H_

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdio.h>

#include "RT_ConstantsT.h"

#define VERSION_MAJOR 4
#define VERSION_MINOR 11

/* Change any of these and you need to change RT_Data_*.c
 * Don't include the MemSpace pointer as that's where the data will be stored.
 */
#define RT_DATA_N_FIELDS_DOUBLE_BASE 15

#ifdef RT_DATA_VELOCITIES
#  define RT_DATA_N_FIELDS_DOUBLE_VELOCITIES 2
#else
#  define RT_DATA_N_FIELDS_DOUBLE_VELOCITIES 0
#endif

#ifdef REFINEMENTS
#  define RT_DATA_N_FIELDS_DOUBLE_REFINEMENTS 12
#  define RT_DATA_N_FIELDS_POINTER 1
#else
#  define RT_DATA_N_FIELDS_DOUBLE_REFINEMENTS 0
#  define RT_DATA_N_FIELDS_POINTER 0
#endif

#define RT_DATA_N_FIELDS_DOUBLE \
  (RT_DATA_N_FIELDS_DOUBLE_BASE + RT_DATA_N_FIELDS_DOUBLE_VELOCITIES)

/* RT_Data contains the details of the state of the gas in a line of sight.
 * RT_Data is primarily a set of pointers into a contiguous block of memory
 * MemSpace.  In the construction of RT_Data, memory must be allocated to
 * MemSpace and then the pointers set to offsets from MemSpace.  Two routines
 * provide these functions: RT_Data_Allocate and RT_Data_Assign.*/
struct RT_Data_struct {
  /* Header */
  size_t NumCells; /* Number of cells in this list of cells */
  size_t Cell;     /* The cell number for which we are a refinement. */
  /* IS Cell ACTUALLY USED? Possibly only useful for post-processing. */
  size_t NBytes; /* Size of MemSpace in bytes. */
  /* Core data */
  rt_float *R;       /* Distance from source [m] */
  rt_float *dR;      /* Distance across the cell [m] */
  rt_float *Density; /* Current density [kg m^-3] */
  rt_float *Entropy; /* Entropy [J/kg (m^3/kg)^(gamma_ad-1)*/
  rt_float *T;       /* Temperature [k] */
  /* Number densities and ionization fractions [m^-3] */
  rt_float *n_H; /* Derivable? */
  rt_float *f_H1;
  rt_float *f_H2;
  rt_float *n_He; /* Derivable? */
  rt_float *f_He1;
  rt_float *f_He2;
  rt_float *f_He3;
  /* Column depth up to  each cell [m^-2] */
  rt_float *column_H1;
  rt_float *column_He1;
  rt_float *column_He2;
#ifdef RT_DATA_VELOCITIES
  /* Peculiar velocities along LOS (z) and perpendicular (x) [km s^-1] */
  rt_float *v_z;
  rt_float *v_x;
#endif
#if (defined REFINEMENTS) || (defined SEGMENTED_LOS)
  /* NCol exists for codes like EnzoRT in which the RT is performed on separate
   * grids (and nodes) along the same line of sight. NCol helps tie the ends
   * together. It really makes a muck of everything else. Refinements make use
   * of elements 0 to 5. */
  rt_float *NCol; /* The Column densities at the start and end of the iteration.
                    rt_float[12] =
   [column_H1_start[0], column_He1_start[0], column_He2_start[0],
    column_H1_end[0],   column_He1_end[0],   column_He2_end[0],
    column_H1_start[NumCells], column_He1_start[NumCells],
   column_He2_start[NumCells], column_H1_end[NumCells],
   column_He1_end[NumCells],   column_He2_end[NumCells]] where by [NumCells] I
   mean column_*[NumCells-1]+dr*n_*[NumCells-1] [0:8] values need to be assigned
   prior to being processed by through dt. [9:11] need to be assigned after
   processing through time dt. */
#endif
#ifdef REFINEMENTS
  /* A list of pointers to RT_Data structures holding data for the refinements
   * for cell iz. The list (Ref_pointer[i]) is either: NULL (no refinement). a
   * pointer to a RT_Data structure. */
  struct RT_Data_struct **Ref_pointer;
#endif

  /* The memory space allocated to contain the data for the above vectors.
   * The values for R, D, etc, will be offsets of MemSpace. */
  void *MemSpace;
};

/* Define the type RT_Data */
typedef struct RT_Data_struct RT_Data;

/* Creation, Initialization, and destruction */
/*@null@*/ RT_Data *RT_Data_Allocate(size_t const NumCells);

/* Radiative Transfer: Allocate memory for a list of Lines of Sight */
/*@null@*/ RT_Data **RT_Data_AllocateGridMemory(size_t const Nlos,
                                                size_t const Ncells);

void RT_Data_InitializeTemperatureAndFractions(size_t const Nlos,
                                               RT_ConstantsT const Constants,
                                               rt_float const Redshift,
                                               RT_Data **const Data);
int RT_Data_Assign(RT_Data *const Data);
void RT_Data_Free(/*@null@*/ RT_Data *Data);

/* MPI send and receive */
int RT_Data_MPI_Send(RT_Data *Data, int const dest);
/*@null@*/ RT_Data *RT_Data_MPI_Recv(int const orig);

/* IO to file */
int RT_Data_Write(RT_Data const *const Data, FILE *const fid);
/*@null@*/ RT_Data *RT_Data_Read(FILE *const fid);

/* Check the data */
int RT_Data_CheckData(RT_Data const Data, const char * File, const int Line);

/* Data for a single cell */
struct RT_Cell_struct {
  rt_float R;       /* Position [m] */
  rt_float dR;      /* Distance across cell */
  rt_float Density; /* Current density [kg m^-3] */
  rt_float Entropy; /* Entropy [J/kg (m^3/kg)^(gamma_ad-1) */
  rt_float T;       /* Temperature [K] */
  /* Number densities and ionization fractions [m^-3] */
  rt_float n_H;
  rt_float f_H1;
  rt_float f_H2;
  rt_float n_He;
  rt_float f_He1;
  rt_float f_He2;
  rt_float f_He3;
  /* Column depth up to cell [m^-2] */
  rt_float column_H1;
  rt_float column_He1;
  rt_float column_He2;
#ifdef RT_DATA_VELOCITIES
  /* Peculiar velocities along LOS (z) and perpendicular (x) [km s^-1] */
  rt_float v_z;
  rt_float v_x;
#endif
};

/* Define the type RT_Data */
typedef struct RT_Cell_struct RT_Cell;

void RT_Cell_Pack(size_t const iz,
                  RT_Data const *const Data_los,
                  /*@out@*/ RT_Cell *const Data_cell);
void RT_Cell_Unpack(size_t const iz,
                    RT_Data *const Data_los,
                    RT_Cell const *const Data_cell);
void RT_Cell_Copy(RT_Cell const *const Cell_from,
                  /*@out@*/ RT_Cell *const Cell_to);

#endif /* _RT_DATA_H_ */
