#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <math.h>
#  include <stdio.h>
#endif

#include "RT.h"

int RT_SHFEV_Iterate(RT_RatesBlockT const Rates, RT_Data const Data) {
  size_t cellindex;
  for (cellindex = 0; cellindex < Data.NumCells; cellindex++) {
    if (Rates.Gamma_HI[cellindex] > 0.0) {
      /* Find the neutral & ionised number densities. */
      rt_float n_H1 = Data.n_H[cellindex] * Data.f_H1[cellindex];
      rt_float n_H2 = Data.n_H[cellindex] * Data.f_H2[cellindex];
      rt_float const n_He2 = Data.n_He[cellindex] * Data.f_He2[cellindex];
      rt_float const n_He3 = Data.n_He[cellindex] * Data.f_He3[cellindex];

      rt_float n_e; /* Electron density */

      rt_float delta;
      rt_float const Tol = 0.01; /* 1% tolerance in fractional change to n_HI */
      unsigned int LoopCount = 0;
      unsigned int const LoopCountMax = 20;

      rt_float const AlphaDivGamma =
          Rates.alpha_H1[cellindex] / Rates.Gamma_HI[cellindex];

      if (n_H1 < n_H2) {
        rt_float n_H1_EquilOld;
        delta = 1.0; /* Trigger on first pass */
        while ((FABS(delta) > Tol) && (LoopCount < LoopCountMax)) {
          n_H1_EquilOld = n_H1;
          n_e = n_H2 + n_He2 + (2.0 * n_He3);
          n_H1 += n_H2 * n_e * AlphaDivGamma;
          n_H1 /= 2.0;
          if (n_H1 > Data.n_H[cellindex]) n_H1 = 0.5 * Data.n_H[cellindex];
#if VERBOSE >= 2
          printf(
              "LoopCount=%2i; n_H1_EquilOld=%5.3e; n_H1=%5.3e; n_H2=%5.3e; "
              "n_e=%5.3e; AlphaDivGamma=%5.3e; n_H=%5.3e\n",
              LoopCount,
              n_H1_EquilOld,
              n_H1,
              n_H2,
              n_e,
              AlphaDivGamma,
              Data.n_H[cellindex]);
#endif
          /* Big minus small is fine as fractional errors in n_HII will be small
           */
          n_H2 = Data.n_H[cellindex] - n_H1;
          delta = n_H1 / n_H1_EquilOld - 1.0;
          LoopCount++;
        }
      } else { /* n_H1 > n_H2 */
        rt_float n_H2_EquilOld;
        delta = 1.0; /* Trigger on first pass */
        while ((FABS(delta) > Tol) && (LoopCount < LoopCountMax)) {
          n_H2_EquilOld = n_H2;
          n_e = n_H2 + n_He2 + (2.0 * n_He3);
          n_H2 += n_H1 / (n_e * AlphaDivGamma);
          n_H2 /= 2.0;
          if (n_H2 > Data.n_H[cellindex]) n_H2 = 0.5 * Data.n_H[cellindex];
#if VERBOSE >= 2
          printf(
              "LoopCount=%2i; n_H2_EquilOld=%5.3e; n_H2=%5.3e; n_H1=%5.3e; "
              "n_e=%5.3e; AlphaDivGamma=%5.3e; n_H=%5.3e\n",
              LoopCount,
              n_H2_EquilOld,
              n_H2,
              n_H1,
              n_e,
              AlphaDivGamma,
              Data.n_H[cellindex]);
#endif
          /* Big minus small is fine as fractional errors in n_HII will be small
           */
          n_H1 = Data.n_H[cellindex] - n_H2;
          delta = n_H2 / n_H2_EquilOld - 1.0;
          LoopCount++;
        }
      }
#if VERBOSE >= 1
      if (LoopCount == LoopCountMax) {
        printf(
            "WARNING: %s: %i: %i Failed to converge on an equilibrium nHI. "
            "delta=%5.3e\n",
            __FILE__,
            __LINE__,
            cellindex,
            delta);
      }
#endif

      if ((n_H1 < Data.n_H[cellindex]) && (n_H2 < Data.n_H[cellindex])) {
        if (n_H1 < n_H2) {
          Rates.n_HI_Equilibrium[cellindex] = n_H1;
          Rates.n_HII_Equilibrium[cellindex] = Data.n_H[cellindex] - n_H1;
        } else {
          Rates.n_HI_Equilibrium[cellindex] = Data.n_H[cellindex] - n_H2;
          Rates.n_HII_Equilibrium[cellindex] = n_H2;
        }
      } else {
        if (n_H1 > Data.n_H[cellindex]) {
          Rates.n_HI_Equilibrium[cellindex] = Data.n_H[cellindex];
          Rates.n_HII_Equilibrium[cellindex] = 0.0;
        } else {
          Rates.n_HI_Equilibrium[cellindex] = 0.0;
          Rates.n_HII_Equilibrium[cellindex] = Data.n_H[cellindex];
        }
      }
#ifdef DEBUG
      if (!(bool)isfinite(Rates.n_HI_Equilibrium[cellindex])) {
        printf(
            "ERROR: %s: %i: n_HI_Equilibrium=%5.3e; n_H2=%5.3e; n_e=%5.3e; "
            "alpha_H1=%5.3e; Gamma_HI=%5.3e\n",
            __FILE__,
            __LINE__,
            Rates.n_HI_Equilibrium[cellindex],
            n_H2,
            n_e,
            Rates.alpha_H1[cellindex],
            Rates.Gamma_HI[cellindex]);
        (void)fflush(stdout);
        return EXIT_FAILURE;
      }
      if (!(bool)isfinite(Rates.n_HII_Equilibrium[cellindex])) {
        printf(
            "ERROR: %s: %i: n_HII_Equilibrium=%5.3e; n_H=%5.3e; "
            "n_HI_Equilibrium=%5.3e\n",
            __FILE__,
            __LINE__,
            Rates.n_HII_Equilibrium[cellindex],
            Data.n_H[cellindex],
            Rates.n_HI_Equilibrium[cellindex]);
        (void)fflush(stdout);
        return EXIT_FAILURE;
      }
#endif
    } else {
      Rates.n_HI_Equilibrium[cellindex] = Data.n_H[cellindex];
      Rates.n_HII_Equilibrium[cellindex] = 0.0;
    }

  } /* End loop over cells */

  return EXIT_SUCCESS;
}

int RT_SHFEV_noHe(RT_RatesBlockT const Rates, RT_Data const Data) {
  size_t cellindex;
  double C;
  rt_float f_HI;
  for (cellindex = 0; cellindex < Data.NumCells; cellindex++) {
    if ((Rates.alpha_H1[cellindex] * Data.n_H[cellindex]) > 0.0) {
      /* Solution is the "minus" of the quadratic equation solution */
      C = Rates.Gamma_HI[cellindex] /
          (Rates.alpha_H1[cellindex] * Data.n_H[cellindex]);
      f_HI = (rt_float)(((2.0 + C) - sqrt((2.0 + C) * (2.0 + C) - 4.0)) / 2.0);
      Rates.n_HI_Equilibrium[cellindex] = f_HI * Data.n_H[cellindex];
      Rates.n_HII_Equilibrium[cellindex] = (1.0 - f_HI) * Data.n_H[cellindex];
    } else {
      Rates.n_HII_Equilibrium[cellindex] = 0.0;
      if (Data.n_H[cellindex] <= 0.0) {
        Rates.n_HI_Equilibrium[cellindex] = 0.0;
      } else {
        /* alpha is 0 */
        Rates.n_HI_Equilibrium[cellindex] = Data.n_H[cellindex];
      }
    }
  }
  return EXIT_SUCCESS;
}

int RT_SetHydrogenFractionEquilibriumValues(RT_RatesBlockT const Rates,
                                            RT_Data const Data) {
  int ierr;
  if (Data.n_He[0] > 0.0) {
    ierr = RT_SHFEV_Iterate(Rates, Data);
  } else {
    /* No helium */
    ierr = RT_SHFEV_noHe(Rates, Data);
  }
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: FAILED\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
