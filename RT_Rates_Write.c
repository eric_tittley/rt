/* Write a RT_RatesBlockT structure to a file stream.
 *
 * ARGUMENTS
 *  Rates  Pointer to the RatesT structure to write
 *  fid    Pointer to the file stream to which to write.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE if unable to write any component.
 *
 * SEE ALSO
 *  RT_RatesT, RT_Data_Write, RT_SaveData
 *
 * TODO: RT_RatesRateBlockT should be malloced as a block and then the pointers
 *       assigned, just at RT_Data is.
 *
 * AUTHOR: Eric Tittley
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "RT_RatesT.h"

int RT_RatesBlock_Write(RT_RatesBlockT const Rates, FILE* const fid) {
  size_t Nwrote;

  /* Explicitly initialize the header */
  int hdrInt[128];
  float
      hdrFloat[128]; /* Not rt_float as we want to keep it a predictable size */
  memset(hdrInt, 0, 128 * sizeof(int));
  memset(hdrFloat, 0, 128 * sizeof(float));

  /* Write the integer 1.  If it can be read as a one, then we know
   * the endianess is correct.  Otherwise, we can re-read it with swapped
   * endianess. */
  hdrInt[0] = 1;
  hdrInt[1] = (int)Rates.NumCells; /* Will fail if more than 2 billion cells */
#ifndef RT_DOUBLE
  hdrInt[2] = 1; /* Single precision flag */
#endif
#ifndef NO_COOLING
  hdrInt[3] = 1;
#endif
  size_t Nwritten = fwrite(hdrInt, 128 * sizeof(int), 1, fid);
  if (Nwritten != 1) {
    printf("ERROR: %s: %i: Failed to write hdrInt.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwritten = fwrite(hdrFloat, 128 * sizeof(float), 1, fid);
  if (Nwritten != 1) {
    printf("ERROR: %s: %i: Failed to write hdrFloat.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  Nwrote = fwrite(Rates.I_H1, sizeof(rt_float), Rates.NumCells, fid);
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.I_H1.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.I_He1, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.I_He1.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.I_He2, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.I_He2.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.G, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.G.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.G_H1, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.G_H1.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.G_He1, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.G_He1.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.G_He2, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.G_He2.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.alpha_H1, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf(
        "ERROR: %s: %i: Unable to write Rates.alpha_H1.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.alpha_He1, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.alpha_He1.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.alpha_He2, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.alpha_He2.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.L, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.L.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.Gamma_HI, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf(
        "ERROR: %s: %i: Unable to write Rates.Gamma_HI.\n", __FILE__, __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote =
      fwrite(Rates.n_HI_Equilibrium, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.n_HI_Equilibrium.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote =
      fwrite(Rates.n_HII_Equilibrium, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.n_HII_Equilibrium.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  Nwrote = fwrite(Rates.TimeScale, sizeof(rt_float), Rates.NumCells, fid);
  ;
  if (Nwrote != Rates.NumCells) {
    printf("ERROR: %s: %i: Unable to write Rates.TimeScale.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
