#ifndef _EPSILON_H_
#define _EPSILON_H_

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#ifdef RT_DOUBLE

#  ifndef EPSILON
#    ifdef __DBL_EPSILON__
#      define EPSILON __DBL_EPSILON__
#    endif
#  endif

#  ifndef FLOAT_MAX
#    ifdef __DBL_MAX__
#      define FLOAT_MAX __DBL_MAX__
#    endif
#  endif

#  ifndef FLOAT_MIN
#    ifdef __DBL_MIN__
#      define FLOAT_MIN __DBL_MIN__
#    endif
#  endif

#  define LN_EPSILON -36.043653389117154
#  define INV_LN_EPSILON 36.043653389117154
#  define LN_MAX_FLT 709.782712893384

#else /* RT_SINGLE (default) */

#  ifndef EPSILON
#    ifdef __FLT_EPSILON__
#      define EPSILON __FLT_EPSILON__
#    endif
#  endif

#  ifndef FLOAT_MAX
#    ifdef __FLT_MAX__
#      define FLOAT_MAX __FLT_MAX__
#    endif
#  endif

#  ifndef FLOAT_MIN
#    ifdef __FLT_MIN__
#      define FLOAT_MIN __FLT_MIN__
#    endif
#  endif

#  define LN_EPSILON -15.942385152878742f
#  define INV_LN_EPSILON 15.942385152878742f
#  define LN_MAX_FLT 88.7228390520684f

#endif

#endif /* _EPSILON_H_ */
