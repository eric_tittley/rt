#include <stdio.h>
#include <stdlib.h>

#include "RT.h"
#include "RT_ConstantsT.h"

void RT_ConstantsPrint(RT_ConstantsT const Constants) {
  printf("Constants.NLevels (%p)=(%lu %lu %lu)\n",
         Constants.NLevels,
         Constants.NLevels[0],
         Constants.NLevels[1],
         Constants.NLevels[2]);

  size_t iInterval, iLevel, iSpecies;
  for (iInterval = 0; iInterval < Constants.NLevels_NIntervals; ++iInterval) {
    for (iLevel = 0; iLevel < Constants.NLevels[iInterval]; ++iLevel) {
      printf("Alpha[%lu][%lu]=(", iInterval, iLevel);
      for (iSpecies = 0; iSpecies < 3; ++iSpecies) {
        printf("%e ", Constants.Alpha[iInterval][iLevel][iSpecies]);
      }
      printf(")\n");
    }
  }
}
