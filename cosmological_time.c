/* cosmological_time: Age of the universe given the expansion factor.
 *
 * rt_float cosmological_time(rt_float a)
 *
 * ARGUMENTS
 *  Input, not modified
 *   a   The expansion factor at which point to find the age.
 *
 * RETURNS
 *   The age of the universe. [s]
 *
 * AUTHOR: Eric Tittley
 */

#include "config.h"

#ifdef DEBUG
#  include <stdio.h>
#endif
#include <math.h>
#ifdef S_SPLINT_S
extern rt_float ASINH(rt_float);
extern int isfinite(rt_float);
#endif

#include "RT.h"
#include "RT_ConstantsT.h"
#include "isequal.h"

rt_float cosmological_time(rt_float const a, RT_ConstantsT const Constants) {
  /* Time (s) as a function of expansion factor */
  /* Note, this equation reduces to  2/(3H_0) * a^1.5 for omega_m=1 */
  rt_float const H_0 = Constants.h * 1e5 / Constants.Mpc; /* s^{-1} */
  rt_float ct, ct1, ct2, ct3;
  if (isequal(Constants.omega_m, 1.0)) {
    ct = 2. / (3. * H_0) * POW(a, 1.5);
  } else {
    ct1 = (2. / (3. * H_0 * SQRT(1. - Constants.omega_m)));
    ct2 = a * SQRT(a);
    ct3 = ASINH(SQRT((1. - Constants.omega_m) / Constants.omega_m) * ct2);
    ct = ct1 * ct3;
  }
#ifdef DEBUG
  if (!(bool)isfinite(ct)) {
    printf(
        "ERROR: cosmological_time: %i: ct=%f; H_0=%5.3e; omega_m=%5.3e, "
        "a=%5.3e\n",
        __LINE__ - 5,
        ct,
        H_0,
        Constants.omega_m,
        a);
    (void)fflush(stdout);
  }
#endif
  return ct;
}
