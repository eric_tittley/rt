/* Read the RT data from file.
 *
 * ARGUMENTS
 *  Input, not modified
 *   filename   The name of the file from which to read the data.
 *   Nlos       The number of lines-of-sight to be read.
 *  Input, initialised
 *   Data       The structure pointer to contain the data to be read.
 *
 * RETURNS
 *  Nlos        The number of Lines of Sight in the file.
 *              0 if failed.
 *
 * AUTHOR: Eric Tittley
 */
#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>

#include "RT.h"
#include "RT_Data.h"
#include "RT_FileHeaderT.h"

#ifdef SUPPORT_EXISTS
#  include "Support.h"
#endif

size_t RT_LoadData(char const *const filename,
                   /*@null@*/ RT_Data **Data,
                   RT_FileHeaderT *const FileHeader) {
  FILE *fid;
  int ierr;
  int hdrInt[128];
  float hdrFloat[128];
  size_t Nread;
  size_t Nlos = 0;

  /* Open the file */
  fid = fopen(filename, "r");
  if (fid == NULL) {
    printf("ERROR: %s: Unable to open file %s\n", __FILE__, filename);
#ifdef SUPPORT_EXISTS
    ReportFileError(filename);
#endif
    (void)fflush(stdout);
    return 0;
  }

  /* *** Read the wee header *** */
  /* Read the integer 1.  If it can be read as a one, then we know
   * the endianess is correct.  Otherwise, we can re-read it with swapped
   * endianess. */
  Nread = fread(hdrInt, 128 * sizeof(int), 1, fid);
  if (Nread != 1) {
    printf("ERROR: %s: Only read %d of %d elements from %s\n",
           __FILE__,
           (int)Nread,
           1,
           filename);
    (void)fflush(stdout);
    return 0;
  }
  if (hdrInt[0] != 1) {
    printf("ERROR: %s: Wrong endianess.\n", __FILE__);
    (void)fflush(stdout);
    return 0;
  }
  if ((hdrInt[1] != VERSION_MAJOR) || (hdrInt[2] != VERSION_MINOR)) {
    printf("ERROR: %s: Wrong version (%i.%i).\n",
           __FILE__,
           VERSION_MAJOR,
           VERSION_MINOR);
    (void)fflush(stdout);
    return 0;
  }

  if (hdrInt[4] <= 0) return 0;

  Nlos = hdrInt[4];

  Nread = fread(hdrFloat, 128 * sizeof(float), 1, fid);
  if (Nread != 1) {
    printf("ERROR: %s: Only read %d of %d elements from %s\n",
           __FILE__,
           (int)Nread,
           1,
           filename);
    (void)fflush(stdout);
    return 0;
  }

  FileHeader->ExpansionFactor = hdrFloat[0];
  FileHeader->Redshift = hdrFloat[1];
  FileHeader->Time = hdrFloat[2];

  /* Read the data */
  size_t i;
  for (i = 0; i < Nlos; i++) {
    /* We've pre-allocated memory.  Free it before reading in data. */
    RT_Data_Free(Data[i]);
    /* Read in one LOS */
    Data[i] = RT_Data_Read(fid);
    if (Data[i] == NULL) {
      printf("ERROR: %s: %i: Failed to read RT_Data[%lu]\n",
             __FILE__,
             __LINE__,
             i);
      (void)fflush(stdout);
      return 0;
    }
  }

#ifdef REFINEMENTS
  /* Read the refinements */
  for (i = 0; i < Nlos; i++) {
    size_t j;
    for (j = 0; j < Data[i]->NumCells; j++) {
      if (Data[i]->Ref_pointer[j]) {
        Data[i]->Ref_pointer[j] = RT_Data_Read(fid);
        if (Data[i]->Ref_pointer[j] == NULL) {
          printf(
              "ERROR: %s: %i: Failed to read refinement for cell %i in LOS "
              "%i.\n",
              __FILE__,
              __LINE__,
              j,
              i);
          (void)fflush(stdout);
          return 0;
        }
      }
    }
  }
#endif

  /* Close the file */
  ierr = fclose(fid);
  if (ierr != 0) {
    printf("ERROR: %s: %i: Unable to close file.\n", __FILE__, __LINE__);
#ifdef SUPPORT_EXISTS
    ReportFileError(filename);
#endif
    (void)fflush(stdout);
    return 0;
  }

  return Nlos;
}
