#include "RT.h"

void RT_UpdateCumulativeHeatingCoolingRate(RT_RatesBlockT Rates) {
  size_t iParticle;
  for (iParticle = 0; iParticle < Rates.NumCells; iParticle++) {
    Rates.G[iParticle] = Rates.G_H1[iParticle];
    Rates.G[iParticle] += Rates.G_He1[iParticle];
    Rates.G[iParticle] += Rates.G_He2[iParticle];
  }
}
