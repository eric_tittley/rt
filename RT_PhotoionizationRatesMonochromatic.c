/* PhotoionizationRatesMonochromatic: Evaluate the photoionization and
 * photoionization heating rate at a specific frequency.
 *
 * int PhotoionizationRatesMonochromatic(rt_float N_H1, rt_float N_He1, rt_float
 * N_He2, rt_float N_H1_cum, rt_float N_He1_cum, rt_float N_He2_cum, rt_float z,
 * rt_float f[6])
 *
 * ARGUMENTS
 *  Input, not modified
 *   N_H1, N_He1, N_He2              Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum  Cumulative column densities between
 *   z                               Redshift (used if the source is a function
 * of time).
 *
 *  Output
 *   f[7]       Rates at frequency, nu. [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2]
 *              I_* are the photoionisation rates (* h_p). [s^-1]
 *              G_* are the heating rates. [J s^-1]
 *              Gamma_HI is the photoionization rate per particle [m^3 s^-1
 * Hz^-1]
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math.h>
#include <stdlib.h>

#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Luminosity.h"
#include "RT_SourceT.h"

/**********************************************************************
 * Integration via parts: nu_0 to nu_1, nu_1 to nu_2, nu_2 to nu_end. *
 * This assists convergence since it avoid the discontinuities at     *
 * these frequencies.                                                 *
 **********************************************************************/
int RT_PhotoionizationRatesMonochromatic(rt_float const N_H1,
                                         rt_float const N_He1,
                                         rt_float const N_He2,
                                         rt_float const N_H1_cum,
                                         rt_float const N_He1_cum,
                                         rt_float const N_He2_cum,
                                         rt_float const SourceRedshift,
                                         RT_SourceT Source,
                                         RT_ConstantsT const Constants,
                                         /*@out@*/ rt_float* const f) {
  rt_float Alpha[3];
  Alpha[0] = H1_H2(Source.nu, Constants);
  Alpha[1] = He1_He2(Source.nu, Constants);
  Alpha[2] = He2_He3(Source.nu, Constants);

  rt_float Luminosity;

  RT_SetSourceFunction(&Source);

  /* Re-cast the LuminosityFunction from void (*)(void *) */
  rt_float (*LuminosityFunction)(RT_LUMINOSITY_ARGS) =
      (rt_float(*)(RT_LUMINOSITY_ARGS))Source.LuminosityFunction;
  Luminosity =
      (*(LuminosityFunction))(Source.nu, SourceRedshift, &Source, &Constants);
  Luminosity /= Source.nu;

  size_t i;
  for (i = 0; i < 7; i++) {
    f[i] = 0.;
  }
  int ierr;
  ierr = RT_PhotoionizationRatesFunctions(Source.nu,
                                          Luminosity,
                                          N_H1,
                                          N_He1,
                                          N_He2,
                                          N_H1_cum,
                                          N_He1_cum,
                                          N_He2_cum,
                                          &Constants,
                                          Alpha,
                                          f);
  if (ierr == EXIT_FAILURE) {
    printf("ERROR: %s: %i: RT_PhotoionizationRatesFunctions failed.\n",
           __FILE__,
           __LINE__);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#ifdef DEBUG
  for (i = 0; i < 7; i++) {
    if (!isfinite(f[i]) || (f[i] < 0.0)) {
      printf("%s: %i: ERROR: f[%lu]=%e\n", __FILE__, __LINE__, i, f[i]);
      return EXIT_FAILURE;
    }
  }
#endif

  return EXIT_SUCCESS;
}
