#include "RT.h"

int RT_useEquilibriumValues(size_t const cellindex,
                            RT_Data const* const Data,
                            RT_RatesBlockT const* const Rates,
                            RT_ConstantsT const* const Constants) {
  /* If
   *  H is mostly ionized
   *   OR
   *  element is mostly ionised AND HI is within 1% of equilibrium
   *   OR
   *  element is mostly neutral AND HI is within 1% of equilibrium
   * then set to the equilibrium value.
   */
  rt_float const n_H1 = Data->n_H[cellindex] * Data->f_H1[cellindex];
  rt_float const n_H2 = Data->n_H[cellindex] * Data->f_H2[cellindex];
  return ((Data->f_H1[cellindex] < Constants->xHIEquilibriumThreshold) ||
          ((FABS(n_H1 / Rates->n_HI_Equilibrium[cellindex] - 1.0) < 0.01) &&
           (Data->f_H1[cellindex] < 0.5)) ||
          ((FABS(n_H2 / Rates->n_HII_Equilibrium[cellindex] - 1.0) < 0.01) &&
           (Data->f_H2[cellindex] < 0.5)));
}
