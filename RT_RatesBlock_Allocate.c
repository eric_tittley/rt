/* Allocate memory for a RT_Data structure and associated memory. The memory is
 * cleared and the pointers are set to appropriate locations in the memory
 * space.
 *
 * ARGUMENTS
 *  nParticles
 *
 * RETURNS
 *  RT_RatesBlockT structure on the host whose elements are pointers to
 *  global memory spaces on the device.
 *    rt_float *I_H1
 *    rt_float *I_He1
 *    rt_float *I_He2
 *    rt_float *G_H1
 *    rt_float *G_He1
 *    rt_float *G_He2
 *
 * TODO: RT_RatesRateBlockT should be malloced as a block and then the pointers
 *       assigned, just at RT_Data is.
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>

#include "RT_Precision.h"
#include "RT_RatesT.h"

int RT_RatesBlock_Allocate(size_t const nParticles,
                           RT_RatesBlockT* const Rates) {
  Rates->I_H1 = malloc(nParticles * sizeof(rt_float));
  if (Rates->I_H1 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->I_He1 = malloc(nParticles * sizeof(rt_float));
  if (Rates->I_He1 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->I_He2 = malloc(nParticles * sizeof(rt_float));
  if (Rates->I_He2 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->G = malloc(nParticles * sizeof(rt_float));
  if (Rates->G == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->G_H1 = malloc(nParticles * sizeof(rt_float));
  if (Rates->G_H1 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->G_He1 = malloc(nParticles * sizeof(rt_float));
  if (Rates->G_He1 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->G_He2 = malloc(nParticles * sizeof(rt_float));
  if (Rates->G_He2 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->alpha_H1 = malloc(nParticles * sizeof(rt_float));
  if (Rates->alpha_H1 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->alpha_He1 = malloc(nParticles * sizeof(rt_float));
  if (Rates->alpha_He1 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->alpha_He2 = malloc(nParticles * sizeof(rt_float));
  if (Rates->alpha_He2 == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->L = malloc(nParticles * sizeof(rt_float));
  if (Rates->L == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->Gamma_HI = malloc(nParticles * sizeof(rt_float));
  if (Rates->Gamma_HI == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->n_HI_Equilibrium = malloc(nParticles * sizeof(rt_float));
  if (Rates->n_HI_Equilibrium == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->n_HII_Equilibrium = malloc(nParticles * sizeof(rt_float));
  if (Rates->n_HII_Equilibrium == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }
  Rates->TimeScale = malloc(nParticles * sizeof(rt_float));
  if (Rates->TimeScale == NULL) {
    printf("ERROR: %s: %i: : Unable to allocate %lu bytes\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
