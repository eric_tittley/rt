#ifndef _RT_LUMINOSITY_H_
#define _RT_LUMINOSITY_H_

/* Declarations of the available RT_Luminosity functions
 * Generally one need to point to one of these for each source
 * as the Source.LuminosityFunction
 *  e.g.
 *  #include "RT_Luminosity.h"
 *  #include "RT_SourceT.h"
 *  RT_SourceT Source;
 *  Source.LuminosityFunction = &RT_Luminosity_POWER_LAW_MINUS_ONE;
 *  */

#include "RT_ConstantsT.h"
#include "RT_SourceT.h"

#define RT_LUMINOSITY_ARGS                                             \
  rt_float const nu, rt_float const z, RT_SourceT const *const Source, \
      RT_ConstantsT const *const Constants

rt_float RT_Luminosity_BLACK_BODY(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_HYBRID(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_MONOCHROMATIC(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_POWER_LAW(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_POWER_LAW_MINUS_ONE(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_POWER_LAW_MINUS_ONE_HALF(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_POWER_LAW_MINUS_THREE_HALVES(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_POWER_LAW_MINUS_TWO(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_SOURCE_MINI_QUASAR(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_TABULATED(RT_LUMINOSITY_ARGS);
rt_float RT_Luminosity_GIZMO(RT_LUMINOSITY_ARGS);
#endif /* _RT_LUMINOSITY_H_ */
