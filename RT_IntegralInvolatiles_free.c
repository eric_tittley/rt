#include <stdio.h>
#include <stdlib.h>

#include "RT_IntegralInvolatilesT.h"

void RT_IntegralInvolatiles_free(RT_IntegralInvolatilesT Involatiles) {
  free(Involatiles.Frequencies);
  free(Involatiles.Weights);
  free(Involatiles.Alphas);
  free(Involatiles.Luminosities);
}
