#include <stdio.h>
#include <assert.h>

#include "RT_Luminosity.h"
#include "RT_Luminosity_Types.h"
#include "RT_SourceT.h"

void RT_SetSourceFunction(RT_SourceT* const Source) {
  switch (Source->SourceType) {
    case SourceBlackBody:
      Source->LuminosityFunction = (SourceFunctionT)&RT_Luminosity_BLACK_BODY;
      break;
    case SourceHybrid:
      Source->LuminosityFunction = (SourceFunctionT)&RT_Luminosity_HYBRID;
      break;
    case SourceMonochromatic:
      Source->LuminosityFunction =
          (SourceFunctionT)&RT_Luminosity_MONOCHROMATIC;
      break;
    case SourcePowerLaw:
      Source->LuminosityFunction = (SourceFunctionT)&RT_Luminosity_POWER_LAW;
      break;
    case SourcePowerLawMinusOne:
      Source->LuminosityFunction =
          (SourceFunctionT)&RT_Luminosity_POWER_LAW_MINUS_ONE;
      break;
    case SourcePowerLawMinusOneHalf:
      Source->LuminosityFunction =
          (SourceFunctionT)&RT_Luminosity_POWER_LAW_MINUS_ONE_HALF;
      break;
    case SourcePowerLawMinusThreeHalves:
      Source->LuminosityFunction =
          (SourceFunctionT)&RT_Luminosity_POWER_LAW_MINUS_THREE_HALVES;
      break;
    case SourcePowerLawMinusTwo:
      Source->LuminosityFunction =
          (SourceFunctionT)&RT_Luminosity_POWER_LAW_MINUS_TWO;
      break;
    case SourceMiniQuasar:
      Source->LuminosityFunction =
          (SourceFunctionT)&RT_Luminosity_SOURCE_MINI_QUASAR;
      break;
    case SourceTabulated:
      Source->LuminosityFunction = (SourceFunctionT)&RT_Luminosity_TABULATED;
      break;
    case SourceGizmo:
      Source->LuminosityFunction = (SourceFunctionT)&RT_Luminosity_GIZMO;
      break;
    case SourceUser:
      assert(Source->LuminosityFunction); // LuminosityFunction was set by the user
      break;
    default:
      printf("ERROR: %s: Unknown Source Type or Source Type not set: %i\n",
             __FILE__,
             Source->SourceType);
  }
}
