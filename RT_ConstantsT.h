#ifndef _RT_CONSTANTS_T_H_
#define _RT_CONSTANTS_T_H_

#include "RT_Precision.h"
#include <stdlib.h>

struct RT_ConstantsT_struct {
  /**** CONFIGURATION ****/

  /* These are for the refinements at the ionisation front */
  rt_float Max_tau_in_cell;
  rt_float Max_cumulative_tau_for_refinement;

  /* Minimum time step in the Radiative Transfer Section */
  rt_float MIN_DT;

  /* MAX_DT sets a hard maximum timestep. Should not be needed with adaptive
   * timesteps, but may be needed to sychronise LOS segments on different
   * grids. For reference, the Hubble time is 4e17s.
   * Tests (10 04 27) indicate very little runtime penalty (and potentially
   * a benefit) until MAX_DT < 5e11 but clear benefits for MAX_DT < 5e13 .
   * MAX_DT < MIN_DT will probably break things. */
  rt_float MAX_DT; /* s */

  rt_float Timestep_Factor;

  /* xHIEquilibriumThreshold defines the HI fraction below which species
   * fractions for Hydrogen are set to equilibrium values. This functionality is
   * necessary to prevent timestepping instabilities which crop up when
   * fractions approach equilibrium values.
   */
  rt_float xHIEquilibriumThreshold;

  rt_float OnePlusEps;
  rt_float OneMinusEps;

  /**** COSMOLOGY ****/
  rt_float Y, h;
  rt_float omega_m, omega_b, omega_v;
  rt_float T_CMB_0; /* Cosmic Microwave Background temperature at present */

  /**** Physical ****/
  /*Ionisation thresholds */
  rt_float a;                /* Radiation density constant */
  rt_float c;                /* Speed of light */
  rt_float gamma_ad;         /* Adiabatic index for a monoatomic gas */
  rt_float h_p;              /* Plank constant */
  rt_float k_B;              /* Boltzman's Constant */
  rt_float Ma;               /* Million years */
  rt_float mass_e;           /* Electron mass */
  rt_float mass_p;           /* Proton mass */
  rt_float Mpc;              /* Mega parsec */
  rt_float nu_0, nu_1, nu_2; /* Ionizatino frequency thresholds HI, HeI, HeII */
  rt_float sigma_T;          /* Thomson cross section */

  /*Constants for interpolation formula for cross-section, Osterbrock p.36 */
  rt_float sigma_0, sigma_1, sigma_2;

  /* Private stuff. */

  /* The number of sample frequencies in each of the three intervals:
   * nu_0 to nu_1,
   * nu_1 to nu_2,
   * nu_2 to infinity
   */
  size_t *NLevels;
  size_t NLevels_NIntervals;

  /* The ionization cross sections at the integration frequencies.
   * Alpha does not need to be filled in. It can be set by
   * RT_InitializeCrossSectionArray()
   * Alpha will be [NLevels_NIntervals][N_Levels*][3]
   * (recall the last index is the fast index, in this case the one that holds
   * the rt_float value)
   * There are three intervals, nu_0 to nu_1, nu_1 to nu_2, nu_2 to infinity.
   * Those three intervals are the same as NLevels_NIntervals above.
   * In each interval, there will be N_Levels(interval) groups of 3 cross
   * sections (for HI, HeI, and HeII).
   * So Alpha is three dimensional but with different size lengths of slices. */
  rt_float ***Alpha;
};

typedef struct RT_ConstantsT_struct RT_ConstantsT;

enum ConstantsEntries {
  iMax_tau_in_cell,
  iMax_cumulative_tau_for_refinement,
  iMIN_DT,
  iMAX_DT,
  iTimestep_Factor,
  ixHIEquilibriumThreshold,
  iOnePlusEps,
  iOneMinusEps,
  iY,
  ih,
  iomega_m,
  iomega_b,
  iomega_v,
  inu_0,
  inu_1,
  inu_2,
  isigma_0,
  isigma_1,
  isigma_2,
  ih_p,
  ik_B,
  ic,
  imass_e,
  imass_p,
  isigma_T,
  ia,
  iMa,
  iMpc,
  iT_CMB_0,
  igamma_ad,
  iNLevels,
  nConstants
};

#endif
