/* Dump all the RT data to file.
 *
 * ARGUMENTS
 *  Input, not modified
 *   filename           The name of the file to which to write the data.
 *   Data               The structure containing the data to be written.
 *   Nlos               The number of lines-of-sight in Data.
 *
 * RETURNS
 *  EXIT_SUCCESS        Success
 *  EXIT_FAILURE        Failure
 *
 * SEE ALSO
 *  RT_LoadData RT_Data_Write
 *
 * AUTHOR: Eric Tittley
 */
#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "RT.h"
#include "RT_Data.h"
#include "RT_FileHeaderT.h"

#ifdef SUPPORT_EXISTS
#  include "Support.h"
#endif

int RT_SaveData(char const *const filename,
                RT_Data **const Data,
                size_t const Nlos,
                RT_FileHeaderT const FileHeader) {
  /* Open the file */
  FILE *fid = fopen(filename, "w");
  if (fid == NULL) {
    printf("ERROR: %s: %i: Unable to open file %s\n",
           __FILE__,
           __LINE__,
           filename);
#ifdef SUPPORT_EXISTS
    ReportFileError(filename);
#endif
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  /* Explicitly initialize the header */
  int hdrInt[128];
  float
      hdrFloat[128]; /* Not rt_float as we want to keep it a predictable size */
  memset(hdrInt, 0, 128 * sizeof(int));
  memset(hdrFloat, 0, 128 * sizeof(float));

  /* *** Write a wee header composed of an integer component and a floating
   * *** point component. *** */
  /* Write the integer 1.  If it can be read as a one, then we know
   * the endianess is correct.  Otherwise, we can re-read it with swapped
   * endianess. */
  hdrInt[0] = 1;
  hdrInt[1] = VERSION_MAJOR;
  hdrInt[2] = VERSION_MINOR;
  hdrInt[3] = Data[0]->NumCells;
  hdrInt[4] = Nlos;
#ifdef RT_DATA_VELOCITIES
  hdrInt[6] = 1;
#endif
#if (defined SEGMENTED_LOS) || (defined REFINEMENTS)
  hdrInt[7] = 1; /* NCol present */
#endif
#ifdef REFINEMENTS
  hdrInt[8] = 1;
#endif
#ifndef RT_DOUBLE
  hdrInt[9] = 1; /* Single precision flag */
#endif
  hdrInt[10] = 1; /* TimeScales are stored */
  size_t Nwritten = fwrite(hdrInt, 128 * sizeof(int), 1, fid);
  if (Nwritten != 1) {
    printf("ERROR: %s: %i: Failed to write hdrInt to %s.\n",
           __FILE__,
           __LINE__,
           filename);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  hdrFloat[0] = FileHeader.ExpansionFactor;
  hdrFloat[1] = FileHeader.Redshift;
  hdrFloat[2] = FileHeader.Time; /* s */
  Nwritten = fwrite(hdrFloat, 128 * sizeof(float), 1, fid);
  if (Nwritten != 1) {
    printf("ERROR: %s: %i: Failed to write hdrFloat to %s.\n",
           __FILE__,
           __LINE__,
           filename);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  /* Write the data */
  int ierr;
  size_t i;
  for (i = 0; i < Nlos; i++) {
    ierr = RT_Data_Write(Data[i], fid);
    if (ierr != EXIT_SUCCESS) {
      printf("ERROR: %s: %i: Failed to write LOS %lu of %lu to %s.\n",
             __FILE__,
             __LINE__,
             i,
             Nlos,
             filename);
      (void)fflush(stdout);
      return EXIT_FAILURE;
    }
  }

#ifdef REFINEMENTS
  for (i = 0; i < Nlos; i++) {
    size_t j;
    for (j = 0; j < Data[i]->NumCells; j++) {
      if (Data[i]->Ref_pointer[j]) {
        ierr = RT_Data_Write(Data[i]->Ref_pointer[j], fid);
        if (ierr != EXIT_SUCCESS) {
          printf(
              "ERROR: %s: %i: Failed to write refinement for cell %lu of LOS "
              "%lu to %s\n",
              __FILE__,
              __LINE__,
              j,
              i,
              filename);
          (void)fflush(stdout);
          return EXIT_FAILURE;
        }
      }
    }
  }
#endif

  /* Close the file */
  ierr = fclose(fid);
  if (ierr != 0) {
    printf("ERROR: %s: Unable to close file %s\n", __FILE__, filename);
#ifdef SUPPORT_EXISTS
    ReportFileError(filename);
#endif
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

#if VERBOSE > 0
  printf("%s: Exiting\n", __FILE__);
  (void)fflush(stdout);
#endif

  return EXIT_SUCCESS;
}
