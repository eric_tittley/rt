/* PREPROCESSOR MACROS
 *  DEBUG
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif
#include <math.h>

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_RatesT.h"

int RT_LastIterationUpdate(RT_ConstantsT const Constants,
                           RT_RatesT const* const Rates,
                           RT_Cell* const Data_cell) {
  /* Find the neutral & ionized number densities. */
  rt_float const n_H2 = Data_cell->n_H * Data_cell->f_H2;
  rt_float const n_He2 = Data_cell->n_He * Data_cell->f_He2;
  rt_float const n_He3 = Data_cell->n_He * Data_cell->f_He3;

  /* Calculate T */
  rt_float const n_e = n_H2 + n_He2 + 2. * n_He3;
  rt_float const n = Data_cell->n_H + Data_cell->n_He + n_e;
  rt_float const mu = (Data_cell->n_H + 4. * Data_cell->n_He) / n;
#ifndef RT_DOUBLE
  rt_float const T = mu * (Constants.mass_p / Constants.k_B) *
                     Data_cell->Entropy *
                     powf(Data_cell->Density * 1e24, Constants.gamma_ad - 1.) *
                     powf(1e-24, Constants.gamma_ad - 1.0);
#else
  rt_float const T = mu * (Constants.mass_p / Constants.k_B) *
                     Data_cell->Entropy *
                     pow(Data_cell->Density, Constants.gamma_ad - 1.);
#endif

#ifdef DEBUG
  if (!(bool)isfinite(T) || (T < 0)) {
    printf("ERROR: %s: %i: T=%5.3e; Entropy=%5.3e; mu=%7.5f; Density=%5.3e\n",
           __FILE__,
           __LINE__ - 4,
           T,
           Data_cell->Entropy,
           mu,
           Data_cell->Density);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  /* Update derivable structure elements. */
  Data_cell->T = T;

  return EXIT_SUCCESS;
}
