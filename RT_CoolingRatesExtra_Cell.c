/* RT_CoolingRatesExtra: Set the Collisional (L_eH) and Compton (L_C) cooling
 *                       rates.
 *
 * NOTE: Rates are multiplied by Ma to keep them scaled within a float range.
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math.h>

#include "RT.h"

void RT_CoolingRatesExtra_Cell(rt_float const ExpansionFactor,
                               RT_ConstantsT const Constants,
                               RT_Cell const* const Data,
                               RT_RatesT* const Rates) {
/* Need this because RT_RatesBlockT won't have elements L_eH and L_C */
#ifndef NO_COOLING

  /* Find the neutral & ionized number densities. */
  rt_float const n_H1 = Data->n_H * Data->f_H1;
  rt_float const n_H2 = Data->n_H * Data->f_H2;
  rt_float const n_He2 = Data->n_He * Data->f_He2;
  rt_float const n_He3 = Data->n_He * Data->f_He3;

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

  /* Total number density */
  rt_float const n = Data->n_H + Data->n_He + n_e;

  /* Temperature is a function of Entropy */
  rt_float const mu = (Data->n_H + 4. * Data->n_He) /
                      n; /* Assumes m_He = 4 m_H and mass_e = 0 */

  rt_float const Temperature =
      mu * (Constants.mass_p / Constants.k_B) * Data->Entropy *
      POW(Constants.Mpc, 1.0 - Constants.gamma_ad) *
      POW(Data->Density * Constants.Mpc, Constants.gamma_ad - 1.0);

  /* Cooling rates:
   * TODO:
   *  Collisional Ionization?
   *  Collisional excitation of the excited levels of HeI & HeII?
   */

  /* Cooling via Collisional Excitation of Neutral Hydrogen
   * L_eH: the cooling rate due to collisional excitation of the excited
   *       levels in neutral hydrogen J m^-3 s^-1.
   *       Spitzer 1978, Physical Processes in the ISM, p.140
   *        which is a fit to Dalgarno & McCray (ARAA 1972, p 375) Table 2
   *        for 3000 to 7500 K.
   *        D&M's Table 2 comes from Gould & Thakur (1970, Ann Phys 61:351)
   *        which I can't get a copy of.
   *       Scholz & Waters '91 (ApJ 380 302) provide a different derivation
   *        that spans a larger range in temperature.
   *
   */
#  ifdef COOLING_COLLISIONAL_EXCITATION_OF_HYDROGEN__SPITZER
  /* n_e = 10^0 to 10^4
   * n_H1 = 10^-5 to 10^5
   * The exponential = 10^-10 to 10^0
   * So we have 10^[ -32 +0 -5 -10 ] = 10^-47
   *         to 10^[ -32 +4 +5 +0  ] = 10^-23
   * The smallest value of a float is 1.17e-38, so we are flirting with
   * underflow.  Scale by Ma
   */
  Rates->L +=
      (Constants.Ma * 7.3e-32) * n_e * n_H1 * EXP(-118400.0 / Temperature);
#  else
  /* Coefficients for Schulz & Walters' '91 approximation for
   * Cooling via collisional excitation of neutral hydrogen */
  rt_float const CECoef_lo[6] = {2.137913e+2,
                                 -1.139492e+2,
                                 2.506062e+1,
                                 -2.762755,
                                 1.515352e-1,
                                 -3.290382e-3};
  rt_float const CECoef_hi[6] = {2.7125446e+2,
                                 -9.8019455e+1,
                                 1.40072760e+1,
                                 -9.7808421e-1,
                                 3.3562891e-2,
                                 -4.55332321e-4};
  if ((Temperature > 0.0) && (bool)isfinite(Temperature)) {
    rt_float const lnT1 = LOG(Temperature);
    rt_float const lnT2 = lnT1 * lnT1;
    rt_float const lnT3 = lnT1 * lnT2;
    rt_float const lnT4 = lnT1 * lnT3;
    rt_float const lnT5 = lnT1 * lnT4;
    rt_float const PreFactor = Constants.Ma * 1e-33;
    if (Temperature <= 1.0e5) {
      Rates->L += PreFactor * EXP(CECoef_lo[0] + CECoef_lo[1] * lnT1 +
                                    CECoef_lo[2] * lnT2 + CECoef_lo[3] * lnT3 +
                                    CECoef_lo[4] * lnT4 + CECoef_lo[5] * lnT5 -
                                    1.184156e5 / Temperature) * (n_e * n_H1);
    } else {
      Rates->L += PreFactor * EXP(CECoef_hi[0] + CECoef_hi[1] * lnT1 +
                                    CECoef_hi[2] * lnT2 + CECoef_hi[3] * lnT3 +
                                    CECoef_hi[4] * lnT4 + CECoef_hi[5] * lnT5 -
                                    1.184156e5 / Temperature) * (n_e * n_H1);
    }
  } else {
    printf("ERROR: %s: %i: Temperature is out of range: %e\n",
           __FILE__,
           __LINE__,
           Temperature);
    (void)fflush(stdout);
  }
#  endif /* #ifndef COOLING_COLLISIONAL_EXCITATION_OF_HYDROGEN__SPITZER #else \
          */

  /* L_C: Cooling from Inverse Compton scattering off CMB photons. */
  /*  10^[-29 -16 -23 - ( -31 + 8 ) + [0 to 4] + 4*0 + [4 to 8]]
   *   = 10^[-41 to -33]
   *  Susceptible to underflow.  Force double and scale by Ma.
   */
  double const Compton_coefficient =
      ((double)Constants.Ma * 4.0 * (double)Constants.sigma_T *
       (double)Constants.a * (double)Constants.k_B) /
      ((double)Constants.mass_e * (double)Constants.c);
  double const T_CMB = (double)Constants.T_CMB_0 / (double)ExpansionFactor;

  Rates->L += (float)(Compton_coefficient * n_e * pow(T_CMB, 4.0) *
                       ((double)Temperature - T_CMB));


#endif /* #ifndef NO_COOLING */
}
