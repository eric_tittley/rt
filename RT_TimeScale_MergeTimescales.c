#include "RT_TimeScale.h"

void RT_TimeScale_MergeTimescales(RT_TimeScale* const TS_base,
                                  RT_TimeScale const* const TS_to_merge) {
  if (TS_to_merge->Entropy < TS_base->Entropy)
    TS_base->Entropy = TS_to_merge->Entropy;
  if (TS_to_merge->I_H1 < TS_base->I_H1) TS_base->I_H1 = TS_to_merge->I_H1;
  if (TS_to_merge->I_He1 < TS_base->I_He1) TS_base->I_He1 = TS_to_merge->I_He1;
  if (TS_to_merge->I_He2 < TS_base->I_He2) TS_base->I_He2 = TS_to_merge->I_He2;
  if (TS_to_merge->R_H1 < TS_base->R_H1) TS_base->R_H1 = TS_to_merge->R_H1;
  if (TS_to_merge->R_He1 < TS_base->R_He1) TS_base->R_He1 = TS_to_merge->R_He1;
  if (TS_to_merge->R_He2 < TS_base->R_He2) TS_base->R_He2 = TS_to_merge->R_He2;
}
