#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>

#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"

void RT_UpdateColumnDensitiesAlongLOS(rt_float const t_step,
                                      rt_float const dt,
                                      RT_ConstantsT const Constants,
                                      RT_Data* const Data_los) {
  size_t N_cells = Data_los->NumCells;

  /* Update the cumulative optical depths to the cells */
#ifndef THIN_APPROX
  /* Calculates column density up to the shell denoted by index iz.*/
#  if (defined REFINEMENTS) || (defined SEGMENTED_LOS)
  /* Linearly interpolate the cumulative column density incoming to the start
   * of the LOS.
   * NCol[0:5] must be set prior to calling RT_ProcessLOS.*/
  Data_los->column_H1[0] =
      Data_los->NCol[0] + t_step / dt * (Data_los->NCol[3] - Data_los->NCol[0]);
  Data_los->column_He1[0] =
      Data_los->NCol[1] + t_step / dt * (Data_los->NCol[4] - Data_los->NCol[1]);
  Data_los->column_He2[0] =
      Data_los->NCol[2] + t_step / dt * (Data_los->NCol[5] - Data_los->NCol[2]);
#  else
  Data_los->column_H1[0] = 0.;
  Data_los->column_He1[0] = 0.;
  Data_los->column_He2[0] = 0.;
#  endif

  /* Each cell's column density is set to be the sum of the previous cells' */
  size_t iz;
  for (iz = 1; iz < N_cells; iz++) {
    Data_los->column_H1[iz] =
        Data_los->column_H1[iz - 1] +
        (Data_los->n_H[iz - 1] * Data_los->f_H1[iz - 1] * Data_los->dR[0]);
  }
  for (iz = 1; iz < N_cells; iz++) {
    Data_los->column_He1[iz] =
        Data_los->column_He1[iz - 1] +
        (Data_los->n_He[iz - 1] * Data_los->f_He1[iz - 1] * Data_los->dR[0]);
  }
  for (iz = 1; iz < N_cells; iz++) {
    Data_los->column_He2[iz] =
        Data_los->column_He2[iz - 1] +
        (Data_los->n_He[iz - 1] * Data_los->f_He2[iz - 1] * Data_los->dR[0]);
  }

#else
  /* Thin approximation.  The input spectrum is not modified by
   * absorption in the column. */
  size_t iz;
  for (iz = 0; iz < N_cells; iz++) {
    Data_los->column_H1[iz] = 0.;
    Data_los->column_He1[iz] = 0.;
    Data_los->column_He2[iz] = 0.;
  }
#endif

  return;
}
