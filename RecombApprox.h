#ifndef _RECOMBAPPROX_H_
#define _RECOMBAPPROX_H_

/***** RECOMBINATION COEFFICIENTS ******/
enum RecombApprox { Approx_A, Approx_B };

#endif
