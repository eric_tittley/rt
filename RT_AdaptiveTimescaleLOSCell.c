/* RT_AdaptiveTimescaleLOS: Update the various timescales.
 *
 * ierr = RT_AdaptiveTimescaleLOS(rt_float const dt,
                               RT_RatesT *Rates,
                               RT_Cell *Data_cell,
                               RT_TimeScale *dt_RT_sugg)
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  dt         Top-level timestep. [s]
 *  Rates
 *  Data_cell
 *
 * ARGUMENTS INPUT AND MODIFIED
 *  dt_RT_sugg The timescales structure.  Must be initialised.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * PREPROCESSOR MACROS
 *  DEBUG
 *
 * SEE ALSO
 *  RT_AdaptiveTimescale which processes a RT_cell
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math.h>
#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif

#include "Logic.h"
#include "RT.h"
#include "RT_ConstantsT.h"
#include "RT_RatesT.h"
#include "RT_TimeScale.h"

int RT_AdaptiveTimescaleLOSCell(rt_float const dt,
                                RT_ConstantsT const Constants,
                                RT_RatesT const* const Rates,
                                RT_Data const Data,
                                size_t const iCell,
                                RT_TimeScale* const dt_RT_sugg) {
  rt_float const InfiniteTime = 1e20;

  RT_TimeScale tau;

  /* Find the neutral & ionised number densities. */
  rt_float const n_H1 = Data.n_H[iCell] * Data.f_H1[iCell];
  rt_float const n_H2 = Data.n_H[iCell] * Data.f_H2[iCell];
  rt_float const n_He1 = Data.n_He[iCell] * Data.f_He1[iCell];
  rt_float const n_He2 = Data.n_He[iCell] * Data.f_He2[iCell];
  rt_float const n_He3 = Data.n_He[iCell] * Data.f_He3[iCell];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

  /* Entropy timescale */
  rt_float ResidualHeatingScaleFactorGammaInv =
      POW(Constants.Ma, 1. / Constants.gamma_ad);
  rt_float const EntropyRate =
      FABS((Constants.gamma_ad - 1.) /
           POW(Data.Density[iCell] * ResidualHeatingScaleFactorGammaInv,
               Constants.gamma_ad) *
           (Rates->G - Rates->L));
  if (EntropyRate > 0.) {
    tau.Entropy = Data.Entropy[iCell] / EntropyRate;
  } else {
    tau.Entropy = InfiniteTime;
  }

  /* Ionization timescales */
  if (Rates->I_H1 > 0.0) {
    tau.I_H1 = n_H1 / Rates->I_H1;
  } else {
    tau.I_H1 = InfiniteTime;
  }
  if ((Rates->I_He1 > 0.0) && (Constants.Y > 0.0)) {
    tau.I_He1 = n_He1 / Rates->I_He1;
  } else {
    tau.I_He1 = InfiniteTime;
  }
  if ((Rates->I_He2 > 0.0) && (Constants.Y > 0.0)) {
    tau.I_He2 = n_He2 / Rates->I_He2;
  } else {
    tau.I_He2 = InfiniteTime;
  }

  /* Recombination timescales */
  if (n_e > 0.0) {
    if (Rates->alpha_H1 > 0.0) {
      tau.R_H1 = 1.0 / (Rates->alpha_H1 * n_e);
    } else {
      tau.R_H1 = InfiniteTime;
    }
    if ((Rates->alpha_He1 > 0.0) && (Constants.Y > 0.0)) {
      tau.R_He1 = 1.0 / (Rates->alpha_He1 * n_e);
    } else {
      tau.R_He1 = InfiniteTime;
    }
    if ((Rates->alpha_He2 > 0.0) && (Constants.Y > 0.0)) {
      tau.R_He2 = 1.0 / (Rates->alpha_He2 * n_e);
    } else {
      tau.R_He2 = InfiniteTime;
    }
  } else {
    tau.R_H1 = InfiniteTime;
    tau.R_He1 = InfiniteTime;
    tau.R_He2 = InfiniteTime;
  }

  if (dt_RT_sugg->Entropy > tau.Entropy) dt_RT_sugg->Entropy = tau.Entropy;
  if (dt_RT_sugg->I_H1 > tau.I_H1) dt_RT_sugg->I_H1 = tau.I_H1;
  if (dt_RT_sugg->I_He1 > tau.I_He1) dt_RT_sugg->I_He1 = tau.I_He1;
  if (dt_RT_sugg->I_He2 > tau.I_He2) dt_RT_sugg->I_He2 = tau.I_He2;
  if (dt_RT_sugg->R_H1 > tau.R_H1) dt_RT_sugg->R_H1 = tau.R_H1;
  if (dt_RT_sugg->R_He1 > tau.R_He1) dt_RT_sugg->R_He1 = tau.R_He1;
  if (dt_RT_sugg->R_He2 > tau.R_He2) dt_RT_sugg->R_He2 = tau.R_He2;
#ifdef DEBUG
  if (!(bool)isfinite(dt_RT_sugg->Entropy) || (dt_RT_sugg->Entropy <= 0)) {
    printf("ERROR: %s: %i: dt_RT_sugg->Entropy=%5.2e; tau.Entropy=%5.2e\n",
           __FILE__,
           __LINE__,
           dt_RT_sugg->Entropy,
           tau.Entropy);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if (!(bool)isfinite(dt_RT_sugg->I_H1) || (dt_RT_sugg->I_H1 <= 0)) {
    printf("ERROR: %s: %i: dt_RT_sugg->I_H1=%5.2e; tau.I_H1=%5.2e\n",
           __FILE__,
           __LINE__,
           dt_RT_sugg->I_H1,
           tau.I_H1);
    printf("      n_H1=%5.2e; n_H=%5.2e; f_H1=%5.2e;\n",
           n_H1,
           Data.n_H[iCell],
           Data.f_H1[iCell]);
    printf("      Rates->I_H1=%5.2e\n", Rates->I_H1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if ((!(bool)isfinite(dt_RT_sugg->I_He1) || (dt_RT_sugg->I_He1 <= 0)) &&
      (Constants.Y > 0.0)) {
    printf("ERROR: %s: %i: dt_RT_sugg->I_He1=%5.2e; tau.I_He1=%5.2e\n",
           __FILE__,
           __LINE__,
           dt_RT_sugg->I_He1,
           tau.I_He1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if ((!(bool)isfinite(dt_RT_sugg->I_He2) || (dt_RT_sugg->I_He2 <= 0)) &&
      (Constants.Y > 0.0)) {
    printf("ERROR: %s: %i: dt_RT_sugg->I_He2=%5.2e; tau.I_He2=%5.2e\n",
           __FILE__,
           __LINE__,
           dt_RT_sugg->I_He2,
           tau.I_He2);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if (!(bool)isfinite(dt_RT_sugg->R_H1) || (dt_RT_sugg->R_H1 <= 0)) {
    printf("ERROR: %s: %i: dt_RT_sugg->R_H1=%5.2e; tau.R_H1=%5.2e\n",
           __FILE__,
           __LINE__,
           dt_RT_sugg->R_H1,
           tau.R_H1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if ((!(bool)isfinite(dt_RT_sugg->R_He1) || (dt_RT_sugg->R_He1 <= 0)) &&
      (Constants.Y > 0.0)) {
    printf("ERROR: %s: %i: dt_RT_sugg->R_He1=%5.2e; tau.R_He1=%5.2e\n",
           __FILE__,
           __LINE__,
           dt_RT_sugg->R_He1,
           tau.R_He1);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  if ((!(bool)isfinite(dt_RT_sugg->R_He2) || (dt_RT_sugg->R_He2 <= 0)) &&
      (Constants.Y > 0.0)) {
    printf("ERROR: %s: %i: dt_RT_sugg->R_He2=%5.2e; tau.R_He2=%5.2e\n",
           __FILE__,
           __LINE__,
           dt_RT_sugg->R_He2,
           tau.R_He2);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
#endif

  return EXIT_SUCCESS;
}
